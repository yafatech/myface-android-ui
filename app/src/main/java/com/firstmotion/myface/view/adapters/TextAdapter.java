package com.firstmotion.myface.view.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.ItemTextBinding;

import java.util.ArrayList;
import java.util.List;


public class TextAdapter extends RecyclerView.Adapter<TextAdapter.ViewHolder> {
private List<String> data;

    public TextAdapter() {
        data = new ArrayList<>();
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTextBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_text,
                parent,
                false);
        return new TextAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.bindTo(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ItemTextBinding binding;

        public ViewHolder(ItemTextBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindTo(String text) {
            binding.body.setText(text);
            binding.executePendingBindings();

        }
    }
}
