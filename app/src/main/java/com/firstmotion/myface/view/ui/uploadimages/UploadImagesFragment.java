package com.firstmotion.myface.view.ui.uploadimages;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.UploadImagesFragmentBinding;
import com.firstmotion.myface.networking.models.requests.UpdateProfileRequest;
import com.firstmotion.myface.networking.repositories.ProfileRepository;
import com.firstmotion.myface.utils.AppUtils;
import com.firstmotion.myface.utils.ImageRotators;
import com.firstmotion.myface.utils.imagedetection.DetectFaces;
import com.firstmotion.myface.utils.systemnotifications.SystemNotification;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.FileProvider.getUriForFile;

/**
 * fragment to Hold upload initial User Images to the CDN For having at least some images for the Customers.
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.1
 */
public class UploadImagesFragment extends Fragment {
    private UploadImagesFragmentBinding binding;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static final DetectFaces detectFaces = DetectFaces.build();
    private static final SystemNotification systemNotifications = SystemNotification.build();
    private static boolean firstImageAccepted;
    private static boolean secondImageAccepted;
    private static boolean thirdImageAccepted;
    private static boolean fourthImageAccepted;
    private File image;
    private static String userId, fullName, phoneNumber, country, city;
    private static int age;


    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.upload_images_fragment, container, false);
        binding.uploadImgOne.setOnClickListener((v) -> dispatchTakePictureIntent(1));
        binding.uploadImgTwo.setOnClickListener((v) -> dispatchTakePictureIntent(2));
        binding.uploadImgThree.setOnClickListener((v) -> dispatchTakePictureIntent(3));
        binding.uploadImgFour.setOnClickListener((v) -> dispatchTakePictureIntent(4));

        return binding.getRoot();
    }


    @SuppressLint("QueryPermissionsNeeded")
    private void dispatchTakePictureIntent(int request) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(requireActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = getUriForFile(requireContext(),
                        "com.firstmotion.myface.utils.FileProvider",
                        photoFile);
                System.out.println("uri -> " + photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, request);
            }
        }

    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String imageFileName = "myFace_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
//        String currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        NavController navController = Navigation.findNavController(view);
        binding.uploadImgTV.setText(preferences.getString("upload-images-note", "For Accurate Results, All Users Are Required to Upload Some Photos At First Usage"));
        binding.uploadImgNextBtn.setText(preferences.getString("next", "NEXT"));
        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا")
                || preferences.getString("langCode", "").contains("او"))
            view.setTextDirection(View.TEXT_DIRECTION_RTL);
        else
            view.setTextDirection(View.TEXT_DIRECTION_LTR);
        if (!AppUtils.isNetworkAvailable(requireContext())) {
            systemNotifications
                    .pushNotification(
                            preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                            getView(),
                            "dismiss",
                            getActivity(),
                            R.color.red
                    );
        } else {

            ProfileRepository.getInstance()
                    .loadingUserProfile("/users/profile", preferences.getString("token", ""), preferences.getString("refreshToken", ""), preferences.getString("username", ""), "uploadImg")
                    .observe(getViewLifecycleOwner(), loadUserProfileResponseApiResponse -> {
                        if (loadUserProfileResponseApiResponse != null && loadUserProfileResponseApiResponse.isStatus()) {
                            userId = loadUserProfileResponseApiResponse.getResults().getUserId();
                            fullName = loadUserProfileResponseApiResponse.getResults().getUserInfo() == null ? "MyFace User" : loadUserProfileResponseApiResponse.getResults().getUserInfo().getFullName();
                            phoneNumber = loadUserProfileResponseApiResponse.getResults().getUserInfo() == null ? "" : loadUserProfileResponseApiResponse.getResults().getUserInfo().getPhoneNumber();
                            country = loadUserProfileResponseApiResponse.getResults().getUserInfo() == null ? "" : loadUserProfileResponseApiResponse.getResults().getUserInfo().getCountry();
                            city = loadUserProfileResponseApiResponse.getResults().getUserInfo() == null ? "" : loadUserProfileResponseApiResponse.getResults().getUserInfo().getCity();
                            age = loadUserProfileResponseApiResponse.getResults().getUserInfo() == null ? 0 : loadUserProfileResponseApiResponse.getResults().getUserInfo().getAge();
                            if (fullName != null && !fullName.isEmpty()
                                    && phoneNumber != null && !phoneNumber.isEmpty()
                                    && country != null && !country.isEmpty()
                                    && city != null && !city.isEmpty()
                                    && age != 0)
                                editor.putBoolean("fullUpdatedProfile", true).apply();
                        }
                    });
        }
        // Next BTN.
        binding.uploadImgNextBtn.setOnClickListener((v) -> {
            if (firstImageAccepted && secondImageAccepted && thirdImageAccepted && fourthImageAccepted) {
                editor.putBoolean("FourImagesUploadedSuccessfully", true).apply();
                if (preferences.getBoolean("fullUpdatedProfile", false))
                    navController.navigate(UploadImagesFragmentDirections.actionUploadImgOneToHomeFragment());
                else
                    navController.navigate(UploadImagesFragmentDirections.actionUploadImgOneToProfile());
            } else
                systemNotifications.pushNotification(
                        "You Must Complete the Four Images First!",
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );

        });
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        // upload success.
        if (resultCode == RESULT_OK && image != null) {

            try {
                String filePath = image.getPath();
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                bitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, true);
                Bitmap routedBitMap = ImageRotators.rotateImage(bitmap, filePath);
                switch (requestCode) {
                    case 1:
                        detectFaces.imageFromBitmap(routedBitMap)
                                .detectFaces().addOnSuccessListener(
                                faces -> {
                                    if (!faces.isEmpty()) {
                                        binding.uploadImgOne.setImageBitmap(routedBitMap);
                                        binding.imageOneAccept.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.ic_accept));
                                        uploadImage(routedBitMap, "one");
                                        firstImageAccepted = true;
                                    } else
                                        systemNotifications.pushNotification(
                                                "You Must Upload images for people only, make sure its a photo of you",
                                                getView(),
                                                "dismiss",
                                                getActivity(),
                                                R.color.red
                                        );

                                }).addOnFailureListener(
                                e -> systemNotifications.pushNotification(
                                        "Failed To Detect Faces",
                                        getView(),
                                        "dismiss",
                                        getActivity(),
                                        R.color.red
                                ));
                        break;
                    case 2:
                        detectFaces.imageFromBitmap(routedBitMap)
                                .detectFaces().addOnSuccessListener(
                                faces -> {
                                    if (!faces.isEmpty()) {
                                        binding.uploadImgTwo.setImageBitmap(routedBitMap);
                                        binding.imageTwoAccept.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.ic_accept));
                                        uploadImage(routedBitMap, "two");
                                        secondImageAccepted = true;

                                    } else {
                                        systemNotifications.pushNotification(
                                                "You Must Upload images for people only, make sure its a photo of you",
                                                getView(),
                                                "dismiss",
                                                getActivity(),
                                                R.color.red
                                        );
                                    }
                                })
                                .addOnFailureListener(
                                        e -> {
                                            // Task failed with an exception
                                            systemNotifications.pushNotification(
                                                    "Failed To Detect Faces",
                                                    getView(),
                                                    "dismiss",
                                                    getActivity(),
                                                    R.color.red
                                            );
                                        });
                        break;
                    case 3:
                        detectFaces.imageFromBitmap(routedBitMap)
                                .detectFaces().addOnSuccessListener(
                                faces -> {
                                    if (!faces.isEmpty()) {
                                        binding.uploadImgThree.setImageBitmap(routedBitMap);
                                        binding.imageThreeAccept.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.ic_accept));
                                        uploadImage(routedBitMap, "three");
                                        thirdImageAccepted = true;
                                    } else {
                                        systemNotifications.pushNotification(
                                                "You Must Upload images for people only, make sure its a photo of you",
                                                getView(),
                                                "dismiss",
                                                getActivity(),
                                                R.color.red
                                        );
                                    }
                                })
                                .addOnFailureListener(
                                        e -> {
                                            // Task failed with an exception
                                            systemNotifications.pushNotification(
                                                    "Failed To Detect Faces",
                                                    getView(),
                                                    "dismiss",
                                                    getActivity(),
                                                    R.color.red
                                            );
                                        });
                        break;
                    case 4:
                        detectFaces.imageFromBitmap(routedBitMap)
                                .detectFaces().addOnSuccessListener(
                                faces -> {
                                    if (!faces.isEmpty()) {
                                        binding.uploadImgFour.setImageBitmap(routedBitMap);
                                        binding.imageFourAccept.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.ic_accept));
                                        uploadImage(routedBitMap, "four");
                                        fourthImageAccepted = true;
                                    } else {
                                        systemNotifications.pushNotification(
                                                "You Must Upload images for people only, make sure its a photo of you",
                                                getView(),
                                                "dismiss",
                                                getActivity(),
                                                R.color.red
                                        );
                                    }
                                })
                                .addOnFailureListener(
                                        e -> {
                                            // Task failed with an exception
                                            systemNotifications.pushNotification(
                                                    "Failed To Detect Faces",
                                                    getView(),
                                                    "dismiss",
                                                    getActivity(),
                                                    R.color.red
                                            );
                                        });
                        break;

                }
            } catch (IOException e) {
                systemNotifications.pushNotification(
                        "please try again",
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
            }

        }
    }


    @SuppressLint("UseCompatLoadingForDrawables")
    public void uploadImage(Bitmap bitmap, String imgNum) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            File file = new File(requireContext().getFilesDir(), "myFace-" + UUID.randomUUID() + ".jpg");
            byte[] bitmapData = bos.toByteArray();
            //write the bytes in file
            FileOutputStream fos;
            fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();


            ProfileRepository.getInstance()
                    .uploadImage("cdn/storage/media/upload", userId, file)
                    .observe(getViewLifecycleOwner(), uploadImageResponse -> {
                        if (uploadImageResponse != null && uploadImageResponse.isStatus()) {
                            systemNotifications
                                    .pushNotification(
                                            uploadImageResponse.getMessage(),
                                            getView(),
                                            "ok",
                                            getActivity(),
                                            R.color.green
                                    );
                            ProfileRepository.getInstance()
                                    .updateUserProfile("/users/info", preferences.getString("token", "")
                                            , preferences.getString("refreshToken", ""), new UpdateProfileRequest(
                                                    userId,
                                                    uploadImageResponse.getResults().getFileId(),
                                                    fullName,
                                                    phoneNumber,
                                                    country,
                                                    city,
                                                    age)
                                    ).observe(getViewLifecycleOwner(), editProfileResponse -> {
                                if (editProfileResponse != null && editProfileResponse.isStatus()) {
                                    systemNotifications
                                            .pushNotification(
                                                    editProfileResponse.getMessage(),
                                                    getView(),
                                                    "ok",
                                                    getActivity(),
                                                    R.color.green
                                            );
                                } else{
                                    if (editProfileResponse != null && editProfileResponse.getErrors() != null ){
                                        systemNotifications
                                                .pushNotification(
                                                        editProfileResponse.getErrors().getErrorMessage(),
                                                        getView(),
                                                        "dismiss",
                                                        getActivity(),
                                                        R.color.red
                                                );
                                    }
                                }

                            });
                        } else if (uploadImageResponse != null && !uploadImageResponse.isStatus()) {
                            systemNotifications
                                    .pushNotification(
                                            uploadImageResponse.getErrors().getErrorMessage(),
                                            getView(),
                                            "dismiss",
                                            getActivity(),
                                            R.color.green
                                    );
                        }

                    });

        } catch (IOException ignored) {

        }


    }
}
