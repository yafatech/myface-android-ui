package com.firstmotion.myface.view.ui.tutorial;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.firstmotion.myface.R;

public class TutorialPage2Fragment extends Fragment {
    private NavController navController;
    private Button toPage3Button;
    private SharedPreferences preferences;

    public TutorialPage2Fragment() {
        // Required empty public constructor
    }

    public static TutorialPage2Fragment newInstance(String param1, String param2) {
        return new TutorialPage2Fragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root= inflater.inflate(R.layout.tutorial_fragment_page2, container, false);
        TextView nextTV=root.findViewById(R.id.nextBTN);
        nextTV.setText(preferences.getString("next", "NEXT"));
        return root;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        TextView nextBTN=view.findViewById(R.id.nextBTN);
//        nextBTN.setOnClickListener(v-> navController.navigate(TutorialPage2FragmentDirections.actionTutorialPage2FragmentToSignUp()));
    }
}