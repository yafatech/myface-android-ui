package com.firstmotion.myface.view.ui.changepassword;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.ChangePasswordFragmentBinding;
import com.firstmotion.myface.networking.models.requests.ChangePassRequest;
import com.firstmotion.myface.networking.repositories.ProfileRepository;
import com.firstmotion.myface.networking.repositories.UsersRepository;
import com.firstmotion.myface.utils.NavigationHandler;
import com.firstmotion.myface.utils.systemnotifications.SystemNotification;
import com.google.android.material.textfield.TextInputEditText;

import java.util.regex.Pattern;

public class ChangePasswordFragment extends Fragment {
    private ChangePasswordFragmentBinding binding;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private NavController navController;
    private String userId;
    private static boolean isRequestSuccess;
    private static final SystemNotification systemNotifications = SystemNotification.build();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.change_password_fragment, container, false);
        return binding.getRoot();
    }

    @SuppressLint({"CommitPrefEdits", "SetTextI18n", "UseCompatLoadingForDrawables"})
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
        ProfileRepository.getInstance()
                .loadingUserProfile("/users/profile", preferences.getString("token", ""), preferences.getString("refreshToken", ""), preferences.getString("username", ""), "changePass")
                .observe(getViewLifecycleOwner(), loadUserProfileResponseApiResponse -> {
                    if (loadUserProfileResponseApiResponse != null && loadUserProfileResponseApiResponse.isStatus()) {
                        userId = loadUserProfileResponseApiResponse.getResults().getUserId();
                    }
                });

        //set Texts in change password ui as translations
        binding.changePasswordTV.setText(preferences.getString("change-pass", "CHANGE PASSWORD"));
        binding.currentPasswordTIL.setHint(preferences.getString("current-password", "Current Password"));
        binding.newPasswordTIL.setHint(preferences.getString("new-password", "New Password"));
        binding.rePasswordTIL.setHint(preferences.getString("re-enter-password", "Re-enter Password"));
        binding.continueTV.setText(preferences.getString("continue", "Continue"));
        binding.newPasswordTIL.setOnFocusChangeListener((v, hasFocus) -> binding.newPasswordTIL.setHelperText(preferences.getString("pass-restrict-pattern", "Password Should Contain One Upper Letter(A-Z)\n One Small Letter (a-z) and a Special Character (!@#$%^&*)")));
        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا")
                || preferences.getString("langCode", "").contains("او"))
            view.setTextDirection(View.TEXT_DIRECTION_RTL);
        else
            view.setTextDirection(View.TEXT_DIRECTION_LTR);


        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا") || preferences.getString("langCode", "").contains("و")) {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.backSettingsBTN.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.inverted_back_icon));
        } else {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.backSettingsBTN.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.back_icon));
        }

        binding.backSettingsBTN.setOnClickListener((v) -> {
            NavigationHandler.navigate(getView(), ChangePasswordFragmentDirections.actionChangePasswordToSettings().getActionId(), null);
        });

        Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        binding.continueTV.setOnClickListener(v -> {

                    if (binding.currentPasswordTIET.getText().toString().isEmpty()) {
                        binding.currentPasswordTIET.startAnimation(shake);
                        binding.resultTV.setVisibility(View.GONE);
                    } else if (binding.newPasswordTIET.getText().toString().isEmpty()) {
                        binding.newPasswordTIET.startAnimation(shake);
                        binding.resultTV.setVisibility(View.GONE);
                    } else if (binding.rePasswordTIET.getText().toString().isEmpty()) {
                        binding.rePasswordTIET.startAnimation(shake);
                        binding.resultTV.setVisibility(View.GONE);
                    } else if (!validatePassword(binding.newPasswordTIET)) {
                        binding.resultTV.setVisibility(View.GONE);
                        binding.newPasswordTIET.setError("Invalid Password Type", null);
                        binding.newPasswordTIL.setHelperText(preferences.getString("pass-restrict-pattern", "Password Should Contain One Upper Letter(A-Z)\n One Small Letter (a-z) and a Special Character (!@#$%^&*)"));
                    } else if (binding.newPasswordTIET.getText().toString().equals(binding.rePasswordTIET.getText().toString())) {
                        // check if the user is not empty.
                        Log.e("change : userId: ", "userID  " + userId);
                        if (!userId.isEmpty()) {
                            binding.progressBar.setVisibility(View.VISIBLE);
                            UsersRepository.getInstance()
                                    .changePassword("/users/password",
                                            preferences.getString("token", ""),
                                            preferences.getString("refreshToken", ""),
                                            new ChangePassRequest(userId,
                                                    binding.currentPasswordTIET.getText().toString().trim(),
                                                    binding.newPasswordTIET.getText().toString()
                                            )
                                    ).observe(getViewLifecycleOwner(), changePassResponse -> {
                                if (changePassResponse != null && changePassResponse.isStatus()) {
                                    System.out.println("......" + changePassResponse.getMessage());
                                    systemNotifications
                                            .pushNotification(
                                                    changePassResponse.getMessage(),
                                                    getView(),
                                                    "ok",
                                                    getActivity(),
                                                    R.color.green

                                            );
                                    binding.newPasswordTIET.setText("");
                                    binding.rePasswordTIET.setText("");
                                    binding.currentPasswordTIET.setText("");

                                    //   redirect user back to settings page once the password updated!
                                    NavigationHandler.navigate(getView(), ChangePasswordFragmentDirections.actionChangePasswordToSettings().getActionId(), null);


                                } else if (changePassResponse != null && !changePassResponse.isStatus()) {
                                    systemNotifications
                                            .pushNotification(
                                                    changePassResponse.getErrors().getErrorMessage(),
                                                    getView(),
                                                    "dismiss",
                                                    getActivity(),
                                                    R.color.red

                                            );
                                    System.out.println("...." + changePassResponse.getErrors().getErrorMessage());
                                }

                            });
                            binding.progressBar.setVisibility(View.GONE);
                        }
                        binding.resultTV.setVisibility(View.GONE);
                    } else {
                        binding.rePasswordTIET.startAnimation(shake);
                        binding.newPasswordTIET.startAnimation(shake);
                        binding.resultTV.setVisibility(View.VISIBLE);
                        binding.resultTV.setText("Passwords don't match");
                    }


                }
        );


    }

    private boolean validatePassword(TextInputEditText filed) {
        return Pattern
                .compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[@#$%^&+=])(?=.*[A-Z]).{6,}$")
                .matcher(filed.getText().toString().trim()).matches();
    }

}