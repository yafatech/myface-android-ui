package com.firstmotion.myface.view.ui.aboutus;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.AboutUsFragmentBinding;
import com.firstmotion.myface.utils.NavigationHandler;

public class AboutUsFragment extends Fragment {
    private AboutUsFragmentBinding binding;
    private AboutUsViewModel aboutUsViewModel;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.about_us_fragment, container, false);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا") || preferences.getString("langCode", "").contains("و")) {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.backSettingsBTN.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.inverted_back_icon));
        } else {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.backSettingsBTN.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.back_icon));
        }

        binding.backSettingsBTN.setOnClickListener((v) -> {
            NavigationHandler.navigate(getView(), AboutUsFragmentDirections.actionAboutUsToSettings().getActionId(), null);
        });
        return binding.getRoot();
    }

}
