
package com.firstmotion.myface.view.ui.splash;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.firstmotion.myface.R;
import com.firstmotion.myface.networking.models.AppLangGroup;
import com.firstmotion.myface.networking.models.AppSettings;
import com.firstmotion.myface.networking.repositories.SplashScreenRepository;
import com.firstmotion.myface.utils.AppUtils;
import com.firstmotion.myface.utils.RegionsResolver;
import com.firstmotion.myface.utils.systemnotifications.SystemNotification;
import com.google.gson.Gson;

import java.util.List;
import java.util.Locale;

public class SplashFragment extends Fragment {
    private NavController navController;
    private List<AppLangGroup> appLangGroupList;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static final SystemNotification systemNotifications = SystemNotification.build();


    public static SplashFragment newInstance() {
        return new SplashFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        com.firstmotion.myface.databinding.SplashFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.splash_fragment, container, false);
        return binding.getRoot();
    }


    //    @SuppressLint("CommitPrefEdits")
    @SuppressLint("CommitPrefEdits")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
        /*
         *  if token exist: -> Home.
         *  if token not exist and app settings loaded before -> login.
         *  else -> get settings from api -> welcome.
         */

        if (!preferences.getString("token", "").isEmpty() && preferences.getBoolean("FourImagesUploadedSuccessfully", false)) {
            {
                if (preferences.getBoolean("fullUpdatedProfile", false))
                    navController.navigate(SplashFragmentDirections.actionSplashToHomeFragment());
                else
                    navController.navigate(SplashFragmentDirections.actionSplashToProfile());
            }
//            Toast.makeText(requireActivity(), String.valueOf(preferences.getBoolean("FourImagesUploadedSuccessfully", false)), Toast.LENGTH_SHORT).show();
        } else if (!preferences.getString("userId", "").isEmpty() && preferences.getString("token", "").isEmpty())
            if (!AppUtils.isNetworkAvailable(getContext())) {
                systemNotifications
                        .pushNotification(
                                preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                                getView(),
                                "dismiss",
                                getActivity(),
                                R.color.red
                        );
            } else
                navController.navigate(SplashFragmentDirections.actionSplashToLogin());
        else if (!preferences.getString("token", "").isEmpty() && !preferences.getBoolean("FourImagesUploadedSuccessfully", false))
            navController.navigate(SplashFragmentDirections.actionSplashToUploadImgOne());
        else {
            if (!AppUtils.isNetworkAvailable(getContext())) {
                systemNotifications
                        .pushNotification(
                                preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                                getView(),
                                "dismiss",
                                getActivity(),
                                R.color.red
                        );
            } else
                SplashScreenRepository.getInstance()
                        .getSettings("/app-settings/by-region", RegionsResolver.getRegion(Locale.getDefault().getDisplayCountry().toLowerCase()))
                        .observe(getViewLifecycleOwner(), response -> {
                            if (response != null && response.isStatus()) {
                                // we have success Data.
                                for (AppSettings appSetting : response.getResults()) {
                                    appLangGroupList = appSetting.getAppLangGroup();
                                    //save app language group list in shared preferences
                                    saveData(appLangGroupList);

                                    editor.putString("imgUrl", appSetting.getImgUrl()).apply();
                                    editor.putString("systemColorCode", appSetting.getSystemColorCode()).apply();
                                    editor.putString("region", appSetting.getRegion()).apply();
//                                    editor.putString("facebook-page-link", appSetting.getFacebookLink).apply();
//                                    editor.putString("whatsapp-link", appSetting.getWhatsAppLink).apply();
//                                    editor.putString("instagram-link", appSetting.getInstagram).apply();
                                }
                                if (!AppUtils.isNetworkAvailable(getContext())) {
                                    systemNotifications
                                            .pushNotification(
                                                    preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                                                    getView(),
                                                    "dismiss",
                                                    getActivity(),
                                                    R.color.red
                                            );
                                } else
                                    navController.navigate(SplashFragmentDirections.actionSplashToWelcome());
                            } else if (response != null) {
                                systemNotifications
                                        .pushNotification(
                                                "can't Get App Settings: " + response.getErrors().getErrorMessage(),
                                                getView(),
                                                "dismiss",
                                                getActivity(),
                                                R.color.red
                                        );
                            }


                        });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void saveData(List<AppLangGroup> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("AppLangGroupList", json).apply();
    }


}







