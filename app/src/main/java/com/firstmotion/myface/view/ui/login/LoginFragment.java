package com.firstmotion.myface.view.ui.login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.LoginFragmentBinding;
import com.firstmotion.myface.networking.api.NetworkClientBuilder;
import com.firstmotion.myface.networking.models.ApiResponse;
import com.firstmotion.myface.networking.models.reponses.LoadUserProfileResponse;
import com.firstmotion.myface.networking.models.reponses.LoginResponse;
import com.firstmotion.myface.networking.models.requests.LoginRequest;
import com.firstmotion.myface.networking.models.requests.LoginSocialRequest;
import com.firstmotion.myface.networking.models.requests.RegistrationRequest;
import com.firstmotion.myface.networking.models.requests.RegistrationSocialRequest;
import com.firstmotion.myface.networking.repositories.UsersRepository;
import com.firstmotion.myface.networking.resources.ApiUrls;
import com.firstmotion.myface.utils.AppUtils;
import com.firstmotion.myface.utils.NavigationHandler;
import com.firstmotion.myface.utils.systemnotifications.SystemNotification;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class LoginFragment extends Fragment {
    private static final boolean isSocial = false;
    private LoginFragmentBinding binding;
    private static GoogleSignInAccount account;
    private NavController navController;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static PhoneAuthCredential globalPhoneAuthCredential;
    private Animation slidDown;

    private List<EditText> editTextList;
    private CountDownTimer countDownTimer;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    private GoogleSignInClient mGoogleSignInClient;
    private final int RC_SIGN_IN = 101;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private static final SystemNotification systemNotifications = SystemNotification.build();

    private FirebaseAuth mAuth;
    //FACEBOOK
    //values that represent what to get from user facebook account.
    private static final String EMAIL = "email";
    private static final String FB_PROFILE = "public_profile";
    private CallbackManager callbackManager;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false);
        return binding.getRoot();
    }

    public String getCode() {
        try {
            StringBuilder code = new StringBuilder();
            for (EditText item :
                    editTextList) {
                code.append(item.getText().toString());
            }
            return code.toString();
        } catch (Exception ignore) {
            return "0000";
        }
    }

    private static final NetworkClientBuilder networkClient = NetworkClientBuilder.defaults();

    private void startPhoneNumberVerification(String phoneNumber) {
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(mAuth)
                        .setPhoneNumber(phoneNumber)       // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(requireActivity())                 // Activity (for callback binding)
                        .setCallbacks(mCallbacks)          // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);

    }

    private boolean validatePhoneNumber() {
        String phoneNumber = Objects.requireNonNull(binding.phoneTIET.getText()).toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            binding.phoneTIET.setError("Invalid phone number.");
            return false;
        }

        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        System.out.println("[.] google account: 1 ->" + requestCode);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            System.out.println("[.] google account:2 -> " + task);
            handleSignInResult(task);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        //The SMS message sent by Firebase can also be localized by specifying the auth language via the setLanguageCode method on your Auth instance. auth.setLanguageCode("fr");
        // To apply the default app language instead of explicitly setting it.
        mAuth.useAppLanguage();

        // Initialize phone auth callbacks
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.

                String code = phoneAuthCredential.getSmsCode();
                if (code != null && !code.isEmpty())
                    // create a new Account for none Existing User.
                    socialSignUpAfterPhoneVerification(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...

                }
                systemNotifications.pushNotification(
                        "err -> " + e.getMessage(),
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
                // Show a message and update the UI
                // ...

            }

            @Override
            public void onCodeSent(@NonNull String verificationId, @NonNull PhoneAuthProvider.ForceResendingToken token) {
                super.onCodeSent(verificationId, token);
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                // Save verification ID and resending token so we can use them later
//                mVerificationId = verificationId;
                mResendToken = token;
                systemNotifications.pushNotification(
                        "Verification Code sent!",
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.green
                );

                binding.phoneNumGroup.setVisibility(View.GONE);
                binding.verificationGroup.setVisibility(View.VISIBLE);
                startResendTimer(60);

            }
        };


    }

    public void startResendTimer(int seconds) {
        binding.timerTV.setVisibility(View.VISIBLE);
        binding.resendTV.setEnabled(false);
        binding.resendTV.setTextColor(getResources().getColor(R.color.light_gray));


        countDownTimer = new CountDownTimer(seconds * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                String secondsString = Long.toString(millisUntilFinished / 1000);
                if (millisUntilFinished < 10000) {
                    secondsString = "0" + secondsString;
                }
                binding.timerTV.setText(" (0:" + secondsString + ")");
            }

            public void onFinish() {
            }
        }.start();
    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            System.out.println(" [.] google account: " + account);
            updateUI(account);
        } catch (ApiException e) {
            e.printStackTrace();
            System.out.println("[.] google account: 5 -> " + e.getMessage());
//            updateUI(null);
        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) requireActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @SuppressLint({"CommitPrefEdits", "SetTextI18n"})
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
        binding.txtOTP1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.txtOTP1.getText().toString().length() == 1) {
                    binding.txtOTP1.clearFocus();
                    binding.txtOTP2.requestFocus();
                    binding.txtOTP2.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.txtOTP1.getText().toString().length() == 0) {
                    binding.txtOTP1.requestFocus();
                }
            }
        });

        binding.txtOTP2.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.txtOTP2.getText().toString().length() == 1) {
                    binding.txtOTP2.clearFocus();
                    binding.txtOTP3.requestFocus();
                    binding.txtOTP3.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.txtOTP2.getText().toString().length() == 0) {
                    binding.txtOTP2.requestFocus();
                }
            }
        });

        binding.txtOTP3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.txtOTP3.getText().toString().length() == 1) {
                    binding.txtOTP3.clearFocus();
                    binding.txtOTP4.requestFocus();
                    binding.txtOTP4.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.txtOTP3.getText().toString().length() == 0) {
                    binding.txtOTP3.requestFocus();
                }
            }
        });

        binding.txtOTP4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.txtOTP4.getText().toString().length() == 1) {
                    binding.txtOTP4.clearFocus();
                    binding.txtOTP5.requestFocus();
                    binding.txtOTP5.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.txtOTP4.getText().toString().length() == 0) {
                    binding.txtOTP4.requestFocus();
                }
            }
        });

        binding.txtOTP5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.txtOTP5.getText().toString().length() == 1) {
                    binding.txtOTP5.clearFocus();
                    binding.txtOTP6.requestFocus();
                    binding.txtOTP6.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.txtOTP5.getText().toString().length() == 0) {
                    binding.txtOTP5.requestFocus();
                }
            }
        });

        binding.txtOTP6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.txtOTP6.getText().toString().length() == 1) {
                    binding.txtOTP6.clearFocus();
                    binding.confirmTV.requestFocus();
                    hideKeyboard(binding.txtOTP6);
                    if (getCode().length() == 6) {
                        binding.timerTV.setVisibility(View.GONE);
                        binding.confirmTV.setBackgroundColor(getActivity().getResources().getColor(R.color.green));
                        binding.confirmTV.setClickable(true);
                        countDownTimer.cancel();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.txtOTP6.getText().toString().length() == 0) {
                    binding.txtOTP6.requestFocus();
                }
            }
        });
        Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        slidDown = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down);
        binding.ccp.registerPhoneNumberTextView(binding.phoneTIET);

        // set Hint of TextInputs and Texts as translation values.
        binding.usernameTIL.setHint(preferences.getString("user-name", "User Name"));
        binding.passwordTIL.setHint(preferences.getString("pass-word", "Password"));
        binding.continueTV.setText(preferences.getString("continue", "CONTINUE"));
        binding.forgetPasswordTV.setText(preferences.getString("forget-pass", "FORGET PASSWORD"));
        binding.signInButton.setText(preferences.getString("continue-with-google", "CONTINUE WITH GOOGLE"));
        binding.signInTV.setText(preferences.getString("sign-in", "SIGN IN"));
        binding.createAccountTV.setText(preferences.getString("create-account", "CREATE NEW ACCOUNT"));
        if (preferences.getString("langCode", "").contains("ar"))
            view.setTextDirection(View.TEXT_DIRECTION_RTL);
        else
            view.setTextDirection(View.TEXT_DIRECTION_LTR);

        // Set the dimensions of the sign-in button.
        binding.signInButton.setOnClickListener(v -> {
            hideKeyboard(v);
            signIn();
        });
        binding.continueTV.setOnClickListener(v -> {
            if (binding.usernameTIET.getText().toString().trim().isEmpty())
                binding.usernameTIET.startAnimation(shake);
            if (binding.passwordTIET.getText().toString().trim().isEmpty())
                binding.passwordTIET.startAnimation(shake);

            else if (AppUtils.isNetworkAvailable(requireContext())) {

                // start login.
                UsersRepository.getInstance()
                        .login(ApiUrls.API_URL + "/auth/login", new LoginRequest(binding.usernameTIET.getText().toString(),
                                binding.passwordTIET.getText().toString())).observe(getViewLifecycleOwner(), loginResponse -> {
//                    System.out.println("login bad user: " + loginResponse);
                    if (loginResponse != null && loginResponse.isStatus()) {
                        // if login is success save token --> load user information --> go to home
                        editor.putString("token", loginResponse.getResults().getToken()).apply();
                        editor.putString("username", binding.usernameTIET.getText().toString()).apply();
                        editor.putString("refreshToken", loginResponse.getResults().getRefreshToken()).apply();
                        systemNotifications
                                .pushNotification(
                                        loginResponse.getMessage(),
                                        getView(),
                                        "ok",
                                        getActivity(),
                                        R.color.green
                                );
                        if (!preferences.getBoolean("FourImagesUploadedSuccessfully", false)) {

                            NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToUploadImgOne().getActionId(), null);
                        } else {
                            if (preferences.getBoolean("fullUpdatedProfile", false))
                                NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToHomeFragment().getActionId(), null);
                            else
                                NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToProfile().getActionId(), null);
                        }
                    } else {
                        if (loginResponse == null)
                            systemNotifications
                                    .pushNotification(
                                            "User { " + binding.usernameTIET.getText().toString() + " } not Found",
                                            getView(),
                                            "dismiss",
                                            getActivity(),
                                            R.color.red
                                    );
                        else
                            systemNotifications
                                    .pushNotification(
                                            loginResponse.getErrors().getErrorMessage(),
                                            getView(),
                                            "dismiss",
                                            getActivity(),
                                            R.color.red
                                    );
                    }

                });

                hideKeyboard(v);

            } else
                systemNotifications.pushNotification(
                        preferences.getString("no-Internet", "the phone is not connected to the internet!"),
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
        });

        // when the user clicked the verify button
        binding.verifyTV.setOnClickListener(v -> {
            hideKeyboard(v);
            if (binding.ccp.getFullNumber().isEmpty()) {
                systemNotifications.pushNotification(
                        "PhoneNumber Can't Be Empty",
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
                return;
            } else if (!validatePhoneNumber()) {
                systemNotifications.pushNotification(
                        "PhoneNumber Format is wrong!",
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
                return;
            }
            if (AppUtils.isNetworkAvailable(requireContext()))
                // Check if the phone number is already exist in the backend
                UsersRepository.getInstance()
                        .checkPhoneNumber(ApiUrls.API_URL + "/users/verify-phone", binding.ccp.getFullNumber())
                        .observe(getViewLifecycleOwner(), phoneExistResponseApiResponse -> {
                            if (phoneExistResponseApiResponse != null && phoneExistResponseApiResponse.isStatus()) {
                                // phone exist.
                                systemNotifications.pushNotification(
                                        "phone Number { +" + binding.ccp.getFullNumber() + " } Already taken",
                                        getView(),
                                        "dismiss",
                                        getActivity(),
                                        R.color.red
                                );
                            } else {
                                // disable the verify button/
                                binding.verifyTV.setClickable(false);
                                binding.verifyTV.setBackgroundColor(getActivity().getResources().getColor(R.color.gray));
                                binding.progressBar.setVisibility(View.VISIBLE);
                                // phone exist.
                                systemNotifications.pushNotification(
                                        "verifying { " + binding.ccp.getFullNumber() + " }",
                                        getView(),
                                        "dismiss",
                                        getActivity(),
                                        R.color.green
                                );
                                startPhoneNumberVerification("+" + binding.ccp.getFullNumber());
                            }
                        });
            else
                systemNotifications.pushNotification(
                        preferences.getString("no-Internet", "the phone is not connected to the internet!"),
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
        });


        // start to confirm the user
        binding.confirmTV.setOnClickListener(v -> {
            binding.timerTV.setVisibility(View.GONE);
            countDownTimer.cancel();
            binding.progressBar.setVisibility(View.VISIBLE);
            socialSignUpAfterPhoneVerification(globalPhoneAuthCredential);
        });

        binding.forgetPasswordTV.setOnClickListener(v -> navController.navigate(LoginFragmentDirections.actionLoginToResetPassword()));
        binding.createAccountTV.setOnClickListener(v -> navController.navigate(LoginFragmentDirections.actionLoginToSignUp()));

        //  FACEBOOK.
        TextView loginButton = binding.loginButton;
        if (preferences.getString("langCode", "").contains("ar")) {
            loginButton.setText("تسجيل عبر فيسبوك");
        }
        callbackManager = CallbackManager.Factory.create();
        loginButton.setOnClickListener((v) -> {
            // Set permissions
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(EMAIL, FB_PROFILE));

            // Callback registration
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    // App code
                    if (loginResult.getAccessToken() != null) {
                        RequestData(loginResult.getAccessToken());
                    }
                }

                @Override
                public void onCancel() {
                    // App code
                    Log.e("TAG LOGIN", "::::: cancel :::::: ");
                }

                @Override
                public void onError(FacebookException exception) {
                    // App code
                    Log.e("TAG LOGIN", "onError: " + exception.getMessage());
                }
            });
        });

    }

    private void localLogin(ApiResponse<LoginResponse> res, String username) {
        // if login is success save token --> load user information --> go to home
        editor.putBoolean("isSocial", true).apply();
        editor.putString("token", res.getResults().getToken()).apply();
        editor.putString("username", username).apply();
        editor.putString("refreshToken", res.getResults().getRefreshToken()).apply();
        // method to load the User Information after Social login in flavor of the upload four images .
        loadUserInformation(res.getResults().getToken(),
                res.getResults().getRefreshToken(),
                username);
        systemNotifications.pushNotification(
                res.getMessage(),
                getView(),
                "ok",
                getActivity(),
                R.color.green
        );
        if (!preferences.getBoolean("FourImagesUploadedSuccessfully", false)) {
            NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToUploadImgOne().getActionId(), null);
        } else {
            if (preferences.getBoolean("fullUpdatedProfile", false))
                NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToHomeFragment().getActionId(), null);
            else
                NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToProfile().getActionId(), null);
        }
    }

    private void RequestData(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                (object, response) -> {
                    // Application code
                    final JSONObject json = response.getJSONObject();
                    if (json != null) {
                        try {
                            String email = json.getString("email");
                            String username = email.substring(0, email.indexOf('@'));
//                            String picture = json.getString("picture");
//                            String name = json.getString("name");
//                            System.out.println("::::::::: json :::::::" + json);
//                            System.out.println("::::::::: email :::::::" + email);
//                            System.out.println("::::::::: name :::::::" + name);
//                            System.out.println("::::::::: picture :::::::" + picture);


                            // login the user,
                            UsersRepository.getInstance().login(ApiUrls.API_URL + "/auth/login", new LoginRequest(username, email))
                                    .observe(getViewLifecycleOwner(), res -> {
                                        if (res != null && res.isStatus()) {
                                            // we have a registered user already!
                                            // push its info then redirect to home.
                                            System.out.println("::::::::::::: login success ::::::::::::::");
                                            System.out.println("::::::::::::: res ::::::::::::::" + res);
                                            // login the user
                                            localLogin(res, username);

                                        } else {
//                                            System.out.println("::::::::::::: login error ::::::::::::::");
                                            // we need to register the user.
                                            UsersRepository.getInstance().registerUser("/users",
                                                    //  username,  password,  email,  phoneNumber,  role
                                                    new RegistrationRequest(username, email, email, "apple.phone.myface", "CLIENT")
                                            ).observe(getViewLifecycleOwner(), register -> {
//                                                System.out.println("::::::::::::: register ::::::::::::::" + register);
                                                // check the status of the registration.
                                                if (register != null && register.isStatus()) {
                                                    // register success redirect to home.
//                                                    System.out.println("::::::::::::: register success ::::::::::::::");
                                                    editor.putString("userId", register.getResults().getUserId()).apply();
                                                    editor.putString("username", register.getResults().getUsername()).apply();
                                                    editor.putString("email", username).apply();
                                                    editor.putString("phone", binding.ccp.getNumber()).apply();
                                                    editor.putString("ccp", binding.ccp.getSelectedCountryNameCode()).apply();
                                                    editor.putBoolean("accountStatus", register.getResults().isAccountStatus()).apply();
                                                    editor.putBoolean("emailVerified", register.getResults().isEmailVerified()).apply();
                                                    systemNotifications.pushNotification(
                                                            register.getMessage(),
                                                            getView(),
                                                            "ok",
                                                            getActivity(),
                                                            R.color.green
                                                    );
                                                    // login the user after registration.
                                                    UsersRepository.getInstance()
                                                            .login(ApiUrls.API_URL + "/auth/login", new LoginRequest(username, email))
                                                            .observe(getViewLifecycleOwner(), loginResponse -> {
                                                                if (loginResponse != null && loginResponse.isStatus()) {
                                                                    // if login is success save token --> load user information --> go to home
                                                                    editor.putBoolean("isSocial", true).apply();
                                                                    editor.putString("token", loginResponse.getResults().getToken()).apply();
                                                                    editor.putString("username", username).apply();
                                                                    editor.putString("refreshToken", loginResponse.getResults().getRefreshToken()).apply();
                                                                    systemNotifications
                                                                            .pushNotification(
                                                                                    loginResponse.getMessage(),
                                                                                    getView(),
                                                                                    "ok",
                                                                                    getActivity(),
                                                                                    R.color.green
                                                                            );
                                                                    if (!preferences.getBoolean("FourImagesUploadedSuccessfully", false)) {
                                                                        NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToUploadImgOne().getActionId(), null);
                                                                    } else {
                                                                        if (preferences.getBoolean("fullUpdatedProfile", false))
                                                                            NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToHomeFragment().getActionId(), null);
                                                                        else
                                                                            NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToProfile().getActionId(), null);
                                                                    }
                                                                } else {
                                                                    if (loginResponse == null)
                                                                        systemNotifications
                                                                                .pushNotification(
                                                                                        "User { " + username + " } not Found",
                                                                                        getView(),
                                                                                        "dismiss",
                                                                                        getActivity(),
                                                                                        R.color.red
                                                                                );
                                                                    else
                                                                        systemNotifications
                                                                                .pushNotification(
                                                                                        loginResponse.getErrors().getErrorMessage(),
                                                                                        getView(),
                                                                                        "dismiss",
                                                                                        getActivity(),
                                                                                        R.color.red
                                                                                );
                                                                }

                                                            });
                                                } else {
                                                    // register have issue.
                                                    // show the user the error.
//                                                    System.out.println("::::::::::::: register error::::::::::::::");
                                                    systemNotifications.pushNotification(
                                                            "oops, there is an issue completing your request, please try again later...",
                                                            getView(),
                                                            "dismiss",
                                                            getActivity(),
                                                            R.color.red
                                                    );
                                                }
                                            });
                                        }
                                    });

                            // if not exist create account.
                        } catch (JSONException e) {
                            // error getting the json obj.
                            Log.e("TAG LOGIN", "::::::::: eee :::::::" + e.getMessage());

                        }

                    }

                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();

    }


    private void updateUI(GoogleSignInAccount googleSignInAccount) {
        if (googleSignInAccount != null) {
            if (!AppUtils.isNetworkAvailable(getContext())) {
                systemNotifications.pushNotification(
                        preferences.getString("no-Internet", "the phone is not connected to the internet!"),
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
                return;
            }
            // Do Normal Login With the Apis.
            // social login
            UsersRepository.getInstance()
                    .loginSocial(ApiUrls.API_URL + "/auth/social/login", new LoginSocialRequest(googleSignInAccount.getIdToken()))
                    .observe(getViewLifecycleOwner(), loginResponseApiResponse -> {
                        if (loginResponseApiResponse != null && loginResponseApiResponse.isStatus()) {
                            // if login is success save token --> load user information --> go to home
                            editor.putBoolean("isSocial", true).apply();
                            editor.putString("token", loginResponseApiResponse.getResults().getToken()).apply();
                            editor.putString("username", googleSignInAccount.getEmail().split("@")[0]).apply();
                            editor.putString("refreshToken", loginResponseApiResponse.getResults().getRefreshToken()).apply();
                            // method to load the User Information after Social login in flavor of the upload four images .
                            loadUserInformation(loginResponseApiResponse.getResults().getToken(),
                                    loginResponseApiResponse.getResults().getRefreshToken(),
                                    googleSignInAccount.getEmail().split("@")[0]);
                            systemNotifications.pushNotification(
                                    loginResponseApiResponse.getMessage(),
                                    getView(),
                                    "ok",
                                    getActivity(),
                                    R.color.green
                            );
                            if (!preferences.getBoolean("FourImagesUploadedSuccessfully", false))
                                NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToUploadImgOne().getActionId(), null);
                            else {
                                if (preferences.getBoolean("fullUpdatedProfile", false))
                                    NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToHomeFragment().getActionId(), null);
                                else
                                    NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToProfile().getActionId(), null);
                            }
                        } else {
                            binding.signInGroup.setVisibility(View.GONE);
                            binding.phoneNumGroup.setVisibility(View.VISIBLE);
                        }
                    });

        } else {
            //If GoogleSignIn.getLastSignedInAccount returns null, the user has not yet signed in to your app with Google.
            //Update your UI to display the Google Sign-in button.
            systemNotifications.pushNotification(
                    "Google Account is not Valid",
                    getView(),
                    "dismiss",
                    getActivity(),
                    R.color.red
            );
        }
    }

    private void loadUserInformation(String token, String refreshToken, String username) {
//        System.out.println("load info...");
        Map<String, String> extras = new HashMap<>();
        extras.put("token", token);
        extras.put("refreshToken", refreshToken);
        Thread thread = new Thread(() -> {
            ApiResponse<LoadUserProfileResponse> apiResponse = networkClient
                    .url(ApiUrls.API_URL + "/users/profile" + "?username=" + username)
                    .headers(extras)
                    .then()
                    .get(new TypeToken<ApiResponse<LoadUserProfileResponse>>() {
                    }.getType(), true);

            System.out.println(" [.] res::: " + apiResponse);
            if (apiResponse != null) {
                try {
                    editor.putString("userId", apiResponse.getResults().getUserId()).apply();
                } catch (Exception ignored) {

                }

            }
        });
        thread.start();

    }

    private void socialSignUpAfterPhoneVerification(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(requireActivity(), task -> {
                    if (task.isSuccessful()) {
                        // make a request to the backend to create a new user entry then push the credentials to local store.
                        if (account == null)
                            systemNotifications.pushNotification(
                                    "Please Choose Google Account to Start registration process.",
                                    getView(),
                                    "dismiss",
                                    getActivity(),
                                    R.color.red
                            );
                        else {
                            UsersRepository.getInstance()
                                    .registrationSocial("/users/social/register",
                                            // String authToken, String phoneNumber
                                            new RegistrationSocialRequest(account.getIdToken(), binding.ccp.getFullNumber()))
                                    .observe(getViewLifecycleOwner(), socialRegistrationResponse -> {
                                        if (socialRegistrationResponse != null && socialRegistrationResponse.isStatus()) {
                                            // its a new  google account then just register it.
                                            editor.putString("userId", socialRegistrationResponse.getResults().getUserId()).apply();
                                            editor.putString("username", socialRegistrationResponse.getResults().getUsername()).apply();
                                            editor.putString("email", account.getEmail().split("@")[0]).apply();
                                            editor.putString("phone", binding.ccp.getNumber()).apply();
                                            editor.putString("ccp", binding.ccp.getSelectedCountryNameCode()).apply();
                                            editor.putBoolean("accountStatus", socialRegistrationResponse.getResults().isAccountStatus()).apply();
                                            editor.putBoolean("emailVerified", socialRegistrationResponse.getResults().isEmailVerified()).apply();
                                            systemNotifications.pushNotification(
                                                    socialRegistrationResponse.getMessage(),
                                                    getView(),
                                                    "ok",
                                                    getActivity(),
                                                    R.color.green
                                            );

                                            if (!socialRegistrationResponse.getResults().getUsername().isEmpty()) {
                                                UsersRepository.getInstance()
                                                        .loginSocial(ApiUrls.API_URL + "/auth/social/login", new LoginSocialRequest(account.getIdToken()))
                                                        .observe(getViewLifecycleOwner(), loginResponseApiResponse -> {
                                                            if (loginResponseApiResponse != null && loginResponseApiResponse.isStatus()) {
                                                                // if login is success save token --> load user information --> go to home
                                                                editor.putBoolean("isSocial", true).apply();
                                                                editor.putString("token", loginResponseApiResponse.getResults().getToken()).apply();
                                                                editor.putString("username", account.getEmail().split("@")[0]).apply();
                                                                editor.putString("refreshToken", loginResponseApiResponse.getResults().getRefreshToken()).apply();
                                                                // method to load the User Information after Social login in flavor of the upload four images .
                                                                loadUserInformation(loginResponseApiResponse.getResults().getToken(),
                                                                        loginResponseApiResponse.getResults().getRefreshToken(),
                                                                        account.getEmail().split("@")[0]);
                                                                systemNotifications.pushNotification(
                                                                        loginResponseApiResponse.getMessage(),
                                                                        getView(),
                                                                        "ok",
                                                                        getActivity(),
                                                                        R.color.green
                                                                );
                                                                if (!preferences.getBoolean("FourImagesUploadedSuccessfully", false)) {
                                                                    NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToUploadImgOne().getActionId(), null);
                                                                } else {
                                                                    if (preferences.getBoolean("fullUpdatedProfile", false))
                                                                        NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToHomeFragment().getActionId(), null);
                                                                    else
                                                                        NavigationHandler.navigate(getView(), LoginFragmentDirections.actionLoginToProfile().getActionId(), null);
                                                                }
                                                            } else {
                                                                mGoogleSignInClient.signOut();
                                                                mGoogleSignInClient.revokeAccess();
                                                            }
                                                        });
                                            }


                                        } else {
                                            systemNotifications.pushNotification(
                                                    "registration error: " + socialRegistrationResponse.getErrors().getErrorMessage() + " Please Login Instead",
                                                    getView(),
                                                    "dismiss",
                                                    getActivity(),
                                                    R.color.red
                                            );

                                            // if this is the case then we have a registered user already then just redirect to login.

                                            // clean up the google user object.
                                            mAuth.signOut();
                                            mGoogleSignInClient.signOut();
                                            mGoogleSignInClient.revokeAccess();

                                            // redirect to login.
//                                            navController.navigate(LoginFragmentDirections.actl());
                                            // set Visability
                                            binding.signInGroup.setVisibility(View.VISIBLE);
                                            binding.verificationGroup.setVisibility(View.GONE);

                                        }
                                    });
                        }

                    } else {
                        systemNotifications.pushNotification(
                                task.getException().getMessage(),
                                getView(),
                                "dismiss",
                                getActivity(),
                                R.color.red
                        );
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();


    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        binding.getRoot().startAnimation(slidDown);
    }
}