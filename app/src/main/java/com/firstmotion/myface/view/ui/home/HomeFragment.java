
package com.firstmotion.myface.view.ui.home;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.HomeFragmentBinding;
import com.firstmotion.myface.models.DetectFacesObject;
import com.firstmotion.myface.models.RecognizedPeopleResponse;
import com.firstmotion.myface.networking.api.NetworkClientBuilder;
import com.firstmotion.myface.utils.AppUtils;
import com.firstmotion.myface.utils.NavigationHandler;
import com.firstmotion.myface.utils.detection.system.CameraXViewModel;
import com.firstmotion.myface.utils.detection.system.FaceDetectorProcessor;
import com.firstmotion.myface.utils.detection.system.GraphicOverlay;
import com.firstmotion.myface.utils.detection.system.VisionImageProcessor;
import com.firstmotion.myface.utils.detection.utils.PreferenceUtils;
import com.firstmotion.myface.utils.systemnotifications.SystemNotification;
import com.google.mlkit.common.MlKitException;
import com.google.mlkit.vision.face.FaceDetectorOptions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;
import jp.wasabeef.blurry.Blurry;

public class HomeFragment extends Fragment {

    public static Context context;
    public static HomeFragmentBinding binding;
    public static NavController navController;
    private static SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static final String TAG = "MyFaceCamLivePreviewActivity";
    private final int lensFacing = CameraSelector.LENS_FACING_FRONT;
    private CameraSelector cameraSelector = CameraSelector.DEFAULT_FRONT_CAMERA;
    private PreviewView previewView;
    private GraphicOverlay graphicOverlay;
    public static ArrayList<String> image64 = new ArrayList<>();
    public static int homeF = 0;
    private static final SystemNotification systemNotifications = SystemNotification.build();
    private boolean needUpdateGraphicOverlayImageSourceInfo;
    private Animation slide_up;
    private Vibrator vib;
    @Nullable
    private static ProcessCameraProvider cameraProvider;
    @Nullable
    private ImageAnalysis analysisUseCase;
    public static FragmentActivity mainThread;
    @Nullable
    public static VisionImageProcessor imageProcessor;

    public static List<DetectFacesObject.ImageData> imageDataList = new ArrayList<>();
    @Nullable
    private Preview previewUseCase;
    public static String userId;
    public static String token;
    public static String refreshToken;
    @SuppressLint("StaticFieldLeak")
    public static View homeView;
    public static FragmentActivity homeActivity;

    @SuppressLint("LongLogTag")
    private static boolean isPermissionGranted(Context context, String permission) {
        if (ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission granted: " + permission);
            return true;
        }
        Log.i(TAG, "Permission NOT granted: " + permission);
        return false;
    }


    @SuppressLint("StaticFieldLeak")
    private static View homeFragmentView;


    private boolean allPermissionsGranted() {
        for (String permission : getRequiredPermissions()) {
            if (!isPermissionGranted(getContext(), permission)) {
                return false;
            }
        }
        return true;
    }


    public void bindAllCameraUseCases() {
        if (cameraProvider != null) {
            try {
                // As required by CameraX API, unbinds all use cases before trying to re-bind any of them.
                cameraProvider.unbindAll();
                bindPreviewUseCase();
                bindAnalysisUseCase();
            } catch (Exception ignored) {

            }

        }
    }

    @SuppressLint("LongLogTag")
    private void bindAnalysisUseCase() {
        if (cameraProvider == null) {
            return;
        }
        if (analysisUseCase != null) {
            cameraProvider.unbind(analysisUseCase);
        }
        if (imageProcessor != null) {
            imageProcessor.stop();
        }

        try {
            Log.i(TAG, "Using Face Detector Processor");
            FaceDetectorOptions faceDetectorOptions =
                    PreferenceUtils.getFaceDetectorOptionsForLivePreview(getContext());
            imageProcessor = new FaceDetectorProcessor(context, faceDetectorOptions);

        } catch (Exception e) {
            systemNotifications
                    .pushNotification(
                            "Can not create image processor: " + e.getLocalizedMessage(),
                            getView(),
                            "dismiss",
                            getActivity(),
                            R.color.red
                    );
            return;
        }

        ImageAnalysis.Builder builder = new ImageAnalysis.Builder();
        Size targetResolution = PreferenceUtils.getCameraXTargetResolution(requireContext());
        if (targetResolution != null) {
            builder.setTargetResolution(targetResolution);
        }
        analysisUseCase = builder.build();

        needUpdateGraphicOverlayImageSourceInfo = true;
        analysisUseCase.setAnalyzer(
                // imageProcessor.processImageProxy will use another thread to run the detection underneath,
                // thus we can just runs the analyzer itself on main thread.
                ContextCompat.getMainExecutor(context),
                imageProxy -> {
                    if (needUpdateGraphicOverlayImageSourceInfo) {
                        boolean isImageFlipped = lensFacing == CameraSelector.LENS_FACING_FRONT;
                        int rotationDegrees = imageProxy.getImageInfo().getRotationDegrees();
                        if (rotationDegrees == 0 || rotationDegrees == 180) {
                            graphicOverlay.setImageSourceInfo(
                                    imageProxy.getWidth(), imageProxy.getHeight(), isImageFlipped);
                        } else {
                            graphicOverlay.setImageSourceInfo(
                                    imageProxy.getHeight(), imageProxy.getWidth(), isImageFlipped);
                        }
                        needUpdateGraphicOverlayImageSourceInfo = false;
                    }
                    try {
                        imageProcessor.processImageProxy(imageProxy, graphicOverlay);
                    } catch (MlKitException e) {
                        systemNotifications
                                .pushNotification(
                                        e.getLocalizedMessage(),
                                        getView(),
                                        "dismiss",
                                        getActivity(),
                                        R.color.red
                                );
                    }
                });

        cameraProvider.bindToLifecycle(/* lifecycleOwner= */ this, cameraSelector, analysisUseCase);
    }

    public void showProgress(HomeFragmentBinding binding1, int visibility) {
        binding1.progressBar.setVisibility(visibility);
    }

    private void bindPreviewUseCase() {
        if (!PreferenceUtils.isCameraLiveViewportEnabled(getContext())) {
            return;
        }
        if (cameraProvider == null) {
            return;
        }
        if (previewUseCase != null) {
            cameraProvider.unbind(previewUseCase);
        }

        Preview.Builder builder = new Preview.Builder();
        Size targetResolution = PreferenceUtils.getCameraXTargetResolution(requireContext());
        if (targetResolution != null) {
            builder.setTargetResolution(targetResolution);
        }
        previewUseCase = builder.build();
        previewUseCase.setSurfaceProvider(previewView.getSurfaceProvider());
        cameraProvider.bindToLifecycle(/* lifecycleOwner= */ this, cameraSelector, previewUseCase);
    }

    private String[] getRequiredPermissions() {
        try {
            PackageInfo info =
                    this.requireActivity().getPackageManager()
                            .getPackageInfo(this.requireActivity().getPackageName(), PackageManager.GET_PERMISSIONS);
            String[] ps = info.requestedPermissions;
            if (ps != null && ps.length > 0) {
                return ps;
            } else {
                return new String[0];
            }
        } catch (Exception e) {
            return new String[0];
        }
    }

    public void showInternetConnectionSlow(HomeFragmentBinding binding1) {
        new SweetAlertDialog(HomeFragment.context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
//                .setTitleText(":(")
                .setContentText(preferences.getString("slow-internet-message", "it seems that Your Internet Connection is Slow, want to Try again ?"))
                .setCustomImage(R.drawable.error_center_x)
                .setConfirmText(preferences.getString("try-again", "Try Again?"))
                .setCancelText(preferences.getString("leave", "Leave!"))
                .setCancelClickListener(listener -> {
                    listener.dismissWithAnimation();
                    NavigationHandler.navigate(homeFragmentView, HomeFragmentDirections.actionHomeFragmentToSplash().getActionId(), null);
                })
                .setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlertDialog.dismissWithAnimation();
                    navController.navigate(HomeFragmentDirections.actionHomeFragmentToSplash());
                    NavigationHandler.navigate(homeFragmentView, HomeFragmentDirections.actionHomeFragmentToSplash().getActionId(), null);
                })
                .show();
    }

    @SuppressLint({"RestrictedApi", "SetTextI18n"})
    public void showSweetDialog(HomeFragmentBinding binding) {
        binding.progressBar.setVisibility(View.GONE);
        if (cameraProvider != null && imageProcessor != null) {
            imageProcessor.stop();
            cameraProvider.unbindAll();
            cameraProvider.shutdown();
        }

        new SweetAlertDialog(HomeFragment.context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setContentText(preferences.getString("error-find-matching-message", "We Couldn't Find People That Match You, Come Back Again to Check :("))
                .setCustomImage(R.drawable.error_center_x)
                .setConfirmText(preferences.getString("try-again", "Try Again?"))
                .setCancelText(preferences.getString("leave", "Leave!"))
                .setCancelClickListener(listener -> {
                    listener.dismissWithAnimation();
                    NavigationHandler.navigate(homeFragmentView, HomeFragmentDirections.actionHomeFragmentToSplash().getActionId(), null);
                })
                .setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlertDialog.dismissWithAnimation();
                    NavigationHandler.navigate(homeFragmentView, HomeFragmentDirections.actionHomeFragmentToSplash().getActionId(), null);
                })
                .show();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("RestrictedApi")
    public void showSuccessResponse(int status, List<RecognizedPeopleResponse> peopleResponses, HomeFragmentBinding binding1) {
        final MediaPlayer soundWithLogo = MediaPlayer.create(context, R.raw.logo_audio);
        binding.logoAfterApproved.setVisibility(View.VISIBLE);
        binding.progressBar.setVisibility(View.GONE);
        slide_up = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        if (status == 1) {
            if (cameraProvider != null && imageProcessor != null) {
                imageProcessor.stop();
                cameraProvider.unbindAll();
                cameraProvider.shutdown();
            }

            // blur the background.
            Blurry.with(context)
                    .radius(10)
                    .sampling(30)
                    .color(Color.BLACK)
//                    .color(Color.argb(66, 255, 255, 0))
                    .async()
                    .animate(300)
                    .onto(binding1.previewView);

            vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            vib.vibrate(300);
            soundWithLogo.setVolume(0.5f, 0.5f);

            Glide.with(context)
//                    .asGif()
                    .load(R.drawable.logo_after_approved)
//                    .transition(withCrossFade())
//                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                    .into(new DrawableImageViewTarget(binding.logoAfterApproved) {
                        @Override
                        public void onResourceReady(@NotNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            if (resource instanceof GifDrawable) {
                                ((GifDrawable) resource).setLoopCount(1);
                                soundWithLogo.start();
                            }
                            super.onResourceReady(resource, transition);
                        }

                        @Override
                        public void onStop() {
                            super.onStop();
                        }

                    });
            final Timer t = new Timer();
            t.schedule(new TimerTask() {
                @Override
                public void run() {
                    homeActivity.runOnUiThread(() -> {
                        vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(300);
                    });
                }
            }, 6000);
            t.schedule(new TimerTask() {
                @Override
                public void run() {
                    homeActivity.runOnUiThread(() -> {
                        vib = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                        vib.vibrate(500);
                    });
                }
            }, 7500);
            t.schedule(new TimerTask() {
                @Override
                public void run() {
                    homeActivity.runOnUiThread(() -> {
                        binding.resultsContainer.startAnimation(slide_up);
                        binding.logoAfterApproved.setVisibility(View.GONE);
                        binding1.successResponse.setVisibility(View.GONE);

                        // show only 5 results
                        // this will be based on the User Subscription. Free With 5 results, Paid with extended results.
                        for (int i = 0; i <= peopleResponses.size() - 1; i++) {
                            if (i == 4)
                                break;
                            addLayout(binding1.root,
                                    (peopleResponses.get(i).getShowImage() != null && peopleResponses.get(i).getShowImage()) ? peopleResponses.get(i).getImgUrl() : preferences.getString("logoImgUrl", "https://eu-cdn.my-face.app/cdn/storage/media/load?origin=137cc1de-d135-468b-87ca-2ad3f5cd4bca"),
                                    peopleResponses.get(i).getCity(),
                                    peopleResponses.get(i).getCountry(),
                                    peopleResponses.get(i).getAge() + "",
                                    peopleResponses.get(i).getFullName(),
                                    peopleResponses.get(i).getPercentage(),
                                    binding1);
                        }

                    });

                }

            }, 9000);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("SetTextI18n")
    public void addLayout(LinearLayout root, String image, String city, String country, String age, String name, String per, HomeFragmentBinding binding1) {
        final View layout2;
        layout2 = LayoutInflater.from(context).inflate(R.layout.layout_like_user, root, false);
        ImageView profileImgV = layout2.findViewById(R.id.profileImgV);
        TextView name_user = layout2.findViewById(R.id.name_user);
        TextView percent = layout2.findViewById(R.id.percent);
        TextView countryTV = layout2.findViewById(R.id.country);

        binding1.resultsContainer.setVisibility(View.VISIBLE);

        if (country != null && city != null)
            countryTV.setText(country + " , " + city);
        if (name != null && age != null)
            name_user.setText(name + " ( " + age + " )");
        if (per != null) percent.setText(per);
        Bitmap bmp = NetworkClientBuilder.defaults()
                .then()
                .loadAsyncImage(image);
        profileImgV.setImageBitmap(bmp);
        root.addView(layout2);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false);
        homeF = 1;
        cameraSelector = new CameraSelector.Builder().requireLensFacing(lensFacing).build();
        context = getContext();
        binding.successResponse.setVisibility(View.GONE);
        mainThread = getActivity();
        return binding.getRoot();
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        homeFragmentView = getView();
        editor = preferences.edit();
        binding.repeat.setText(preferences.getString("repeat", "REPEAT"));
        binding.title.setText(preferences.getString("username", "Username"));
        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا") || preferences.getString("langCode", "").contains("و")) {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
        homeView = getView();
        homeActivity = getActivity();

        previewView = view.getRootView().findViewById(R.id.preview_view);
        // get the user id from the shared preferences to use it in face-recognition stage.
        userId = preferences.getString("userId", "");
        token = preferences.getString("token", "");
        refreshToken = preferences.getString("refreshToken", "");
        graphicOverlay = view.getRootView().findViewById(R.id.graphic_overlay);

//        getRequiredPermissions();
        new ViewModelProvider(this,
                ViewModelProvider.AndroidViewModelFactory.getInstance(this.requireActivity().getApplication()))
                .get(CameraXViewModel.class)

                .getProcessCameraProvider()
                .observe(
                        getViewLifecycleOwner(),
                        provider -> {
                            cameraProvider = provider;
                            if (allPermissionsGranted()) {
                                bindAllCameraUseCases();
                            }
                        });

        binding.settingsBtn.setOnClickListener(v -> {
            if (!AppUtils.isNetworkAvailable(context)) {
                systemNotifications.pushNotification(
                        preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
            }
            try {
                NavigationHandler.navigate(getView(), HomeFragmentDirections.actionHomeFragmentToSettings().getActionId(), null);
            } catch (Exception ignore) {

            }

//            navController.navigate(HomeFragmentDirections.actionHomeFragmentToSettings());
        });


        binding.repeat.setOnClickListener((v) -> NavigationHandler.navigate(getView(), HomeFragmentDirections.actionHomeFragmentToSplash().getActionId(), null));

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @SuppressLint("RestrictedApi")
    public void redirectToLogin() {
        if (imageProcessor != null && cameraProvider != null) {
            imageProcessor.stop();
            cameraProvider.unbindAll();
            cameraProvider.shutdown();
        }
        new SweetAlertDialog(HomeFragment.context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setContentText("Session Expired, Please Login!")
                .setCustomImage(R.drawable.error_center_x)
                .setConfirmText("GO")
                .setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlertDialog.dismissWithAnimation();
                    navController.navigate(HomeFragmentDirections.actionHomeFragmentToLogin());
                })
                .show();
    }
}