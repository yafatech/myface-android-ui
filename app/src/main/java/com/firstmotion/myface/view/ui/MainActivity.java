package com.firstmotion.myface.view.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowInsetsController;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.preference.PreferenceManager;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.firstmotion.myface.R;
import com.firstmotion.myface.utils.systemnotifications.SystemNotification;
import com.firstmotion.myface.view.ui.home.HomeFragment;

public class MainActivity extends AppCompatActivity {
    public static Context MainAppContext;
    public static SharedPreferences globalPreferences;
    private NavController navController;
    private static final SystemNotification systemNotifications = SystemNotification.build();
    public static Context getMainAppContext() {
        return MainAppContext;
    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // FACEBOOK
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        super.onCreate(savedInstanceState);
        globalPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = globalPreferences.edit();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        MainAppContext = getApplicationContext();
        setSupportActionBar(toolbar);
        NavController navController = Navigation.findNavController(this, R.id.navHostFragment);

        requestCameraAndStoragePermission(true);
        navController.addOnDestinationChangedListener((controller, destination, arguments) -> {
            int id = destination.getId();
            if (id == R.id.splash || id == R.id.welcome || id == R.id.sign_up
                    || id == R.id.settings || id == R.id.login || id == R.id.profile ||
                    id == R.id.aboutUs || id == R.id.change_password || id == R.id.homeFragment
                    || id == R.id.contactUsFragment
                    || id == R.id.upload_img_one || id == R.id.reset_password) {
                toolbar.setVisibility(View.GONE);
                getWindow().addFlags(WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_TOUCH);
            } else {
                toolbar.setVisibility(View.VISIBLE);
                getWindow().clearFlags(WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_TOUCH);
            }
        });


        if (globalPreferences.getBoolean("nightMode", false))
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        else
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);



    }

    private boolean checkIfAlreadyHavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else return result1 == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void requestCameraAndStoragePermission(boolean check) {
        if (!checkIfAlreadyHavePermission()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle("Permissions Required!")
                    .setMessage("MyFace App Need to Access Camera, Storage and SMS To Work")
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA
                            , Manifest.permission.WRITE_EXTERNAL_STORAGE
                            , Manifest.permission.READ_EXTERNAL_STORAGE
                            , Manifest.permission.ACCESS_NETWORK_STATE
                    }, 1))
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                        dialog.dismiss();
                        systemNotifications
                                .pushNotification(
                                        "unfortunately MyFace App Needs Permission",
                                        getWindow().getDecorView(),
                                        "dismiss",
                                        this,
                                        R.color.red
                                );
                        requestCameraAndStoragePermission(true);
                    });


            if (check) {
                builder.setNeutralButton("exit",
                        (dialog, id) -> {
                            int pid = android.os.Process.myPid();
                            android.os.Process.killProcess(pid);
                        });
            }
            builder.create().show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onRequestPermissionsResult(int requestCode, @androidx.annotation.NonNull String[] permissions, @androidx.annotation.NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (checkIfAlreadyHavePermission()) {
                    systemNotifications
                            .pushNotification(
                                    "Welcome To MyFace App",
                                    getWindow().getDecorView(),
                                    "ok",
                                    this,
                                    R.color.green
                            );
                    if (HomeFragment.homeF == 1) {
                        ((AppCompatActivity) getApplicationContext()).finish();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    }
                } else {
                    requestCameraAndStoragePermission(true);
                }
            } else {
                requestCameraAndStoragePermission(true);
            }
        }
    }


}