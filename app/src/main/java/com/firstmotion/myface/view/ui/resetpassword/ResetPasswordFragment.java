package com.firstmotion.myface.view.ui.resetpassword;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.ResetPasswordFragmentBinding;
import com.firstmotion.myface.networking.repositories.UsersRepository;
import com.firstmotion.myface.utils.AppUtils;
import com.firstmotion.myface.utils.NavigationHandler;
import com.firstmotion.myface.utils.systemnotifications.SystemNotification;
import com.firstmotion.myface.view.ui.MainActivity;

import static androidx.core.content.ContextCompat.getSystemService;

public class ResetPasswordFragment extends Fragment {
    private ResetPasswordFragmentBinding binding;
    private ResetPasswordViewModel mViewModel;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static final SystemNotification systemNotification = SystemNotification.build();
    private NavController navController;

    public static ResetPasswordFragment newInstance() {
        return new ResetPasswordFragment();
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.reset_password_fragment, container, false);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();

        return binding.getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        binding.newPasswordTIL.setOnFocusChangeListener((v, hasFocus) -> binding.newPasswordTIL.setHelperText(preferences.getString("pass-restrict-pattern", "Password Should Contain One Upper Letter(A-Z)\n One Small Letter (a-z) and a Special Character (!@#$%^&*)")));
        binding.resetPasswordTV.setText(preferences.getString("reset-password", "RESET PASSWORD"));
        binding.emailTIL.setHint(preferences.getString("e-mail", "Email"));
        binding.continueTV2.setText(preferences.getString("continue", "CONTINUE"));
        binding.newPasswordTIL.setHint(preferences.getString("new-password", "New Password"));
        binding.rePasswordTIL.setHint(preferences.getString("re-enter-password", "Re-enter Password"));
        binding.continueTV1.setText(preferences.getString("continue", "CONTINUE"));
        Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        binding.continueTV1.setOnClickListener(v -> {
            if (binding.emailTIET.getText().toString().isEmpty())
                binding.emailTIET.startAnimation(shake);
            if (!AppUtils.isNetworkAvailable(requireContext())) {
                systemNotification.pushNotification(
                        preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
            } else
                UsersRepository.getInstance()
                        .resetPasswordRequest("/users/req-reset-password", binding.emailTIET.getText().toString()).observe(
                        getViewLifecycleOwner(), resetPassApiResponse -> {
                            if (resetPassApiResponse != null && resetPassApiResponse.isStatus()) {
                                Toast.makeText(requireContext(), resetPassApiResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                addNotification(resetPassApiResponse.getMessage());
//                                systemNotification.pushNotification(
//                                        resetPassApiResponse.getMessage(),
//                                        getView(),
//                                        "ok",
//                                        getActivity(),
//                                        R.color.red
//                                );
                                navController.navigate(ResetPasswordFragmentDirections.actionResetPasswordToLogin());
                            } else if (resetPassApiResponse != null && !resetPassApiResponse.isStatus()) {
//                                systemNotification.pushNotification(
//                                        resetPassApiResponse.getErrors().getErrorMessage(),
//                                        getView(),
//                                        "dismiss",
//                                        getActivity(),
//                                        R.color.red
//                                );
                                Toast.makeText(requireContext(), resetPassApiResponse.getErrors().getErrorMessage(), Toast.LENGTH_SHORT).show();
                                binding.emailTIET.startAnimation(shake);
                            }
                        }
                );
        });

        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا") || preferences.getString("langCode", "").contains("و")) {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.backSettingsBTN.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.inverted_back_icon));
        } else {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.backSettingsBTN.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.back_icon));
        }
        binding.backSettingsBTN.setOnClickListener((v) -> NavigationHandler.navigate(getView(), ResetPasswordFragmentDirections.actionResetPasswordToLogin().getActionId(), null));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ResetPasswordViewModel.class);
        // TODO: Use the ViewModel
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void addNotification(String message) {
        NotificationManager manager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "my_channel_id";
        CharSequence channelName = "My Channel";
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.RED);
        notificationChannel.enableVibration(true);
        notificationChannel.setVibrationPattern(new long[]{500, 1000});
        manager.createNotificationChannel(notificationChannel);


        Intent notificationIntent = new Intent();
        PendingIntent contentIntent = PendingIntent.getActivity(getActivity(), 0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(requireContext())
                        .setSmallIcon(R.drawable.chech_notifiaction_icon)
                        .setContentTitle("Email Notification")
                        .setContentText(message)
                        .setChannelId(channelId)
                        .setAutoCancel(true)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(message))
                        .setContentIntent(contentIntent);

        // Add as notification

        manager.notify(0, builder.build());
    }

}