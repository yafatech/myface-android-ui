package com.firstmotion.myface.view.ui.contactUs;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.ContactUsFragmentBinding;
import com.firstmotion.myface.utils.NavigationHandler;
import com.firstmotion.myface.utils.mail.MailService;
import com.firstmotion.myface.utils.systemnotifications.SystemNotification;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactUsFragment extends Fragment {
    private ContactUsFragmentBinding binding;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private NavController navController;
    private static final MailService mailService = MailService.builder();
    private static final SystemNotification systemNotifications = SystemNotification.build();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.contact_us_fragment, container, false);
        return binding.getRoot();
    }

    @SuppressLint({"CommitPrefEdits", "UseCompatLoadingForDrawables"})
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
        Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        binding.contactUsTv.setText(preferences.getString("contact-us", "CONTACT US"));
        binding.mailTIL.setHint(preferences.getString("e-mail", "Email"));
        binding.typeMsgTV.setText(preferences.getString("type-message", "Type a message"));
        binding.sendTV.setText(preferences.getString("send", "SEND"));

        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا") || preferences.getString("langCode", "").contains("و")) {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.backSettingsBTN.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.inverted_back_icon));
        } else {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.backSettingsBTN.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.back_icon));
        }

        binding.backSettingsBTN.setOnClickListener((v) -> NavigationHandler.navigate(getView(), ContactUsFragmentDirections.actionContactUsFragmentToSettings().getActionId(), null));

        binding.sendTV.setOnClickListener((v) -> {
            // check if we have a valid mail
            if (!validateEmail(binding.mailTIET)) {
                binding.mailTIET.setError("Please Enter A Valid Email Address!");
                binding.mailTIET.setAnimation(shake);
            }

            if (binding.messageTIET.getText().toString().isEmpty()) {
                binding.messageTIET.startAnimation(shake);
                binding.messageTIET.setError("Please Add Message Content");
            }

            // we have a valid email and content
            String to = binding.mailTIET.getText().toString();
            String content = binding.messageTIET.getText().toString();

            if (validateEmail(binding.mailTIET) && !content.isEmpty() && !to.isEmpty()) {
                Thread thread = new Thread(() -> {
                    // disable ui and show spinner
                    this.requireActivity().runOnUiThread(this::disableAndShowSpinner);
                    // one to Support .
                    try {
                          mailService.prepareConnection()
                            .sendMail("info@my-face.app", content);

                    // One To Customer.
                    boolean isSent = mailService.prepareConnection()
                            .sendMail(to, content);

                    if (isSent) {
                        this.requireActivity().runOnUiThread(() ->
                                systemNotifications.pushNotification(
                                        "Message Sent, one of our support team will contact you",
                                        getView(),
                                        "dismiss",
                                        getActivity(),
                                        R.color.green
                                ));
                        // enable the spinner
                        this.requireActivity().runOnUiThread(this::enableAndShowSpinner);
                    } else {
                        this.requireActivity().runOnUiThread(() -> {
                            systemNotifications.pushNotification(
                                    "Failed to Send Message, Please Contact Support!",
                                    getView(),
                                    "dismiss",
                                    getActivity(),
                                    R.color.red
                            );
                        });

                    }
                    } catch (Exception e){
                        this.requireActivity().runOnUiThread(() ->
                                systemNotifications.pushNotification(
                                        "Message Sent, one of our support team will contact you",
                                        getView(),
                                        "dismiss",
                                        getActivity(),
                                        R.color.green
                                ));
                    }

                });
                thread.start();

            }
        });

    }


    @SuppressLint("ResourceAsColor")
    private void disableAndShowSpinner() {
        binding.sendTV.setBackgroundColor(getActivity().getResources().getColor(R.color.gray));
        binding.sendTV.setEnabled(false);
        binding.progressBar.setVisibility(View.VISIBLE);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void enableAndShowSpinner() {
        binding.sendTV.setBackground(getActivity().getResources().getDrawable(R.drawable.btn_bkg));
        binding.sendTV.setEnabled(true);
        binding.progressBar.setVisibility(View.GONE);
    }

    private boolean validateEmail(TextInputEditText filed) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(filed.getText().toString());
        return matcher.matches();
    }
}
