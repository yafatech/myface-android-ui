
package com.firstmotion.myface.view.ui.profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.ProfileFragmentBinding;
import com.firstmotion.myface.networking.models.requests.UpdateImagePrivacy;
import com.firstmotion.myface.networking.models.requests.UpdateProfileRequest;
import com.firstmotion.myface.networking.repositories.ProfileRepository;
import com.firstmotion.myface.utils.AppUtils;
import com.firstmotion.myface.utils.ImageRotators;
import com.firstmotion.myface.utils.NavigationHandler;
import com.firstmotion.myface.utils.imagedetection.DetectFaces;
import com.firstmotion.myface.utils.systemnotifications.SystemNotification;
import com.google.android.material.switchmaterial.SwitchMaterial;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.FileProvider.getUriForFile;

public class ProfileFragment extends Fragment {
    private ProfileFragmentBinding binding;
    private static final int IMAGE = 100;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private File image;
    private static final DetectFaces detectFaces = DetectFaces.build();
    private static final SystemNotification systemNotifications = SystemNotification.build();

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.profile_fragment, container, false);
        return binding.getRoot();
    }


    @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n", "ShowToast"})
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        NavController navController = Navigation.findNavController(view);
        binding.phoneTIET.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        binding.ccp.registerPhoneNumberTextView(binding.phoneTIET);
        binding.progressBar.setVisibility(View.VISIBLE);
        //set texts in profile ui as translations
        binding.fullNameTIL.setHint(preferences.getString("full-name", "Full Name"));
        binding.emailTIL.setHint(preferences.getString("e-mail", "Email"));
        binding.phoneTIL.setHint(preferences.getString("phone-number", "Phone Number"));
        binding.countryTIL.setHint(preferences.getString("country-name", "Country"));
        binding.cityTIL.setHint(preferences.getString("city-name", "City"));
        binding.ageTIL.setHint(preferences.getString("age", "Age"));
        binding.saveTV.setText(preferences.getString("save", "SAVE"));


        binding.hideImageSwitch.setOnCheckedChangeListener(null);
//        binding.hideImageSwitch.setChecked(true);
        //handle switch button action for hide image
        binding.hideImageSwitch.setOnClickListener((sw) -> {
            SwitchMaterial s = (SwitchMaterial) sw;
            if (s.isPressed()) {
                // handle change privacy logic
                if (!AppUtils.isNetworkAvailable(this.requireContext())) {
                    systemNotifications
                            .pushNotification(
                                    preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                                    getView(),
                                    "dismiss",
                                    getActivity(),
                                    R.color.red
                            );
                } else {
                    if (s.isChecked()) {
                        editor.putBoolean("showImage", true).apply();
                        String hideImage = preferences.getString("hide-image", "Hide Image");
                        binding.hideImageSwitchTv.setText(hideImage);
                        changePrivacy(true);
                    } else {
                        editor.putBoolean("showImage", false).apply();
                        String showImage = preferences.getString("show-image", "Show Image");
                        binding.hideImageSwitchTv.setText(showImage);
                        changePrivacy(false);
                    }
                }

            }
        });


        binding.backSettingsBTN.setOnClickListener((v) -> {
            if (preferences.getBoolean("fullUpdatedProfile", false))
                navController.navigate(ProfileFragmentDirections.actionProfileToSettings());
            else if (!Objects.requireNonNull(binding.fullNameTIET.getText()).toString().isEmpty() &&
                    !Objects.requireNonNull(binding.countryTIET.getText()).toString().isEmpty()
                    && !preferences.getBoolean("fullUpdatedProfile", false))
                Toast.makeText(requireContext(), "please save your Information before exit!", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(requireContext(), "please fill all fields!", Toast.LENGTH_LONG).show();

        });

        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا")
                || preferences.getString("langCode", "").contains("او")) {
            binding.ccp.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ccp.setTextDirection(View.TEXT_DIRECTION_LTR);
            binding.phoneTIL.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.phoneTIL.setTextDirection(View.TEXT_DIRECTION_RTL);
            binding.backSettingsBTN.setImageDrawable(requireActivity().getDrawable(R.drawable.inverted_back_icon));
        } else
            view.setTextDirection(View.TEXT_DIRECTION_LTR);

        //load profile data
        if (!AppUtils.isNetworkAvailable(requireContext())) {
            systemNotifications
                    .pushNotification(
                            preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                            getView(),
                            "dismiss",
                            getActivity(),
                            R.color.red
                    );
        } else {

            ProfileRepository.getInstance()
                    .loadingUserProfile("/users/profile", preferences.getString("token", ""), preferences.getString("refreshToken", ""), preferences.getString("username", ""), "profile")
                    .observe(getViewLifecycleOwner(), loadUserProfileResponseApiResponse -> {
                        if (loadUserProfileResponseApiResponse != null && loadUserProfileResponseApiResponse.isStatus()) {
                            boolean imageIndicatorStatus = loadUserProfileResponseApiResponse.getResults().getShowImage();
                            if (imageIndicatorStatus) {
                                String hideImage = preferences.getString("hide-image", "Hide Image");
                                binding.hideImageSwitchTv.setText(hideImage);
                                binding.hideImageSwitch.setChecked(true);
                            } else {
                                String showImage = preferences.getString("show-image", "Show Image");
                                binding.hideImageSwitch.setChecked(false);
                                binding.hideImageSwitchTv.setText(showImage);
                            }
                            // activate save button
                            binding.saveTV.setBackground(getResources().getDrawable(R.drawable.btn_bkg));
                            binding.saveTV.setClickable(true);
                            // Give a message that we must update the profile.
                            String fullName = loadUserProfileResponseApiResponse.getResults().getUserInfo() == null ? "" : loadUserProfileResponseApiResponse.getResults().getUserInfo().getFullName();
                            String city = loadUserProfileResponseApiResponse.getResults().getUserInfo() == null ? "" : loadUserProfileResponseApiResponse.getResults().getUserInfo().getCity();
                            String country = loadUserProfileResponseApiResponse.getResults().getUserInfo() == null ? "" : loadUserProfileResponseApiResponse.getResults().getUserInfo().getCountry();
                            Integer age = loadUserProfileResponseApiResponse.getResults().getUserInfo() == null ? 0 : loadUserProfileResponseApiResponse.getResults().getUserInfo().getAge();

                            if (fullName == null || fullName.isEmpty()
                                    || country == null || country.isEmpty()
                            ) {
                                systemNotifications.pushNotification(
                                        "You Must Update Your Profile!",
                                        getView(),
                                        "ok",
                                        getActivity(),
                                        R.color.red
                                );
                                binding.fullNameTIET.setBackground(requireActivity().getDrawable(R.drawable.empty_tf));
                                binding.countryTIET.setBackground(requireActivity().getDrawable(R.drawable.empty_tf));
                            }
                            // save user ID.
                            Boolean isShowImage = loadUserProfileResponseApiResponse.getResults().getShowImage();
                            boolean showImage = false;
                            if (isShowImage != null) {
                                showImage = isShowImage;
                            }
                            editor.putBoolean("showImage", showImage).apply();
                            editor.putString("userId", loadUserProfileResponseApiResponse.getResults().getUserId()).apply();
                            if (loadUserProfileResponseApiResponse.getResults().getUserInfo() != null) {
                                String[] imgUrl = loadUserProfileResponseApiResponse.getResults().getUserInfo().getImgUrl().split("=");
                                if (!imgUrl[1].contains("null")) {
                                    // user already uploaded an img before.
                                    editor.putString("avatarId", imgUrl[1]).apply();
                                }
                                editor.putString("imgUrl", loadUserProfileResponseApiResponse.getResults().getUserInfo().getImgUrl()).apply();
                                String phone = loadUserProfileResponseApiResponse.getResults().getUserInfo().getPhoneNumber();
                                if (!phone.isEmpty() && phone.startsWith("0"))
                                    phone = phone.substring(2);
                                if (phone.isEmpty())
                                    binding.phoneTIET.setText("");
                                else binding.ccp.setFullNumber(phone);
                            }

                            editor.putString("username", loadUserProfileResponseApiResponse.getResults().getUsername()).apply();

                            if (loadUserProfileResponseApiResponse.getResults().getEmail() == null)
                                binding.emailTIET.setText("");
                            binding.emailTIET.setText(loadUserProfileResponseApiResponse.getResults().getEmail());
                            if (fullName == null)
                                binding.fullNameTIET.setText("");
                            binding.fullNameTIET.setText(fullName);

                            if (age == 0)
                                binding.ageTIET.setText("0");
                            binding.ageTIET.setText(String.valueOf(age));
                            if (country == null)
                                binding.countryTIET.setText("");
                            binding.countryTIET.setText(country);
                            if (city == null)
                                binding.cityTIET.setText("");
                            binding.cityTIET.setText(city);
                            // add restriction to Input value of the age (the value must be between two values 1,100).
//                            binding.ageTIET.setFilters(new InputFilter[]{new InputFilterMinMax("13", "90")});
                            String path = preferences.getString("imgName", "");
                            if (!path.isEmpty()) {
                                Bitmap bitmap = BitmapFactory.decodeFile(path);
                                binding.profileImgV.setImageBitmap(bitmap);
                            }
                        /*
                         case 1: if we already have an image with the profile response.
                         case 2: we don't have a profile image.
                        */
                            // load profile image once we have the img id represents in the get profile response
                            if (!preferences.getString("avatarId", "").isEmpty())
                                ProfileRepository.getInstance()
                                        .loadImage("/cdn/storage/media/load", preferences.getString("avatarId", ""))
                                        .observe(getViewLifecycleOwner(), loadImageResponse -> {
                                            if (loadImageResponse != null) {
                                                binding.profileImgV.setImageBitmap(loadImageResponse);
//                                                Log.e("IMAGE LOADED", "images is loaded");
                                                binding.progressBar.setVisibility(View.GONE);
                                            }
                                        });


                            if (loadUserProfileResponseApiResponse.getResults().getUserInfo() != null && loadUserProfileResponseApiResponse.getResults().getUserInfo().getImgUrl().contains("null"))
                                systemNotifications
                                        .pushNotification(
                                                loadUserProfileResponseApiResponse.getMessage(),
                                                getView(),
                                                "ok",
                                                getActivity(),
                                                R.color.green
                                        );
                        } else if (loadUserProfileResponseApiResponse != null) {
                            systemNotifications
                                    .pushNotification(
                                            "Can't load user profile: " + loadUserProfileResponseApiResponse.getErrors().getErrorMessage(),
                                            getView(),
                                            "dismiss",
                                            getActivity(),
                                            R.color.red
                                    );
                        }

                    });

        }


        binding.saveTV.setOnClickListener(v -> {
            if (!Objects.requireNonNull(binding.fullNameTIET.getText()).toString().isEmpty() && !Objects.requireNonNull(binding.countryTIET.getText()).toString().isEmpty()
            ) {
                if (preferences.getString("avatarId", "").isEmpty())
                    systemNotifications
                            .pushNotification(
                                    "you didn't have a profile image, please consider upload one!",
                                    getView(),
                                    "dismiss",
                                    getActivity(),
                                    R.color.red
                            );
                else {
                    if (!AppUtils.isNetworkAvailable(this.requireContext())) {
                        systemNotifications
                                .pushNotification(
                                        preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                                        getView(),
                                        "dismiss",
                                        getActivity(),
                                        R.color.red
                                );
                    } else {
                        if (binding.ageTIET.getText().toString().isEmpty() || Integer.parseInt(binding.ageTIET.getText().toString()) > 90
                                || Integer.parseInt(binding.ageTIET.getText().toString()) > 90)
                            binding.ageTIET.setError("Age must be between 13 and 90", null);
                        //save user info after editing.
                        ProfileRepository.getInstance()
                                .updateUserProfile("/users/info", preferences.getString("token", "")
                                        , preferences.getString("refreshToken", ""), new UpdateProfileRequest(
                                                preferences.getString("userId", ""),
                                                preferences.getString("avatarId", ""),
                                                binding.fullNameTIET.getText().toString(),
                                                !binding.ccp.getFullNumber().isEmpty() ? binding.ccp.getFullNumber() : "",
                                                binding.countryTIET.getText().toString(),
                                                binding.cityTIET.getText().toString(),
                                                Integer.parseInt(binding.ageTIET.getText().toString())
                                        )
                                ).observe(getViewLifecycleOwner(), editProfileResponse -> {
                            if (editProfileResponse != null && editProfileResponse.isStatus()) {
                                systemNotifications
                                        .pushNotification(
                                                editProfileResponse.getMessage(),
                                                getView(),
                                                "ok",
                                                getActivity(),
                                                R.color.green
                                        );
                                editor.putBoolean("fullUpdatedProfile", true).apply();
                                navController.navigate(ProfileFragmentDirections.actionProfileToSettings());


                            } else if (editProfileResponse != null && editProfileResponse.getErrors() != null) {
                                systemNotifications
                                        .pushNotification(
                                                editProfileResponse.getErrors().getErrorMessage(),
                                                getView(),
                                                "dismiss",
                                                getActivity(),
                                                R.color.red
                                        );
                            }

                        });

                    }
                }
            } else {
                editor.putBoolean("fullUpdatedProfile", false).apply();
                Toast.makeText(requireContext(), "please fill All fields!", Toast.LENGTH_LONG).show();
            }
        });
        binding.profileImgV.setOnClickListener(v -> dispatchTakePictureIntent(1));

    }

    private void changePrivacy(Boolean status) {
        ProfileRepository
                .getInstance()
                .updateProfileImageStatus("/users/change-privacy", preferences.getString("token", "")
                        , preferences.getString("refreshToken", ""),
                        new UpdateImagePrivacy(preferences.getString("userId", ""), status))
                .observe(getViewLifecycleOwner(), editImagePrivacyResponse -> {
                    if (editImagePrivacyResponse != null && editImagePrivacyResponse.isStatus()) {
                        systemNotifications.pushNotification(
                                editImagePrivacyResponse.getResults().getChanged(),
                                getView(),
                                "ok",
                                getActivity(),
                                R.color.green
                        );
                    }
                    // redirect back to settings.
//                    NavigationHandler.navigate(getView(), ProfileFragmentDirections.actionProfileToSettings().getActionId(), null);
                });


    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && image != null) {
            try {
                String filePath = image.getPath();
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                bitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, true);
                Bitmap routedBitMap = ImageRotators.rotateImage(bitmap, filePath);
                detectFaces.imageFromBitmap(routedBitMap)
                        .detectFaces().addOnSuccessListener(
                        faces -> {
                            if (!faces.isEmpty()) {
                                binding.profileImgV.setImageBitmap(routedBitMap);
                                uploadImage(routedBitMap);
                            } else
                                systemNotifications.pushNotification(
                                        "You Must Upload images for people only, make sure its a photo of you",
                                        getView(),
                                        "dismiss",
                                        getActivity(),
                                        R.color.red
                                );

                        }).addOnFailureListener(
                        e -> systemNotifications.pushNotification(
                                "Failed To Detect Faces",
                                getView(),
                                "dismiss",
                                getActivity(),
                                R.color.red
                        ));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void uploadImage(Bitmap bitmap) {
        binding.saveTV.setBackground(getResources().getDrawable(R.drawable.unactive_btn_bkg));
        binding.saveTV.setClickable(false);
        try {
            binding.profileImgV.setImageBitmap(bitmap);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            Bitmap.createScaledBitmap(bitmap, 500, 500, true);
            File file = new File(requireContext().getFilesDir(), "myFace-" + UUID.randomUUID());

            byte[] bitmapData = bos.toByteArray();
            //write the bytes in file
            FileOutputStream fos;
            fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();


            ProfileRepository.getInstance()
                    .uploadImage("cdn/storage/media/upload", preferences.getString("userId", ""), file)
                    .observe(getViewLifecycleOwner(), uploadImageResponse -> {
                        if (uploadImageResponse != null && uploadImageResponse.isStatus()) {
                            binding.progressBar.setVisibility(View.GONE);
                            binding.saveTV.setBackground(getResources().getDrawable(R.drawable.btn_bkg));
                            binding.saveTV.setClickable(true);
                            editor.putString("avatarId", uploadImageResponse.getResults().getFileId().trim()).apply();
                            systemNotifications
                                    .pushNotification(
                                            uploadImageResponse.getMessage(),
                                            getView(),
                                            "ok",
                                            getActivity(),
                                            R.color.green);
                        } else if (uploadImageResponse != null && !uploadImageResponse.isStatus()) {
                            systemNotifications
                                    .pushNotification(uploadImageResponse.getErrors().getErrorMessage(),
                                            getView(),
                                            "dismiss",
                                            getActivity(),
                                            R.color.red);
                        }

                    });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("QueryPermissionsNeeded")
    private void dispatchTakePictureIntent(int request) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = getUriForFile(getContext(),
                        "com.firstmotion.myface.utils.FileProvider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, request);
            }
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String imageFileName = "myFace_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
//        String currentPhotoPath = image.getAbsolutePath();
        return image;
    }
}