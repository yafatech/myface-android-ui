package com.firstmotion.myface.view.ui.tutorial;


import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.viewpager2.widget.ViewPager2;

import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.TutorialFragmentPage1Binding;
import com.firstmotion.myface.view.adapters.TextAdapter;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.Arrays;


public class TutorialPage1Fragment extends Fragment {
    private SharedPreferences preferences;
    private TutorialFragmentPage1Binding binding;
    private TextAdapter adapter;
    private NavController navController;
    private static boolean invertBack;

    public TutorialPage1Fragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا") || preferences.getString("langCode", "").contains("و")) {
            invertBack = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.tutorial_fragment_page1, container, false);
        binding.nextBTN.setText(preferences.getString("next", "NEXT"));
        adapter = new TextAdapter();
        adapter.setData(Arrays.asList(
                preferences.getString("tutorial_txt1", "Start By Upload Some Content For Your Face"),
                preferences.getString("tutorial_txt2", "SignUp With Google if You're Lazy or Just Create a New account"),
                preferences.getString("tutorial_txt3", "The Camera Will Open automatically To Detect Your Face"),
                preferences.getString("tutorial_txt4", "Find Your Matches And Have Fun!! :) ")
        ));
        return binding.getRoot();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        binding.nextBTN.setText(preferences.getString("next", "NEXT"));
        binding.tutorial.setText(preferences.getString("tutorial", "Tutorial"));
//        binding.nextBTN.setOnClickListener(view1 ->
//        .navigate(TutorialPage1FragmentDirections.actionTutorialPage1FragmentToTutorialPage2Fragment()));
        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا") || preferences.getString("langCode", "").contains("و")) {
            binding.previousTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.inverted_back_icon));
            binding.nextTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.green_back));
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        binding.tutorialBodyVb.setAdapter(adapter);
        binding.tutorialBodyVb.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        new TabLayoutMediator(binding.tabDots, binding.tutorialBodyVb,
                (tab, position) -> binding.tutorialBodyVb.setCurrentItem(tab.getPosition(), true)
        ).attach();

        binding.tutorialBodyVb.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });


        binding.nextTutorial.setOnClickListener(view -> {
            binding.tutorialBodyVb.setCurrentItem(binding.tutorialBodyVb.getCurrentItem() + 1);
            if (binding.tutorialBodyVb.getCurrentItem() == 3) {
                if (invertBack)
                    binding.nextTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.back_icon));
                else
                    binding.nextTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.inverted_back_icon));
            } else {
                if (invertBack)
                    binding.nextTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.green_back));
                else
                    binding.nextTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.inverted_green_back));
            }
            if (binding.tutorialBodyVb.getCurrentItem() > 0) {
                if (invertBack)
                    binding.previousTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.inverted_green_back));
                else
                    binding.previousTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.green_back));
            }
        });
        binding.previousTutorial.setOnClickListener(view -> {
            binding.tutorialBodyVb.setCurrentItem(binding.tutorialBodyVb.getCurrentItem() - 1);
            if (binding.tutorialBodyVb.getCurrentItem() == 0) {
                if (invertBack)
                    binding.previousTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.inverted_back_icon));
                else
                    binding.previousTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.back_icon));
            } else {
                if (invertBack)
                    binding.previousTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.inverted_green_back));
                else
                    binding.previousTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.green_back));
            }
            if (binding.tutorialBodyVb.getCurrentItem() < 3) {
                if (invertBack)
                    binding.nextTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.green_back));
                else
                    binding.nextTutorial.setImageDrawable(requireActivity().getDrawable(R.drawable.inverted_green_back));
            }
        });
    }


}

