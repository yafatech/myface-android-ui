package com.firstmotion.myface.view.ui.settings;
/**
 * created by Rahaf Alhameed on 12/10/2020.
 */

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.facebook.login.LoginManager;
import com.firstmotion.myface.BuildConfig;
import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.SettingFragmentBinding;
import com.firstmotion.myface.networking.models.AppLangGroup;
import com.firstmotion.myface.networking.models.AppSettings;
import com.firstmotion.myface.networking.repositories.AccountRepository;
import com.firstmotion.myface.networking.repositories.ProfileRepository;
import com.firstmotion.myface.networking.repositories.SplashScreenRepository;
import com.firstmotion.myface.networking.resources.ApiUrls;
import com.firstmotion.myface.utils.AppUtils;
import com.firstmotion.myface.utils.RegionsResolver;
import com.firstmotion.myface.utils.systemnotifications.SystemNotification;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;


public class SettingsFragment extends Fragment {
    private SettingFragmentBinding binding;
    @SuppressLint("StaticFieldLeak")
    public static NavController navController;
    private static String selectedLang;
    private List<AppLangGroup> appLangGroupList;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private AlertDialog.Builder builder;
    private static String langCode = "en-us";
    private static HashSet<String> languages;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private static String userId;
    private static final SystemNotification systemNotifications = SystemNotification.build();
    @SuppressLint("StaticFieldLeak")
    public static View returnView;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        languages = new HashSet<>();

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        //The SMS message sent by Firebase can also be localized by specifying the auth language via the setLanguageCode method on your Auth instance. auth.setLanguageCode("fr");
        // To apply the default app language instead of explicitly setting it.
        mAuth.useAppLanguage();

        if (mAuth.getCurrentUser() != null)
            mAuth.signOut();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.setting_fragment, container, false);
        return binding.getRoot();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint({"CommitPrefEdits", "UseCompatLoadingForDrawables"})
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();

        returnView = getView();
        if (!AppUtils.isNetworkAvailable(requireContext())) {
            systemNotifications.pushNotification(
                    preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                    getView(),
                    "dismiss",
                    getActivity(),
                    R.color.red
            );
        } else

            ProfileRepository.getInstance()
                    .loadingUserProfile("/users/profile", preferences.getString("token", ""), preferences.getString("refreshToken", ""), preferences.getString("username", ""), "settings")
                    .observe(getViewLifecycleOwner(), loadUserProfileResponseApiResponse -> {
                        if (loadUserProfileResponseApiResponse != null && loadUserProfileResponseApiResponse.isStatus()) {
                            editor.putString("userId", loadUserProfileResponseApiResponse.getResults().getUserId()).apply();
                        }
                    });

        binding.nightModeSwitch.setChecked(preferences.getBoolean("nightMode", false));

        if (preferences.getString("selectedLang", null) != null)
            binding.selectedLangTV.setText(preferences.getString("selectedLang", null));
//        binding.aboutUsTV.setText(preferences.getString("about", "About us"));
        binding.settingsTV.setText(preferences.getString("settings", "Settings"));
        binding.nightModeTV.setText(preferences.getString("dark-mode", "Dark Mode"));
        binding.signOutTV.setText(preferences.getString("sign-out", "Sign Out"));
        binding.notificationTV.setText(preferences.getString("notification", "Notification"));
        binding.shareTV.setText(preferences.getString("share", "Share App"));
        binding.changePassTV.setText(preferences.getString("change-password", "Change Password"));
        binding.editProfileTV.setText(preferences.getString("edit-profile", "Edit Profile"));
        binding.contactUsTv.setText(preferences.getString("contact-us", "Contact Us"));
        binding.languageTV.setText(preferences.getString("language", "Language"));
        binding.deleteAccountTV.setText(preferences.getString("delete-account", "Delete Account"));
        binding.deactivateAccountTV.setText(preferences.getString("deactivate-account", "Deactivate Account"));
        binding.selectedLangTV.setText(preferences.getString("selected_lang", "English"));
        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا") || preferences.getString("langCode", "").contains("و")) {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.backBTN.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.inverted_back_icon));
        } else {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.backBTN.setImageDrawable(requireActivity().getResources().getDrawable(R.drawable.back_icon));
        }

        SplashScreenRepository.getInstance()
                .getSettings("/app-settings/by-region", RegionsResolver.getRegion(Locale.getDefault().getDisplayCountry().toLowerCase()))
                .observe(getViewLifecycleOwner(), response -> {
                    if (response != null && response.isStatus()) {
                        // we have success Data.

                        for (AppSettings appSetting : response.getResults()) {
                            appLangGroupList = appSetting.getAppLangGroup();
                            saveData(appLangGroupList);
                            editor.putString("logoImgUrl", appSetting.getImgUrl()).apply();
                        }
                        loadData().forEach(appLangGroup -> languages.add(appLangGroup.getLanguageName()));
                        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_singlechoice);
                        languages.forEach(arrayAdapter::add);
                        // create dialog display list of available languages.
                        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                        binding.languageLL.setOnClickListener(v -> {
                            dialog.setIcon(R.drawable.lang_icon)
                                    .setSingleChoiceItems(arrayAdapter, arrayAdapter.getPosition(selectedLang), null)
                                    .setAdapter(arrayAdapter, (dialog1, which) ->
                                            selectedLang = arrayAdapter.getItem(which))
                                    .setNegativeButton(preferences.getString("cancel", "Cancel"), (dialog2, i) -> dialog2.dismiss())
                                    .setPositiveButton(preferences.getString("ok", "Ok"), (dialog1, i) -> {
                                        dialog1.dismiss();
                                        binding.selectedLangTV.setText(selectedLang);
                                        loadData().forEach(appLangGroup -> {
                                            if (selectedLang != null && selectedLang.equals(appLangGroup.getLanguageName())) {
                                                langCode = appLangGroup.getLangCode();
                                                appLangGroup.getTranslations().forEach(translations -> editor.putString(translations.getKey(), translations.getValue()).apply());
                                                editor.putString("langCode", langCode).apply();
                                                editor.putString("selected_lang", selectedLang).apply();
                                            }

                                        });
                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.detach(SettingsFragment.this).attach(SettingsFragment.this).commit();

                                    }).show()
                                    .getWindow().setBackgroundDrawable(requireContext().getDrawable(R.drawable.shadow_dialog));

                        });
                    }
                });
        //navigation to the about us ui.
        //temporary comments for about us user interface.
//        binding.aboutUsTV.setOnClickListener(v -> {
//                    if (!AppUtils.isNetworkAvailable(requireContext())) {
//                        systemNotifications.pushNotification(
//                                preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
//                                getView(),
//                                "dismiss",
//                                getActivity(),
//                                R.color.red
//                        );
//                    } else
//                        navController.navigate(SettingsFragmentDirections.actionSettingsToAboutUsFragment());
//                }
//        );


        binding.editProfileTV.setOnClickListener(v -> {
//            if (!preferences.getString("token", "").isEmpty()) {
//                Log.e("TOKEN", preferences.getString("token", ""));
//            }
            if (!AppUtils.isNetworkAvailable(requireContext())) {
                systemNotifications.pushNotification(
                        preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
            } else
                navController.navigate(SettingsFragmentDirections.actionSettingsToProfile());
        });

        binding.changePassTV.setOnClickListener(v -> {
            if (!AppUtils.isNetworkAvailable(requireContext())) {

                systemNotifications.pushNotification(
                        preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
            } else {
                // check if the account is manged by google then we will notify the user only .
                boolean isSocial = preferences.getBoolean("isSocial", false);
                if (isSocial) {
                    systemNotifications.pushNotification(
                            "Your Account is Managed By Social Platform :)",
                            getView(),
                            "dismiss",
                            getActivity(),
                            R.color.green
                    );
                } else
                    navController.navigate(SettingsFragmentDirections.actionSettingsToChangePassword());
            }

        });

        binding.signOutTV.setOnClickListener(v -> new AlertDialog.Builder(getContext())
                .setMessage(preferences.getString("sign-out-message", "Are you sure that you want to logout?"))
                .setNegativeButton(preferences.getString("cancel", "Cancel"), (dialog2, i) -> dialog2.dismiss())
                .setPositiveButton(preferences.getString("confirm", "Confirm"), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    boolean isSocial = preferences.getBoolean("isSocial", false);
                    if (isSocial) {
                        // is social login logout from google account.
                        mAuth.signOut();
                        mGoogleSignInClient.signOut();
                        mGoogleSignInClient.revokeAccess();
                        // logout facebook
                        LoginManager.getInstance().logOut();
                    }
                    editor.putString("token", "").apply();
                    editor.putString("username", "").apply();
                    editor.putString("refreshToken", "").apply();
                    navController.navigate(SettingsFragmentDirections.actionSettingsToLogin());
                })
                .show()
                .getWindow().setBackgroundDrawable(requireContext().getDrawable(R.drawable.shadow_dialog))
        );

        binding.contactUsTv.setOnClickListener(v -> {
            if (!AppUtils.isNetworkAvailable(getContext())) {
                systemNotifications.pushNotification(
                        preferences.getString("no-Internet", "Phone is not Connected to the Internet"),
                        getView(),
                        "dismiss",
                        getActivity(),
                        R.color.red
                );
            } else
                navController.navigate(SettingsFragmentDirections.actionSettingsToContactUsFragment());
        });

        binding.nightModeSwitch.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                editor.putBoolean("nightMode", true).apply();
                binding.notificationSwitchLL.setChecked(preferences.getBoolean("notification_status", false));
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                editor.putBoolean("nightMode", false).apply();
                binding.notificationSwitchLL.setChecked(preferences.getBoolean("notification_status", false));
            }


        });

        binding.notificationSwitchLL.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked)
                editor.putBoolean("notification_status", true).apply();
            else
                editor.putBoolean("notification_status", false).apply();

        });

        //navigate to home.
        binding.backBTN.setOnClickListener(v -> {
            navController.navigate(SettingsFragmentDirections.actionSettingsToHomeFragment());
        });
        binding.shareTV.setOnClickListener(v -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out MyFace App at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        });

        binding.deactivateAccountTV.setOnClickListener(v -> {

            new AlertDialog.Builder(getContext())
                    .setMessage(preferences.getString("deactivate-account-message", "Are you sure that you want to deactivate your Account?"))
                    .setNegativeButton(preferences.getString("cancel", "Cancel"), (dialog2, i) -> dialog2.dismiss())
                    .setPositiveButton(preferences.getString("confirm", "Confirm"), (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        // clear google and facebook
                        boolean isSocial = preferences.getBoolean("isSocial", false);
                        if (isSocial) {
                            // is social login logout from google account.
                            mAuth.signOut();
                            mGoogleSignInClient.signOut();
                            mGoogleSignInClient.revokeAccess();
                            // logout facebook
                            LoginManager.getInstance().logOut();
                        }
                        AccountRepository.getInstance()
                                .deactivateAccount(ApiUrls.API_URL + "/users/deactivate", preferences.getString("token", ""),
                                        preferences.getString("refreshToken", ""), preferences.getString("userId", ""))
                                .observe(getViewLifecycleOwner(), response ->
                                        {
                                            if (response != null)
                                                systemNotifications.pushNotification(
                                                        response.getMessage(),
                                                        getView(),
                                                        "ok",
                                                        getActivity(),
                                                        R.color.green
                                                );
                                        }
                                );
                        editor.clear();
                        editor.commit();
                        navController.navigate(SettingsFragmentDirections.actionSettingsToLogin());
                    })
                    .show()
                    .getWindow().setBackgroundDrawable(requireContext().getDrawable(R.drawable.shadow_dialog));
        });

        binding.deleteAccountTV.setOnClickListener(v -> new AlertDialog.Builder(getContext())
                .setMessage(preferences.getString("delete-account-message", "Are you sure that you want to delete your Account?"))
                .setNegativeButton(preferences.getString("cancel", "Cancel"), (dialog2, i) -> dialog2.dismiss())
                .setPositiveButton(preferences.getString("confirm", "Confirm"), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    // clear google and facebook
                    boolean isSocial = preferences.getBoolean("isSocial", false);
                    if (isSocial) {
                        // is social login logout from google account.
                        mAuth.signOut();
                        mGoogleSignInClient.signOut();
                        mGoogleSignInClient.revokeAccess();
                        // logout facebook
                        LoginManager.getInstance().logOut();
                    }
                    AccountRepository.getInstance()
                            .deleteAccount(ApiUrls.API_URL + "/users/delete", preferences.getString("token", ""),
                                    preferences.getString("refreshToken", ""), preferences.getString("userId", ""))
                            .observe(getViewLifecycleOwner(), response ->
                                    {
                                        if (response != null)
                                            systemNotifications.pushNotification(
                                                    response.getMessage(),
                                                    getView(),
                                                    "ok",
                                                    getActivity(),
                                                    R.color.green
                                            );
                                    }
                            );
                    editor.clear();
                    editor.commit();
                    navController.navigate(SettingsFragmentDirections.actionSettingsToLogin());
                })
                .show()
                .getWindow().setBackgroundDrawable(requireContext().getDrawable(R.drawable.shadow_dialog)));

        binding.facebookBTN.setOnClickListener(v -> {
            Intent i;
            try {
                requireContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            // set page ID  of face detection page.
            i = new Intent(Intent.ACTION_VIEW, Uri.parse(preferences.getString("facebook-page-link", "https://www.facebook.com/MyFaceApplication")));
            checkIfAppExists(i, "Facebook");
        });

        binding.instagramBTN.setOnClickListener(v -> {
            Intent intentInstagram = new Intent(Intent.ACTION_VIEW);
            String url = preferences.getString("instagram-link", "https://www.instagram.com/myface_app/");
            intentInstagram.setData(Uri.parse(url));
            intentInstagram.setPackage("com.instagram.android");
            checkIfAppExists(intentInstagram, "Instagram");
        });

        binding.twitterBTN.setOnClickListener(v -> {
            Intent intent;
            try {
                requireActivity().getPackageManager().getPackageInfo("com.twitter.android", 0);
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + "myface_app"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            } catch (Exception e) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + "myface_app"));
            }
            requireActivity().startActivity(intent);
        });
    }


    @Override
    public void onPause() {
        super.onPause();

    }


    // method to check whether an app exists or not
    @SuppressLint("QueryPermissionsNeeded")
    public boolean checkIfAppExists(Intent appIntent, String appName) {
        if (appIntent.resolveActivity(requireContext().getPackageManager()) != null) {
            // start the activity if the app exists in the system
            startActivity(appIntent);
            return true;
        } else {
//            // tell the user the app does not exist
//            systemNotifications.pushNotification(
//                    appName + "app does not exist!",
//                    getView(),
//                    "dismiss",
//                    getActivity(),
//                    R.color.red
//            );
            return false;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    //convert json data to app language group list.
    private List<AppLangGroup> loadData() {
        Gson gson = new Gson();
        String json = preferences.getString("AppLangGroupList", "");
        Type type = new TypeToken<List<AppLangGroup>>() {
        }.getType();
        appLangGroupList = gson.fromJson(json, type);
        return appLangGroupList;
    }

    private void saveData(List<AppLangGroup> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString("AppLangGroupList", json).apply();
    }

}