package com.firstmotion.myface.view.ui.welcome;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.viewpager2.widget.ViewPager2;

import com.bumptech.glide.Glide;
import com.firstmotion.myface.R;
import com.firstmotion.myface.databinding.WelcomeFragmentBinding;
import com.firstmotion.myface.networking.models.AppLangGroup;
import com.firstmotion.myface.view.adapters.TextAdapter;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

public class WelcomeFragment extends Fragment {
    private final static Logger LOGGER = Logger.getLogger(WelcomeFragment.class.getName());

    private WelcomeFragmentBinding binding;
    private TextAdapter adapter;
    private NavController navController;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private List<AppLangGroup> appLangGroupList;
    private static String selectedLang;
    private static HashSet<String> languages;

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();
        languages = new HashSet<>();

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.welcome_fragment, container, false);
        return binding.getRoot();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        navController = Navigation.findNavController(view);
        Glide.with(binding.logo)
                .load(R.drawable.welcome_logo)
                .into(binding.logo);
        binding.langTV.setText(preferences.getString("selected_lang", "English"));
        if (preferences.getString("langCode", "").contains("ar") || preferences.getString("langCode", "").contains("فا") || preferences.getString("langCode", "").contains("و")) {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        } else {
            requireActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
        adapter = new TextAdapter();

        // Setting Welcome Screen Welcome Text
        adapter.setData(Arrays.asList(
                preferences.getString("welcome1", "You Can Use MyFace App To Find People That Match You in This World"),
                preferences.getString("welcome2", "With in Simple Steps MyFace App Will Give you A list of People to Match You"),
                preferences.getString("welcome3", "Place Your Face In Front Of the Camera And Wait"),
                preferences.getString("welcome4", "When the App Find People That Matches Your Face You Will Get Results")));

        binding.continueTV.setText(preferences.getString("continue", "CONTINUE"));
        binding.nextTV.setText(preferences.getString("next", "NEXT"));

        if (loadData() != null) {
            appLangGroupList.forEach(appLangGroup -> languages.add(appLangGroup.getLanguageName()));
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_singlechoice);
            languages.forEach(arrayAdapter::add);
            // create dialog display list of available languages.
            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
            binding.langTV.setOnClickListener(v ->
                    dialog.setIcon(R.drawable.lang_icon)
                            .setSingleChoiceItems(arrayAdapter, arrayAdapter.getPosition(selectedLang), null)
                            .setAdapter(arrayAdapter, (dialog1, which) ->
                                    selectedLang = arrayAdapter.getItem(which))
                            .setNegativeButton(preferences.getString("cancel", "Cancel"), (dialog2, i) -> dialog2.dismiss())
                            .setPositiveButton(preferences.getString("ok", "Ok"), (dialog1, i) -> {
                                dialog1.dismiss();
                                binding.langTV.setText(selectedLang);
                                loadData().forEach(appLangGroup -> {
                                    if (selectedLang != null && selectedLang.equals(appLangGroup.getLanguageName())) {
                                        String langCode = appLangGroup.getLangCode();
                                        appLangGroup.getTranslations().forEach(translations -> editor.putString(translations.getKey(), translations.getValue()).apply());
                                        editor.putString("langCode", langCode).apply();
                                        editor.putString("selected_lang", selectedLang).apply();
                                    }

                                });
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.detach(WelcomeFragment.this).attach(WelcomeFragment.this).commit();

                            }).show()
                            .getWindow().setBackgroundDrawable(requireContext().getDrawable(R.drawable.shadow_dialog)));
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.bodyVP.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        binding.bodyVP.setAdapter(adapter);

        new TabLayoutMediator(binding.tabLayout, binding.bodyVP,
                (tab, position) -> binding.bodyVP.setCurrentItem(tab.getPosition(), true)
        ).attach();
        binding.bodyVP.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position == adapter.getItemCount() - 1) {
                    binding.continueTV.setVisibility(View.VISIBLE);
                    binding.nextTV.setVisibility(View.INVISIBLE);
                } else if (position <= adapter.getItemCount() - 1) {
                    binding.nextTV.setVisibility(View.VISIBLE);
                    binding.continueTV.setVisibility(View.GONE);

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }
        });

        binding.nextTV.setOnClickListener(v -> binding.bodyVP.setCurrentItem(binding.bodyVP.getCurrentItem() + 1));
        binding.continueTV.setOnClickListener(v -> navController.navigate(WelcomeFragmentDirections.actionWelcomeToSignUp()));

    }

    //convert json data to app language group list.
    private List<AppLangGroup> loadData() {
        Gson gson = new Gson();
        String json = preferences.getString("AppLangGroupList", "");
        Type type = new TypeToken<List<AppLangGroup>>() {
        }.getType();
        appLangGroupList = gson.fromJson(json, type);
        return appLangGroupList;
    }

}