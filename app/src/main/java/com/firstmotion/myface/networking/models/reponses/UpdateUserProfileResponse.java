package com.firstmotion.myface.networking.models.reponses;

public class UpdateUserProfileResponse {
    private String imgUrl;
    private String fullName;
    private String phoneNumber;
    private String country;
    private String city;
    private int age;
    private String created;
    private String modified;

    public UpdateUserProfileResponse() {
    }

    public UpdateUserProfileResponse(String imgUrl, String fullName, String phoneNumber, String country, String city, int age, String created, String modified) {
        this.imgUrl = imgUrl;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.city = city;
        this.age = age;
        this.created = created;
        this.modified = modified;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }
}
