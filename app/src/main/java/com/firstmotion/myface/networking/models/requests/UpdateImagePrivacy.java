package com.firstmotion.myface.networking.models.requests;

import java.io.Serializable;

public class UpdateImagePrivacy implements Serializable {
    private String userId;
    private boolean showImage;

    public UpdateImagePrivacy() {
    }

    public UpdateImagePrivacy(String userId, boolean showImage) {
        this.userId = userId;
        this.showImage = showImage;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isShowImage() {
        return showImage;
    }

    public void setShowImage(boolean showImage) {
        this.showImage = showImage;
    }
}
