package com.firstmotion.myface.networking.repositories;

import androidx.lifecycle.MutableLiveData;

import com.firstmotion.myface.R;
import com.firstmotion.myface.networking.api.NetworkClientBuilder;
import com.firstmotion.myface.networking.models.ApiResponse;
import com.firstmotion.myface.networking.models.reponses.LoginResponse;
import com.firstmotion.myface.networking.models.reponses.PhoneExistResponse;
import com.firstmotion.myface.networking.models.reponses.RegistrationResponse;
import com.firstmotion.myface.networking.models.requests.ChangePassRequest;
import com.firstmotion.myface.networking.models.requests.LoginRequest;
import com.firstmotion.myface.networking.models.requests.LoginSocialRequest;
import com.firstmotion.myface.networking.models.requests.RegistrationRequest;
import com.firstmotion.myface.networking.models.requests.RegistrationSocialRequest;
import com.firstmotion.myface.networking.resources.ApiUrls;
import com.firstmotion.myface.utils.NavigationHandler;
import com.firstmotion.myface.view.ui.home.HomeFragment;
import com.firstmotion.myface.view.ui.settings.SettingsFragmentDirections;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.firstmotion.myface.view.ui.settings.SettingsFragment.returnView;

public final class UsersRepository {
    private static UsersRepository usersRepository;
    private static final ReentrantLock lock = new ReentrantLock();
    private final MutableLiveData<ApiResponse<LoginResponse>> loginMutableLiveData = new MutableLiveData<>();

    private final MutableLiveData<ApiResponse<String>> changePasswordLiveData = new MutableLiveData<>();
    private Thread thread;
    private static final NetworkClientBuilder networkClient = NetworkClientBuilder.defaults();


    /* singleTon Only */
    private UsersRepository() {

    }

    public static UsersRepository getInstance() {
        if (usersRepository == null) {
            synchronized (UsersRepository.class) {
                if (usersRepository == null)
                    return usersRepository = new UsersRepository();
            }
        }

        return usersRepository;
    }

    public MutableLiveData<ApiResponse<PhoneExistResponse>> checkPhoneNumber(String path, String phone) {
        final MutableLiveData<ApiResponse<PhoneExistResponse>> responseMutableLiveData = new MutableLiveData<>();
        thread = new Thread(() -> {
            lock.lock();
            try {
                responseMutableLiveData.postValue(
                        networkClient.
                                // https://eu-apis.my-face.app/api/v1/users/verify-phone
                                        url(path + "?target=" + phone)
                                .then()
                                .get(new TypeToken<ApiResponse<PhoneExistResponse>>() {
                                }.getType(), false));

            } finally {
                lock.unlock();
            }
        });
        thread.start();
        return responseMutableLiveData;
    }

    public MutableLiveData<ApiResponse<LoginResponse>> login(String path, LoginRequest loginRequest) {

        thread = new Thread(() -> {
            lock.lock();
            try {
                loginMutableLiveData.postValue(
                        networkClient
                                .url(path)
                                .then()
                                .post(new TypeToken<ApiResponse<LoginResponse>>() {
                                }.getType(), loginRequest, false));
            } finally {
                lock.unlock();
            }

        });
        thread.start();

        return loginMutableLiveData;
    }

    public MutableLiveData<ApiResponse<LoginResponse>> loginSocial(String path, LoginSocialRequest loginSocialRequest) {
        thread = new Thread(() -> {
            lock.lock();
            try {
                loginMutableLiveData.postValue(
                        networkClient
                                .url(path)
                                .then()
                                .post(new TypeToken<ApiResponse<LoginResponse>>() {
                                }.getType(), loginSocialRequest, false));
            } finally {
                lock.unlock();
            }

        });
        thread.start();

        return loginMutableLiveData;
    }


    public MutableLiveData<ApiResponse<RegistrationResponse>> registrationSocial(String path, RegistrationSocialRequest registrationSocialRequest) {
        final MutableLiveData<ApiResponse<RegistrationResponse>> registrationResponseLiveData = new MutableLiveData<>();
        thread = new Thread(() -> {
            lock.lock();
            try {
                registrationResponseLiveData.postValue(networkClient
                        .url(ApiUrls.API_URL + path)
                        .then()
                        .post(new TypeToken<ApiResponse<RegistrationResponse>>() {
                        }.getType(), registrationSocialRequest, false));

            } finally {
                lock.unlock();
            }
        });

        thread.start();
        return registrationResponseLiveData;

    }

    public MutableLiveData<ApiResponse<RegistrationResponse>> registerUser(String path, RegistrationRequest registrationRequest) {
        final MutableLiveData<ApiResponse<RegistrationResponse>> registrationResponseLiveData = new MutableLiveData<>();
        thread = new Thread(() -> {
            lock.lock();
            try {
                registrationResponseLiveData.postValue(networkClient
                        .url(ApiUrls.API_URL + path)
                        .then()
                        .post(new TypeToken<ApiResponse<RegistrationResponse>>() {
                        }.getType(), registrationRequest, false));

            } finally {
                lock.unlock();
            }
        });

        thread.start();
        return registrationResponseLiveData;
    }

    /*  spinner.start()
        blocking()
    *   updateAsync()
        unblocking()
    *   spinner.stop() */

    /**
     * Method  To Async Update the UserPassWord
     *
     * @param path              the Api Path
     * @param token             User Token
     * @param refreshToken      User Refresh Token
     * @param changePassRequest the Patch request.
     * @return null or updated response in Observer
     */
    public MutableLiveData<ApiResponse<String>> changePassword(String path, String token, String refreshToken, ChangePassRequest changePassRequest) {
        if (token.isEmpty()) {
            new SweetAlertDialog(HomeFragment.context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setContentText("Session Expired, Please Login!")
                    .setCustomImage(R.drawable.error_center_x)
                    .setConfirmText("Login")
                    .setConfirmClickListener(sweetAlertDialog -> {
                        sweetAlertDialog.dismissWithAnimation();
                        NavigationHandler.navigate(returnView, SettingsFragmentDirections.actionSettingsToLogin().getActionId(), null);
                    })
                    .show();
        }
        Map<String, String> extras = new HashMap<>();
        extras.put("token", token);
        extras.put("refreshToken", refreshToken);
        changePasswordLiveData.setValue(
                networkClient
                        .url(ApiUrls.API_URL + path)
                        .headers(extras)
                        .then()
                        .updateAsync(new TypeToken<ApiResponse<String>>() {
                        }.getType(), changePassRequest));

        return changePasswordLiveData;
    }

    public MutableLiveData<ApiResponse<String>> resetPasswordRequest(String path, String email) {
        final MutableLiveData<ApiResponse<String>> resetPassResponseLiveData = new MutableLiveData<>();
        thread = new Thread(() -> {
            lock.lock();
            try {
                resetPassResponseLiveData.postValue(networkClient
                        .url(ApiUrls.API_URL + path + "?email=" + email)
                        .then()
                        .get(new TypeToken<ApiResponse<String>>() {
                        }.getType(), false));
            } finally {
                lock.unlock();
            }
        });

        thread.start();
        return resetPassResponseLiveData;
    }
}
