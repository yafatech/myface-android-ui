package com.firstmotion.myface.networking.models.reponses;

import java.io.Serializable;

public class RegistrationResponse implements Serializable {
    private String userId;
    private String username;
    private boolean accountStatus;
    private String created;
    private boolean phoneVerified;
    private boolean emailVerified;

    public RegistrationResponse() {
    }

    public RegistrationResponse(String userId, String username, boolean accountStatus, String created, boolean phoneVerified, boolean emailVerified) {
        this.userId = userId;
        this.username = username;
        this.accountStatus = accountStatus;
        this.created = created;
        this.phoneVerified = phoneVerified;
        this.emailVerified = emailVerified;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(boolean accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public boolean isPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    @Override
    public String toString() {
        return "RegistrationResponse{" +
                "userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", accountStatus=" + accountStatus +
                ", created='" + created + '\'' +
                ", phoneVerified=" + phoneVerified +
                ", emailVerified=" + emailVerified +
                '}';
    }
}
