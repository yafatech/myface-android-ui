package com.firstmotion.myface.networking.api;


import android.graphics.Bitmap;

import com.firstmotion.myface.networking.models.ApiResponse;

import java.io.File;
import java.lang.reflect.Type;

/**
 * interface to Hold the Core Functions of the backend api call methods
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.1
 */
public interface NetworkClient {

    /**
     * method to request data as a GET request from the Api
     *
     * @param <T> the Type of the query
     * @return an instance to continue chaining
     */

    /**
     * Get without headers.
     *
     * @param <T>
     * @param type
     * @return
     */
    <T> ApiResponse<T> get(Type type, boolean withHeaders);

    /**
     * Method to Upload File to the APIs
     *
     * @param type the return type
     * @param file the file to be uploaded
     * @param <T>  the Type of the response
     * @return apiResponse from the APIs
     */
    <T> ApiResponse<T> uploadFile(Type type, File file);

    /**
     * method to perform POST request to the Api
     *
     * @param <T>  the Object type
     * @param type the type of the Object
     * @param type
     * @param body the json Object to send to the api
     * @return an instance to continue chaining
     */

    <T, R> ApiResponse<R> post(Type type, T body, boolean withHeaders);

    /**
     * loading images to android using okhttp
     *
     * @param imgUrl the image url where the img file is hosted.
     * @return Bitmap file.
     */

    /**
     * method to load Image as bitmap from the APIs
     *
     * @param imgUrl the image path
     * @return Bitmap object
     */
    // Bitmap in android
    Bitmap loadImage(String imgUrl);

    Bitmap loadAsyncImage(String imgUrl);

    /**
     * method to Perform Delete request to the Api.
     *
     * @param type type of the object
     * @param <T>  the object type
     * @return an instance to continue chaining
     */

    <T> ApiResponse<T> delete(Type type);

    /**
     * method to update resource in the DB, send it to the Api to update it
     *
     * @param <T>  the object type
     * @param type the object type
     * @param body the object as json
     * @return an instance to continue chaining
     */
    <T, R> ApiResponse<R> update(Type type, T body);

    <T, R> ApiResponse<R> updateAsync(Type type, T body);

    /**
     * Post a Async Request in Flavor of FaceDetection .
     *
     * @param type the response  type
     * @param body request body as POJO
     * @param <T>  Pojo Type
     * @param <R>  return Type
     * @return Response
     */
    <T, R> ApiResponse<R> postAsync(Type type, T body);

}