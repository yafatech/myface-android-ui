package com.firstmotion.myface.networking.wrappers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.preference.PreferenceManager;

import com.firstmotion.myface.networking.models.reponses.RefreshTokenResponse;
import com.firstmotion.myface.networking.models.requests.RefreshTokenRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import okhttp3.Authenticator;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;

import static com.firstmotion.myface.view.ui.MainActivity.MainAppContext;
import static com.firstmotion.myface.view.ui.MainActivity.getMainAppContext;

/**
 * Authenticator class that is used to Check either we have a Valid Access token or its Expired :)\
 * refresh the access token in case we have an expired one :)
 *
 * @author Feras E Alawadi
 * @version 1.0.101
 * @since 1.0.107
 */
public class RefreshTokenAuthenticator implements Authenticator {
    private static final Logger LOGGER = Logger.getLogger(RefreshTokenAuthenticator.class.getName());
    private static final MediaType JSON_UTF8 = MediaType.parse("application/json; charset=utf-8");
    private static RefreshTokenResponse responseApiResponse;

    public RefreshTokenAuthenticator() {

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Nullable
    @Override
    public Request authenticate(Route route, @NotNull Response response) {
        Context context = getMainAppContext();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String accessToken = prefs.getString("refreshToken", "");
        if (!isRequestWithAccessToken(response) || accessToken == null) {
            return null;
        }
        LOGGER.info(" [.] Starting Authenticator...");
        synchronized (this) {
            final String oldRefreshToken = prefs.getString("refreshToken", "");
            // previous Call needs a refresh token
            final String newAccessToken = prefs.getString("token", "");
            if (accessToken.equals(newAccessToken) && response.code() != 401) {
                // token refreshed prev.
                LOGGER.info(" [*] rollback to request, Valid Token Exist");
                return newRequestWithAccessToken(response.request(), newAccessToken);
            } else {
                // refresh token.
                String newToken = refreshToken(oldRefreshToken, prefs);
                LOGGER.info(" [*] Refreshing AccessToken: " + newToken);
                return newRequestWithAccessToken(response.request(), newToken);
            }
        }
    }


    private boolean isRequestWithAccessToken(@NonNull Response response) {
        String header = response.request().header("Authorization");
        return header != null && header.startsWith("Bearer");
    }

    private String refreshToken(String oldRefreshToken, SharedPreferences prefs) {
        final OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        final Gson gson = new Gson();
        RequestBody requestBody = RequestBody.create(JSON_UTF8, gson.toJson(new RefreshTokenRequest(oldRefreshToken)));
        Request request = new Request.Builder()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .url("https://eu-apis.my-face.app/api/v1/auth/refresh")
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                LOGGER.info(" [x] Failed When Obtaining Refresh Token...");
                // remove the token from the system in case backend failed to refresh the token...
                SharedPreferences globalPreferences = PreferenceManager.getDefaultSharedPreferences(MainAppContext);
                @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = globalPreferences.edit();
                // remove the token
                editor.remove("token").apply();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                responseApiResponse = new Gson().fromJson(response.body().charStream(), new TypeToken<RefreshTokenResponse>() {
                }.getType());
                if (responseApiResponse.getToken() == null) {
                    SharedPreferences globalPreferences = PreferenceManager.getDefaultSharedPreferences(MainAppContext);
                    @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = globalPreferences.edit();
                    // remove the token
                    editor.remove("token").apply();
                } else
                    LOGGER.info(" [.] new token obtained: " + responseApiResponse.getToken());
            }
        });
        if (responseApiResponse != null) {
            LOGGER.info(" [.] return new token: " + responseApiResponse.getToken());
            @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = prefs.edit();
            editor.putString("token", responseApiResponse.getToken()).apply();
            editor.putString("refreshToken", responseApiResponse.getRefreshToken()).apply();
            return responseApiResponse.getToken();
        }
        return null;
    }

    @NonNull
    private Request newRequestWithAccessToken(@NonNull Request request, @NonNull String accessToken) {
        return request.newBuilder()
                .header("Authorization", "Bearer " + accessToken)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();
    }
}
