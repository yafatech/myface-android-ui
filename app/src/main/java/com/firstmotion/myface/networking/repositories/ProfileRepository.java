package com.firstmotion.myface.networking.repositories;

import android.graphics.Bitmap;

import androidx.lifecycle.MutableLiveData;

import com.firstmotion.myface.R;
import com.firstmotion.myface.networking.api.NetworkClientBuilder;
import com.firstmotion.myface.networking.models.ApiResponse;
import com.firstmotion.myface.networking.models.reponses.FileInfo;
import com.firstmotion.myface.networking.models.reponses.LoadUserProfileResponse;
import com.firstmotion.myface.networking.models.reponses.UpdateProfileImageStatus;
import com.firstmotion.myface.networking.models.reponses.UpdateUserProfileResponse;
import com.firstmotion.myface.networking.models.requests.UpdateImagePrivacy;
import com.firstmotion.myface.networking.models.requests.UpdateProfileRequest;
import com.firstmotion.myface.networking.resources.ApiUrls;
import com.firstmotion.myface.view.ui.changepassword.ChangePasswordFragmentDirections;
import com.firstmotion.myface.view.ui.home.HomeFragment;
import com.firstmotion.myface.view.ui.profile.ProfileFragmentDirections;
import com.firstmotion.myface.view.ui.settings.SettingsFragmentDirections;
import com.firstmotion.myface.view.ui.uploadimages.UploadImagesFragmentDirections;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.firstmotion.myface.view.ui.settings.SettingsFragment.navController;

public class ProfileRepository {
    private static ProfileRepository profileRepository;
    private Thread thread;
    private static final NetworkClientBuilder networkClient = NetworkClientBuilder.defaults();
    private static final ReentrantLock lock = new ReentrantLock();


    private ProfileRepository() {
    }

    public static ProfileRepository getInstance() {
        if (profileRepository == null) {
            synchronized (ProfileRepository.class) {
                if (profileRepository == null) return profileRepository = new ProfileRepository();
            }
        }
        return profileRepository;
    }


    public MutableLiveData<ApiResponse<LoadUserProfileResponse>> loadingUserProfile(String path, String token, String refreshToken, String username, String destination) {
        // if the destination is Home, settings or profile

        if (token.isEmpty()) {
            new SweetAlertDialog(HomeFragment.context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setContentText("Session Expired, Please Login!")
                    .setCustomImage(R.drawable.error_center_x)
                    .setConfirmText("GO")
                    .setConfirmClickListener(sweetAlertDialog -> {
                        sweetAlertDialog.dismissWithAnimation();
                        switch (destination) {
                            case "uploadImg":
                                navController.navigate(UploadImagesFragmentDirections.actionUploadImgOneToLogin());
                                break;
                            case "settings":
                                navController.navigate(SettingsFragmentDirections.actionSettingsToLogin());
                                break;
                            case "changePass":
                                navController.navigate(ChangePasswordFragmentDirections.actionChangePasswordToLogin());
                                break;
                            case "profile":
                                navController.navigate(ProfileFragmentDirections.actionProfileToLogin());
                                break;
                            default:
                                break;
                        }
//                        NavigationHandler.navigate(returnView, navController.navigate(), null);
                    })
                    .show();
        }
        Map<String, String> extras = new HashMap<>();
        extras.put("token", token);
        extras.put("refreshToken", refreshToken);

        final MutableLiveData<ApiResponse<LoadUserProfileResponse>> loadLive = new MutableLiveData<>();
        thread = new Thread(() -> {
            lock.lock();
            try {
                loadLive.postValue(networkClient
                        .url(ApiUrls.API_URL + path + "?username=" + username)
                        .headers(extras)
                        .then()
                        .get(new TypeToken<ApiResponse<LoadUserProfileResponse>>() {
                        }.getType(), true));

            } finally {
                lock.unlock();
            }
        });

        thread.start();

        System.out.println("live2: " + loadLive.hasObservers());
        return loadLive;
    }

    public MutableLiveData<Bitmap> loadImage(String path, String origin) {
        final MutableLiveData<Bitmap> loadImageLiveData = new MutableLiveData<>();
        thread = new Thread(() -> {
            lock.lock();
            try {
                loadImageLiveData.postValue(
                        networkClient
                                .then()
                                .loadImage(ApiUrls.CDN_URL + path + "?origin=" + origin)
                );
            } finally {
                lock.unlock();
            }
        });
        thread.start();

        return loadImageLiveData;

    }

    public MutableLiveData<Bitmap> loadImageByUrl(String path) {
        final MutableLiveData<Bitmap> loadImageLiveData = new MutableLiveData<>();
        thread = new Thread(() -> {
            lock.lock();
            try {
                loadImageLiveData.postValue(
                        networkClient
                                .then()
                                .loadImage(path)
                );
            } finally {
                lock.unlock();
            }
        });
        thread.start();

        return loadImageLiveData;

    }

    public MutableLiveData<ApiResponse<FileInfo>> uploadImage(String path, String userId, File imgFile) {
        final MutableLiveData<ApiResponse<FileInfo>> uploadImageLiveData = new MutableLiveData<>();
        thread = new Thread(() -> {
            lock.lock();
            try {
                uploadImageLiveData.postValue(
                        networkClient
                                .url(ApiUrls.CDN_URL + path + "?owner=" + userId)
                                .then()
                                .uploadFile(new TypeToken<ApiResponse<FileInfo>>() {
                                        }.getType(),
                                        imgFile)
                );
            } finally {
                lock.unlock();
            }
        });
        thread.start();
        return uploadImageLiveData;
    }

    public MutableLiveData<ApiResponse<UpdateUserProfileResponse>> updateUserProfile(String path, String token, String refreshToken, UpdateProfileRequest updateProfileRequest) {
        if (token.isEmpty()) {
            new SweetAlertDialog(HomeFragment.context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setContentText("Session Expired, Please Login!")
                    .setCustomImage(R.drawable.error_center_x)
                    .setConfirmText("GO")
                    .setConfirmClickListener(sweetAlertDialog -> {
                        sweetAlertDialog.dismissWithAnimation();
                        navController.navigate(ProfileFragmentDirections.actionProfileToLogin());
                    })
                    .show();
        }
        final MutableLiveData<ApiResponse<UpdateUserProfileResponse>> updateUserProfileLiveData = new MutableLiveData<>();
        Map<String, String> extras = new HashMap<>();
        extras.put("token", token);
        extras.put("refreshToken", refreshToken);
        thread = new Thread(() -> {
            lock.lock();
            try {
                updateUserProfileLiveData.postValue(
                        networkClient
                                .url(ApiUrls.API_URL + path)
                                .headers(extras)
                                .then()
                                .update(new TypeToken<ApiResponse<UpdateUserProfileResponse>>() {
                                }.getType(), updateProfileRequest));

            } finally {
                lock.unlock();
            }
        });

        thread.start();
        return updateUserProfileLiveData;
    }

    public MutableLiveData<ApiResponse<UpdateProfileImageStatus>> updateProfileImageStatus(String path, String token, String refreshToken, UpdateImagePrivacy updateImagePrivacy) {
        if (token.isEmpty()) {
            new SweetAlertDialog(HomeFragment.context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setContentText("Session Expired, Please Login!")
                    .setCustomImage(R.drawable.error_center_x)
                    .setConfirmText("GO")
                    .setConfirmClickListener(sweetAlertDialog -> {
                        sweetAlertDialog.dismissWithAnimation();
                        navController.navigate(ProfileFragmentDirections.actionProfileToLogin());
                    })
                    .show();
        }
        final MutableLiveData<ApiResponse<UpdateProfileImageStatus>> updateImageStatusLiveData = new MutableLiveData<>();
        Map<String, String> extras = new HashMap<>();
        extras.put("token", token);
        extras.put("refreshToken", refreshToken);
        thread = new Thread(() -> {
            lock.lock();
            try {
                updateImageStatusLiveData.postValue(
                        networkClient
                                .url(ApiUrls.API_URL + path)
                                .headers(extras)
                                .then()
                                .update(new TypeToken<ApiResponse<UpdateProfileImageStatus>>() {
                                }.getType(), updateImagePrivacy));

            } finally {
                lock.unlock();
            }
        }

        );
        thread.start();
        return updateImageStatusLiveData;

    }

}
