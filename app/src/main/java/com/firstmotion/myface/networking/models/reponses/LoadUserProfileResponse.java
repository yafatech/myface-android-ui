package com.firstmotion.myface.networking.models.reponses;

import java.util.List;


public class LoadUserProfileResponse {
    private String userId;
    private String username;
    private String email;
    private List<RoleResponse> roles;
    private UserInfo usersInfo;
    private Boolean showImage;
    private String created;
    private String modified;

    public LoadUserProfileResponse() {
    }

    public LoadUserProfileResponse(String userId, String username, String email, List<RoleResponse> roles, UserInfo usersInfo, Boolean showImage, String created, String modified) {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.roles = roles;
        this.usersInfo = usersInfo;
        this.showImage=showImage;
        this.created = created;
        this.modified = modified;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public Boolean getShowImage() {
        return showImage;
    }

    public void setShowImage(Boolean showImage) {
        this.showImage = showImage;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserInfo getUsersInfo() {
        return usersInfo;
    }

    public void setUsersInfo(UserInfo usersInfo) {
        this.usersInfo = usersInfo;
    }

    public List<RoleResponse> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleResponse> roles) {
        this.roles = roles;
    }

    public UserInfo getUserInfo() {
        return usersInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.usersInfo = userInfo;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    @Override
    public String toString() {
        return "LoadUserProfileResponse{" +
                "userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", roles=" + roles +
                ", usersInfo=" + usersInfo +
                ", created='" + created + '\'' +
                ", modified='" + modified + '\'' +
                '}';
    }
}
