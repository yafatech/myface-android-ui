package com.firstmotion.myface.networking.repositories;

import androidx.lifecycle.MutableLiveData;

import com.firstmotion.myface.networking.api.NetworkClientBuilder;
import com.firstmotion.myface.networking.models.ApiResponse;
import com.firstmotion.myface.networking.models.AppSettings;
import com.firstmotion.myface.networking.resources.ApiUrls;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class SplashScreenRepository {
    private static final MutableLiveData<ApiResponse<List<AppSettings>>> mutableLiveData = new MutableLiveData<>();
    private static final MutableLiveData<ApiResponse<AppSettings>> singleAppSettingsLiveData = new MutableLiveData<>();
    private static volatile SplashScreenRepository splashScreenRepository;
    private static final NetworkClientBuilder networkClient = NetworkClientBuilder.defaults();
    private Thread thread;
    private static final ReentrantLock lock = new ReentrantLock();

    /* SingleTon */
    private SplashScreenRepository() {
    }

    public static SplashScreenRepository getInstance() {
        if (splashScreenRepository == null) {
            synchronized (SplashScreenRepository.class) {
                return splashScreenRepository = new SplashScreenRepository();
            }
        }
        return splashScreenRepository;
    }

    public MutableLiveData<ApiResponse<List<AppSettings>>> getSettings(String path, String region) {
        thread = new Thread(() -> {
            lock.lock();
            try {
                mutableLiveData.postValue(
                        networkClient
                                .url(ApiUrls.API_URL + path + "?region=" + region)
                                .then()
                                .get(new TypeToken<ApiResponse<List<AppSettings>>>() {
                                }.getType(), false));
            } finally {
                lock.unlock();
            }
        });

        // start
        thread.start();

        return mutableLiveData;
    }


    public MutableLiveData<ApiResponse<AppSettings>> getRegionAndLanguageCode(String path, String region, String langCode) {
        thread = new Thread(() -> {
            lock.lock();
            try {
                singleAppSettingsLiveData.postValue(
                        networkClient
                                .url(ApiUrls.API_URL + path + "?region=" + region + "&lang_code=" + langCode)
                                .then()
                                .get(new TypeToken<ApiResponse<AppSettings>>() {
                                }.getType(), false));
            } finally {
                lock.unlock();
            }
        });

        // start
        thread.start();

        return singleAppSettingsLiveData;
    }
}
