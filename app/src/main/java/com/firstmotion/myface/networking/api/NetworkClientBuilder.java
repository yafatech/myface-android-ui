package com.firstmotion.myface.networking.api;

import com.firstmotion.myface.networking.wrappers.NetworkClientBuilderWrapper;

import java.util.Map;

/**
 * System Network Client Builder interface is used to Perform HTTP Request
 * this interface is the Entry point to the System
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.1
 */
public interface NetworkClientBuilder {
    /**
     * method to Build a new implementation of the interface
     *
     * @param clazz the class
     * @param <E>   return type usually the class that implements the interface
     * @return new instance of the new implementation
     * @throws Exception Constructors Exception
     */
    static <E> E build(Class<E> clazz) throws Exception {
        return clazz.getDeclaredConstructor().newInstance();
    }

    /**
     * method to build the object with the default interface implementation.
     * ded
     *
     * @return new instance of that implementation.
     */
    static NetworkClientBuilderWrapper defaults() {
        return NetworkClientBuilderWrapper.getInstance();
    }

    /**
     * Method to get the api url (usually the End point for a resource)
     *
     * @param apiUrl the End point url
     * @return chaining Object
     */

    NetworkClientBuilder url(String apiUrl);

    /**
     * Method to Pass The Apis Headers if they are needed
     *
     * @param headers hashtable that contains the Api Header like token and any extra headers
     * @return network client instance
     */
    NetworkClientBuilder headers(Map<String, String> headers);

    /**
     * Method to Complete the Chain of the Network Client
     * this method will give us an instance of the Network Client which contains the HTTP Methods like get,post...etc
     *
     * @return network client instance
     */
    NetworkClient then();
}
