package com.firstmotion.myface.networking.models;

import java.util.List;

public class AppLangGroup {
    private String languageName;
    private String langCode;
    private boolean active;
    private List<Translations> translations;

    public AppLangGroup() {
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Translations> getTranslations() {
        return translations;
    }

    public void setTranslations(List<Translations> translations) {
        this.translations = translations;
    }

    @Override
    public String toString() {
        return "AppLangGroup{" +
                "languageName='" + languageName + '\'' +
                ", langCode='" + langCode + '\'' +
                ", active=" + active +
                ", translations=" + translations +
                '}';
    }
}
