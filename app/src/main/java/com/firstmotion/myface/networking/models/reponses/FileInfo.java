package com.firstmotion.myface.networking.models.reponses;


public class FileInfo {
    private String fileId;

    public FileInfo() {
    }


    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

}
