package com.firstmotion.myface.networking.models.reponses;

public class PhoneExistResponse {
    private boolean phoneExist;

    public PhoneExistResponse() {
    }

    public boolean isPhoneExist() {
        return phoneExist;
    }

    public void setPhoneExist(boolean phoneExist) {
        this.phoneExist = phoneExist;
    }
}
