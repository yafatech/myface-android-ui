package com.firstmotion.myface.networking.repositories;


import androidx.lifecycle.MutableLiveData;

import com.firstmotion.myface.R;
import com.firstmotion.myface.models.DetectFacesObject;
import com.firstmotion.myface.models.RecognizedPeopleResponse;
import com.firstmotion.myface.networking.api.NetworkClientBuilder;
import com.firstmotion.myface.networking.models.ApiResponse;
import com.firstmotion.myface.networking.resources.ApiUrls;
import com.firstmotion.myface.utils.NavigationHandler;
import com.firstmotion.myface.view.ui.home.HomeFragment;
import com.firstmotion.myface.view.ui.settings.SettingsFragmentDirections;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.firstmotion.myface.view.ui.settings.SettingsFragment.returnView;

public class FaceRecognitionRepository {
    private static final ReentrantLock lock = new ReentrantLock();
    private static final NetworkClientBuilder networkClient = NetworkClientBuilder.defaults();


    public MutableLiveData<ApiResponse<List<RecognizedPeopleResponse>>> recognizePeople(DetectFacesObject detectFacesObject, String path, String token, String refreshToken) {
        if (token.isEmpty()) {
            new SweetAlertDialog(HomeFragment.context, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setContentText("Session Expired, Please Login!")
                    .setCustomImage(R.drawable.error_center_x)
                    .setConfirmText("Login")
                    .setConfirmClickListener(sweetAlertDialog -> {
                        sweetAlertDialog.dismissWithAnimation();
                        NavigationHandler.navigate(returnView, SettingsFragmentDirections.actionSettingsToLogin().getActionId(), null);
                    })
                    .show();
        }
        Map<String, String> params = new HashMap<>();
        params.put("token", token);
        params.put("refreshToken", refreshToken);
        MutableLiveData<ApiResponse<List<RecognizedPeopleResponse>>> responseMutableLiveData = new MutableLiveData<>();
        Thread thread = new Thread(() -> {
            lock.lock();
            try {
                ApiResponse<List<RecognizedPeopleResponse>> recognizedPeopleResponse = networkClient
                        .url(ApiUrls.API_URL + path)
                        .headers(params)
                        .then()
                        .post(new TypeToken<ApiResponse<List<RecognizedPeopleResponse>>>() {
                        }.getType(), detectFacesObject, true);

                responseMutableLiveData.postValue(recognizedPeopleResponse);
                System.out.println("recognizedPeopleResponse: " + recognizedPeopleResponse);

            } finally {
                lock.unlock();
            }
        });

        thread.start();

        return responseMutableLiveData;
    }
}
