package com.firstmotion.myface.networking.models.requests;

public class LoginSocialRequest {
    private String authToken;

    public LoginSocialRequest() {
    }

    public LoginSocialRequest(String authToken) {
        this.authToken = authToken;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
