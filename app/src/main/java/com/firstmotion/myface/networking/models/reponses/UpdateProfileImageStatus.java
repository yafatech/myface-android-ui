package com.firstmotion.myface.networking.models.reponses;

public class UpdateProfileImageStatus {
    private String changed;
    private String showImage;

    public UpdateProfileImageStatus() {
    }

    public UpdateProfileImageStatus(String changed, String showImage) {
        this.changed = changed;
        this.showImage = showImage;
    }

    public String getChanged() {
        return changed;
    }

    public void setChanged(String changed) {
        this.changed = changed;
    }

    public String getShowImage() {
        return showImage;
    }

    public void setShowImage(String showImage) {
        this.showImage = showImage;
    }


    @Override
    public String toString() {
        return "UpdateProfileImageStatus{" +
                "changed='" + changed + '\'' +
                ", showImage='" + showImage + '\'' +
                '}';
    }
}
