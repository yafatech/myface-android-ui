package com.firstmotion.myface.networking.models.reponses;

import java.io.Serializable;
import java.util.Objects;

/**
 * created by Rahaf Alhameed on 12/6/2020.
 **/
public class UserInfo implements Serializable {
    private String imgUrl;
    private String fullName;
    private String phoneNumber;
    private String country;
    private String city;
    private Integer age;
    private String created;
    private String modified;

    public UserInfo(String imgUrl, String fullName, String phoneNumber, String country, String city, Integer age, String created, String modified) {
        this.imgUrl = imgUrl;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.city = city;
        this.age = age;
        this.created = created;
        this.modified = modified;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfo that = (UserInfo) o;
        return Objects.equals(imgUrl, that.imgUrl) &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(country, that.country) &&
                Objects.equals(city, that.city) &&
                Objects.equals(age, that.age) &&
                Objects.equals(created, that.created) &&
                Objects.equals(modified, that.modified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(imgUrl, fullName, phoneNumber, country, city, age, created, modified);
    }

    @Override
    public String toString() {
        return "EditProfile{" +
                "imgUrl='" + imgUrl + '\'' +
                ", fullName='" + fullName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                ", created='" + created + '\'' +
                ", modified='" + modified + '\'' +
                '}';
    }
}
