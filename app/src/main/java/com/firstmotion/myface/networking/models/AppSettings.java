package com.firstmotion.myface.networking.models;

import java.util.List;

public class AppSettings {
    private String id;
    private String imgUrl;
    private String region;
    private String systemColorCode;
    private List<AppLangGroup> appLangGroup;


    public AppSettings() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSystemColorCode() {
        return systemColorCode;
    }

    public void setSystemColorCode(String systemColorCode) {
        this.systemColorCode = systemColorCode;
    }

    public List<AppLangGroup> getAppLangGroup() {
        return appLangGroup;
    }

    public void setAppLangGroup(List<AppLangGroup> appLangGroup) {
        this.appLangGroup = appLangGroup;
    }

    @Override
    public String toString() {
        return "AppSettings{" +
                "id='" + id + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", region='" + region + '\'' +
                ", systemColorCode='" + systemColorCode + '\'' +
                ", appLangGroup=" + appLangGroup +
                '}';
    }
}
