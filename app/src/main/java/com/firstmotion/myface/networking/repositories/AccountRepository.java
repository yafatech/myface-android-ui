package com.firstmotion.myface.networking.repositories;

import androidx.lifecycle.MutableLiveData;

import com.firstmotion.myface.networking.api.NetworkClientBuilder;
import com.firstmotion.myface.networking.models.ApiResponse;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

public final class AccountRepository {
    private static AccountRepository accountRepository;
    private static final ReentrantLock lock = new ReentrantLock();
    private Thread thread;
    private static final NetworkClientBuilder networkClient = NetworkClientBuilder.defaults();

    /* singleTon Only */
    private AccountRepository() {

    }

    public static AccountRepository getInstance() {
        if (accountRepository == null) {
            synchronized (AccountRepository.class) {
                if (accountRepository == null)
                    return accountRepository = new AccountRepository();
            }
        }
        return accountRepository;
    }

    public MutableLiveData<ApiResponse<String>> deleteAccount(String path, String token, String refreshToken, String userId) {
        final MutableLiveData<ApiResponse<String>> responseMutableLiveData = new MutableLiveData<>();
        Map<String, String> extras = new HashMap<>();
        extras.put("token", token);
        extras.put("refreshToken", refreshToken);
        thread = new Thread(() -> {
            lock.lock();
            try {
                responseMutableLiveData.postValue(
                        networkClient.
                                // https://eu-apis.my-face.app/api/v1/users/delete?user_id=
                                        url(path + "?user_id=" + userId)
                                .headers(extras)
                                .then()
                                .delete(new TypeToken<ApiResponse<String>>() {
                                }.getType()));

            } finally {
                lock.unlock();
            }
        });
        thread.start();
        return responseMutableLiveData;
    }
    public MutableLiveData<ApiResponse<String>> deactivateAccount(String path, String token, String refreshToken, String userId)
    {
        final MutableLiveData<ApiResponse<String>> responseMutableLiveData = new MutableLiveData<>();
        Map<String, String> extras = new HashMap<>();
        extras.put("token", token);
        extras.put("refreshToken", refreshToken);
        thread = new Thread(() -> {
            lock.lock();
            try {
                responseMutableLiveData.postValue(
                        networkClient.
                                // https://eu-apis.my-face.app/api/v1/users/deactivate?user_id=
                                        url(path + "?user_id=" + userId)
                                .headers(extras)
                                .then()
                                .update(new TypeToken<ApiResponse<String>>() {
                                }.getType(), null));

            } finally {
                lock.unlock();
            }
        });thread.start();
        return responseMutableLiveData;
    }

}
