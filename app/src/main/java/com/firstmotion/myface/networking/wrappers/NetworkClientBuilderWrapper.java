package com.firstmotion.myface.networking.wrappers;


import android.os.Build;

import androidx.annotation.RequiresApi;

import com.firstmotion.myface.networking.api.NetworkClient;
import com.firstmotion.myface.networking.api.NetworkClientBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * network client builder implementation
 * is used to creat the first instance of the network client with passing some params to the API Class like the URL, Some headers ...etc.
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.1
 */
public final class NetworkClientBuilderWrapper implements NetworkClientBuilder {

    private static volatile NetworkClientBuilderWrapper networkClientBuilderWrapper;
    // hashtable to hold any extra stuff the api needs.
    private final Map<String, String> extras = new HashMap<>();

    // singleton only.
    private NetworkClientBuilderWrapper() {
    }

    public static NetworkClientBuilderWrapper getInstance() {
        if (networkClientBuilderWrapper == null) {
            synchronized (NetworkClientBuilderWrapper.class) {
                networkClientBuilderWrapper = new NetworkClientBuilderWrapper();
            }
        }
        return networkClientBuilderWrapper;
    }

    @Override
    public NetworkClientBuilder url(String apiUrl) {
        extras.put("url", apiUrl);
        return this;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public NetworkClientBuilder headers(Map<String, String> headers) {
        // adding the headers.
        headers.forEach(extras::put);
        return this;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public NetworkClient then() {
        return BasicNetworkClientWrapper.getInstance(extras);
    }

}
