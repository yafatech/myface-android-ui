package com.firstmotion.myface.networking.models.reponses;

public class RoleResponse {
    private String roleName;

    public RoleResponse() {
    }

    public RoleResponse(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
