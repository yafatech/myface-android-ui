package com.firstmotion.myface.networking.resources;

public final class ApiUrls {
    public static final String API_URL = "https://eu-apis.my-face.app/api/v1";
    public static final String CDN_URL = "https://eu-cdn.my-face.app/";
}
