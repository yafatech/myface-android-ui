package com.firstmotion.myface.networking.wrappers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.firstmotion.myface.networking.api.NetworkClient;
import com.firstmotion.myface.networking.callbacks.CallbackFuture;
import com.firstmotion.myface.networking.models.ApiResponse;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * for deserialize the objects please refer to: https://stackoverflow.com/questions/34660339/gson-deserialization-with-generic-types-and-generic-field-names
 * Basic Network client interface wrapper that is used to perform Network HTTP Request GET, POST, PATCH ...etc.
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.1
 */
@RequiresApi(api = Build.VERSION_CODES.N)
public final class BasicNetworkClientWrapper implements NetworkClient {
    private static final Logger logger = Logger.getLogger(BasicNetworkClientWrapper.class.getName());
    // meta Data for the post request.
    private static final MediaType JSON_UTF8 = MediaType.parse("application/json; charset=utf-8");
    // External Api Client
    private static final OkHttpClient client = new OkHttpClient.Builder()
            // refresh token authenticator is used to refresh accessTokens When they are expired
            .authenticator(new RefreshTokenAuthenticator())
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build();


    private final Gson gson = new Gson();
    private static volatile BasicNetworkClientWrapper basicNetworkClientWrapper;
    // extra api attr
    private static final Map<String, String> extras = new HashMap<>();

    /* SingleTon Object only */
    private BasicNetworkClientWrapper() {
    }

    /* singleton object instance */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static BasicNetworkClientWrapper getInstance(Map<String, String> attributes) {
        attributes.forEach(extras::put);
        // the object is expensive so use only the singleton object.
        if (basicNetworkClientWrapper == null) {
            synchronized (BasicNetworkClientWrapper.class) {
                return basicNetworkClientWrapper = new BasicNetworkClientWrapper();
            }
        }
        return basicNetworkClientWrapper;
    }


    @Override
    public <T> ApiResponse<T> uploadFile(Type type, File file) {
        RequestBody formBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MediaType.parse("multipart/form-data"), file))
                .build();
        Request request = new Request.Builder()
                .url(Objects.requireNonNull(extras.get("url")))
                .post(formBody)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                return gson.fromJson(Objects.requireNonNull(response.body()).charStream(), type);
            } else {
                logger.info("Can't Upload File: code: " + response.code());
                return null;
            }

        } catch (IOException e) {
            logger.info("can't Upload the File, check the api call");
            return null;
        }
    }

    private Request buildRequest(String url, String token, boolean withHeaders) {
        if (withHeaders)
            return new Request.Builder()
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .header("Authorization", "Bearer " + token)
                    .url(url)
                    .build();

        else
            return new Request.Builder()
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .url(url)
                    .build();
    }

    private Request buildRequest(String url, String token, RequestBody requestBody, boolean withHeaders) {
        if (withHeaders)
            return new Request.Builder()
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .header("Authorization", "Bearer " + token)
                    .url(url)
                    .post(requestBody)
                    .build();
        else
            return new Request.Builder()
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .url(Objects.requireNonNull(extras.get("url")))
                    .post(requestBody)
                    .build();
    }

    @Override
    public <T> ApiResponse<T> get(Type type, boolean withHeaders) {
        Request request = buildRequest(extras.get("url"), extras.get("token"), withHeaders);
        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                return gson.fromJson(Objects.requireNonNull(response.body()).charStream(), type);
            } else {
                System.out.println("not success..." + response.code());
                return null;
            }
        } catch (Exception e) {
            return null;
//            throw new IllegalStateException("can't get data from api, error: " + e.getMessage());
        }

    }


    @Override
    public <T, R> ApiResponse<R> post(Type type, T body, boolean withHeaders) {
        // Generating request body.
        RequestBody requestBody = RequestBody.create(JSON_UTF8, gson.toJson(body));

        Request request = buildRequest(extras.get("url"), extras.get("token"), requestBody, withHeaders);

        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                return gson.fromJson(Objects.requireNonNull(response.body()).charStream(), type);
            } else {
                logger.info("can't Post Data To The APIs, response code: " + response.code());
                return null;
            }
        } catch (Exception e) {
            logger.info("can't post date to the Api, error: " + e.getMessage());
            return null;
        }
    }

    @Override
    public Bitmap loadImage(String imgUrl) {

        Request request = new Request.Builder()
                .url(imgUrl)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                return BitmapFactory.decodeStream(Objects.requireNonNull(response.body()).byteStream());
            } else return null;
        } catch (IOException e) {
            return null;
        }

    }

    @Override
    public Bitmap loadAsyncImage(String imgUrl) {
        final CallbackFuture future = new CallbackFuture();
        Request request = new Request.Builder()
                .url(imgUrl)
                .build();

        client.newCall(request).enqueue(future);
        try {
            Response response = future.get();
            if (response.isSuccessful()) {
                return BitmapFactory.decodeStream(Objects.requireNonNull(response.body()).byteStream());
            } else return null;
        } catch (ExecutionException | InterruptedException e) {
            return null;
        }
    }

    @Override
    public <T> ApiResponse<T> delete(Type type) {
        Request request =
                new Request.Builder()
                        .header("Accept", "application/json")
                        .header("Content-Type", "application/json")
                        .header("Authorization", "Bearer " + Objects.requireNonNull(extras.get("token").trim()))
                        .url(Objects.requireNonNull(extras.get("url").trim()))
                        .delete()
                        .build();
        try (Response response = client.newCall(request).execute()) {
            if (response.isSuccessful()) {
                return gson.fromJson(response.body().string(), type);
            } else {
                logger.info("can't Delete resource: error code: " + response.code());
                return null;
            }
        } catch (IOException e) {
            logger.info("can't perform an delete request");
            return null;
        }
    }

    @Override
    public <T, R> ApiResponse<R> update(Type type, T body) {

        RequestBody requestBody = RequestBody.create(JSON_UTF8, gson.toJson(body));
        Request request = new Request.Builder()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + Objects.requireNonNull(extras.get("token").trim()))
                .url(Objects.requireNonNull(extras.get("url").trim()))
                .patch(requestBody)
                .build();

        try (Response response = client.newCall(request).execute()) {

            if (response.isSuccessful()) {
                return gson.fromJson(response.body() != null ? response.body().string() : null, type);
            } else {
                logger.info("can't Update resource: error code: " + response.code());
                return null;
            }
        } catch (IOException e) {
            logger.info("can't perform an update request");
            return null;
        }
    }

    public <T, R> ApiResponse<R> updateAsync(Type type, T body) {
        final CallbackFuture future = new CallbackFuture();
        RequestBody requestBody = RequestBody.create(JSON_UTF8, gson.toJson(body));
        Request request = new Request.Builder()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + Objects.requireNonNull(extras.get("token")).trim())
                .url(Objects.requireNonNull(extras.get("url")).trim())
                .patch(requestBody)
                .build();

        client.newCall(request).enqueue(future);
        try {
            Response response = future.get();
            if (response.isSuccessful()) {
                return gson.fromJson(response.body() != null ? response.body().string() : null, type);
            } else
                return null;
        } catch (IOException | ExecutionException | InterruptedException ignore) {
            return null;
        }

    }


    public <T, R> ApiResponse<R> postAsync(Type type, T body) {

        final CallbackFuture future = new CallbackFuture();
        RequestBody requestBody = RequestBody.create(JSON_UTF8, new Gson().toJson(body));

        Request request = new Request.Builder()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + Objects.requireNonNull(extras.get("token")).trim())
                .url(Objects.requireNonNull(extras.get("url")).trim())
                .post(requestBody)
                .build();


        client.newCall(request).enqueue(future);

        try {
            Response response = future.get();
            if (response.isSuccessful()) {
                return gson.fromJson(Objects.requireNonNull(response.body()).charStream(), type);
            } else {
                return null;
            }

        } catch (InterruptedException | ExecutionException e) {
            logger.info("post exception..." + e.getMessage());
            return null;
        }

    }

}
