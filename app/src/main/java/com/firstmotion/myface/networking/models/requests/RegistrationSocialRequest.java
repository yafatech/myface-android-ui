package com.firstmotion.myface.networking.models.requests;

public class RegistrationSocialRequest {
    private String authToken;
    private String phoneNumber;

    public RegistrationSocialRequest() {
    }

    public RegistrationSocialRequest(String authToken, String phoneNumber) {
        this.authToken = authToken;
        this.phoneNumber = phoneNumber;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
