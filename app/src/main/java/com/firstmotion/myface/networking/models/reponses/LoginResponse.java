package com.firstmotion.myface.networking.models.reponses;

import java.util.List;

public class LoginResponse {
    private String token;
    private String refreshToken;
    private List<RoleResponse> roles;

    public LoginResponse() {
    }

    public LoginResponse(String token, String refreshToken, List<RoleResponse> roles) {
        this.token = token;
        this.refreshToken = refreshToken;
        this.roles = roles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public List<RoleResponse> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleResponse> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "token='" + token + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", roles=" + roles +
                '}';
    }
}
