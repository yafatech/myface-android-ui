package com.firstmotion.myface.networking.models.requests;

import java.io.Serializable;

/**
 * created by Rahaf Alhameed on 12/6/2020.
 **/
public class UpdateProfileRequest implements Serializable {

    private String userId;
    private String avatarId;
    private String fullName;
    private String phoneNumber;
    private String country;
    private String city;
    private Integer age;

    public UpdateProfileRequest() {
    }

    public UpdateProfileRequest(String userId, String avatarId, String fullName, String phoneNumber, String country, String city, Integer age) {
        this.userId = userId;
        this.avatarId = avatarId;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.city = city;
        this.age = age;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(String avatarId) {
        this.avatarId = avatarId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

}
