package com.firstmotion.myface.models;

import java.util.List;

public final class DetectFacesObject {
    private String userId;
    private List<ImageData> imgData;


    public DetectFacesObject(String userId, List<ImageData> imgData) {
        this.userId = userId;
        this.imgData = imgData;
    }

    public DetectFacesObject() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<ImageData> getImgData() {
        return imgData;
    }

    public void setImgData(List<ImageData> imgData) {
        this.imgData = imgData;
    }


    public static class ImageData {
        private String fileOwner;
        private String fileName;
        private String fileData;

        public ImageData() {
        }

        public ImageData(String fileOwner, String fileName, String fileData) {
            this.fileOwner = fileOwner;
            this.fileName = fileName;
            this.fileData = fileData;
        }


        public String getFileOwner() {
            return fileOwner;
        }

        public void setFileOwner(String fileOwner) {
            this.fileOwner = fileOwner;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getFileData() {
            return fileData;
        }

        public void setFileData(String fileData) {
            this.fileData = fileData;
        }


    }

}
