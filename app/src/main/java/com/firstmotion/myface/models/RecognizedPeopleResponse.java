package com.firstmotion.myface.models;

public final class RecognizedPeopleResponse {
    private String fullName;
    private String imgUrl;
    private String country;
    private String city;
    private int age;
    private String percentage;
    private Boolean showImage;

    public RecognizedPeopleResponse() {
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Boolean getShowImage() {
        return showImage;
    }

    public void setShowImage(Boolean showImage) {
        this.showImage = showImage;
    }

    @Override
    public String toString() {
        return "RecognizedPeopleResponse{" +
                "fullName='" + fullName + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                ", percentage='" + percentage + '\'' +
                ", showImage=" + showImage +
                '}';
    }
}
