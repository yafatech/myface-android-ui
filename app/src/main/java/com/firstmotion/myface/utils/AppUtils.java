package com.firstmotion.myface.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Helper Methods
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.1
 */
public final class AppUtils {
    /**
     * Method to Check if the Phone have available internet Connection
     *
     * @param context Fragment Context
     * @return true if connected to Internet
     */
    public static boolean isNetworkAvailable(final Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        if (cm == null) return false;
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        return (networkInfo != null && networkInfo.isConnected());
    }

}
