package com.firstmotion.myface.utils.mail;

import android.content.Context;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailServiceImpl implements MailService {
    private static final String companyMail = "support@my-face.app";
    private static final String companyMailPass = "mdmpajfieeudgoxx";
    private static MailServiceImpl mailService;
    private Context context;
    private Session session;

    // singleton only
    private MailServiceImpl() {

    }

    // grab an instance
    public static MailServiceImpl getInstance() {
        if (mailService == null) {
            synchronized (MailServiceImpl.class) {
                if (mailService == null)
                    return mailService = new MailServiceImpl();
            }
        }
        return mailService;
    }


    @Override
    public MailService prepareConnection() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(companyMail, companyMailPass);
            }
        });
        return this;
    }

    public boolean sendMail(String to, String content) {
        if (session == null)
            throw new IllegalStateException("Please Make sure to initialize the connection First, call prepareConnection() method first");
        try {
            MimeMessage mm = new MimeMessage(session);
            // from
            mm.setFrom(new InternetAddress(companyMail));
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            mm.setSubject("no-reply, Technical Support needed!");
            mm.setText(content);
            Transport.send(mm);
            return true;
        } catch (MessagingException e) {
            return false;
        }

    }

}
