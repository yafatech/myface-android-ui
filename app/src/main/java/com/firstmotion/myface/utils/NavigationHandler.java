package com.firstmotion.myface.utils;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;

import com.firstmotion.myface.view.ui.login.LoginFragment;

public class NavigationHandler {

    public static void navigate(View view, @IdRes int destination) {
        navigate(view, destination, /* args */null);
    }

    public static void navigate(View view, @IdRes int destination, @Nullable Bundle args) {
        try {
            Navigation.findNavController(view).navigate(destination, args);
        } catch (IllegalArgumentException e) {
            Log.e(LoginFragment.class.getSimpleName(), "Multiple navigation attempts handled.");
        }
    }
}
