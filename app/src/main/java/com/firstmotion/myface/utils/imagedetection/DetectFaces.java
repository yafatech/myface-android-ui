package com.firstmotion.myface.utils.imagedetection;

import android.graphics.Bitmap;

import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.face.Face;

import java.util.List;

/**
 * Interface for Implementation Make sure that the users uploads Photos for real persons
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.1
 */
public interface DetectFaces {

    /**
     * Get the Default implementation
     *
     * @return Default implementation instance
     */
    static DetectFaces build() {
        return DetectFacesImpl.getInstance();
    }

    /**
     * Method to Pass the user photo
     *
     * @param bitmap photo as bitmap
     * @return Same instance fro the implementation
     */
    DetectFaces imageFromBitmap(Bitmap bitmap);

    /**
     * method to start the detect daces implementation
     *
     * @return Listeners For the Detect Faces onSuccessListener()
     */
    Task<List<Face>> detectFaces();
}
