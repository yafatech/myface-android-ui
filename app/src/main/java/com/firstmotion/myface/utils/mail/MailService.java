package com.firstmotion.myface.utils.mail;

public interface MailService {

    static MailService builder() {
        return MailServiceImpl.getInstance();
    }

    MailService prepareConnection();

    boolean sendMail(String to, String content);
}
