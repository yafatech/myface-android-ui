package com.firstmotion.myface.utils.systemnotifications;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

/**
 * System Notifications Using Snackbar Default implementation
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.1
 */
public class SystemNotificationImpl implements SystemNotification {
    private static SystemNotificationImpl systemNotification;

    /*  SingleTon Only  */
    private SystemNotificationImpl() {
    }

    /* singleton instance  */
    public static SystemNotificationImpl getInstance() {
        if (systemNotification == null) {
            synchronized (SystemNotificationImpl.class) {
                if (systemNotification == null) {
                    systemNotification = new SystemNotificationImpl();
                }
            }
        }
        return systemNotification;
    }

    @SuppressLint({"ResourceAsColor", "ShowToast"})
    @Override
    public void pushNotification(String message, View parentLayout, String action, Activity activity, int color) {

        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction(action, view1 -> {
                    // snackBar.dismiss()
                    // but left empty to have the default behavior which is dismiss()
                }).setDuration(3000)
                .setActionTextColor(activity.getResources().getColor(color))
                .show();
    }
}
