package com.firstmotion.myface.utils.systemnotifications;

import android.app.Activity;
import android.view.View;

/**
 * System Notifications as Snackbar
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.1
 */
public interface SystemNotification {
    /**
     * Get the Snackbar Default implementation
     *
     * @return SystemNotification Instance
     */
    static SystemNotification build() {
        return SystemNotificationImpl.getInstance();
    }

    /**
     * Method to Push the Notification to the user
     *
     * @param message  the message content
     * @param view     the current view to show the Snackbar in
     * @param action   the action as String -> the button in the Snackbar
     * @param activity the activity that host the Snackbar
     * @param color    the Snackbar btn color from resources.
     */
    void pushNotification(String message, View view, String action, Activity activity, int color);
}
