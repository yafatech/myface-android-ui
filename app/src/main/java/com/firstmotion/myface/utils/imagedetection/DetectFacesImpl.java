package com.firstmotion.myface.utils.imagedetection;

import android.graphics.Bitmap;

import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetectorOptions;

import java.util.List;

/**
 * Base Implementation For DetectFaces Interface and its used to Detect if the uploaded images by the user are For Real people not
 * random images to make sure that the user only uploads valid images for real persons.
 *
 * @author Feras E Alawadi
 * @version 1.0.1
 * @since 1.0.1
 */

public class DetectFacesImpl implements DetectFaces {
    // High-accuracy landmark detection and face classification
    private final static FaceDetectorOptions highAccuracyOpts =
            new FaceDetectorOptions.Builder()
                    .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                    .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                    .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                    .build();
    private static final boolean isFace = false;
    private static DetectFacesImpl detectFaces;
    private InputImage inputImage;

    /* SingleTon Only */
    private DetectFacesImpl() {
    }

    public static DetectFacesImpl getInstance() {
        if (detectFaces == null) {
            synchronized (DetectFacesImpl.class) {
                detectFaces = new DetectFacesImpl();
            }
        }
        return detectFaces;
    }


    @Override
    public DetectFaces imageFromBitmap(Bitmap bitmap) {
        int rotationDegree = 0;
        if (bitmap == null)
            throw new IllegalStateException("Bitmap File Can't Be null");

        this.inputImage = InputImage.fromBitmap(bitmap, rotationDegree);
        return this;
    }

    @Override
    public Task<List<Face>> detectFaces() {
        FaceDetectorOptions options =
                new FaceDetectorOptions.Builder()
                        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                        .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
//                        .setMinFaceSize(0.15f)
//                        .enableTracking()
                        .build();

        return FaceDetection.getClient(options).process(inputImage);
    }
}
