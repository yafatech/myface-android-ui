package com.firstmotion.myface.utils;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.HashMap;
import java.util.Map;

public final class RegionsResolver {
    private static final Map<String, String> availableRegions = new HashMap<>();

    /* static methods only */
    private RegionsResolver() {

    }

    static {
        // ME, Europe Countries.
        availableRegions.put("united kingdom", "eu");
        availableRegions.put("germany", "eu");
        availableRegions.put("saudi arabia", "eu");
        availableRegions.put("lebanon", "eu");
        availableRegions.put("syria", "eu");
        availableRegions.put("turkey", "eu");
        availableRegions.put("oman", "eu");
        availableRegions.put("bahrain", "eu");
        availableRegions.put("kuwait", "eu");
        availableRegions.put("united arab emirates", "eu");

        // us regions
        availableRegions.put("canada", "us");
        availableRegions.put("united states of america", "us");

        // Asia, Australia.
        availableRegions.put("india", "ap");
        availableRegions.put("korea", "ap");
        availableRegions.put("australia", "ap");
        availableRegions.put("new zealand", "ap");

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String getRegion(String countryName) {
        try {
            String region = availableRegions.get(countryName);
            return region != null ? region : "eu";
        } catch (Exception e) {
            return "eu";
        }
    }
}
