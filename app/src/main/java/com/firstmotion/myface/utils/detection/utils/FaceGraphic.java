package com.firstmotion.myface.utils.detection.utils;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Vibrator;
import android.view.View;

import androidx.annotation.RequiresApi;

import com.firstmotion.myface.R;
import com.firstmotion.myface.models.DetectFacesObject;
import com.firstmotion.myface.models.RecognizedPeopleResponse;
import com.firstmotion.myface.networking.callbacks.OkHttpResponseFuture;
import com.firstmotion.myface.networking.models.ApiResponse;
import com.firstmotion.myface.networking.resources.ApiUrls;
import com.firstmotion.myface.networking.wrappers.RefreshTokenAuthenticator;
import com.firstmotion.myface.utils.detection.system.GraphicOverlay;
import com.firstmotion.myface.view.ui.home.HomeFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceContour;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.firstmotion.myface.utils.detection.system.VisionProcessorBase.imagesList;
import static com.firstmotion.myface.view.ui.home.HomeFragment.binding;
import static com.firstmotion.myface.view.ui.home.HomeFragment.homeView;
import static com.firstmotion.myface.view.ui.home.HomeFragment.imageDataList;
import static com.firstmotion.myface.view.ui.home.HomeFragment.token;
import static com.firstmotion.myface.view.ui.home.HomeFragment.userId;


/**
 * Graphic instance for rendering face position, contour, and landmarks within the associated
 * graphic overlay view.
 */
@RequiresApi(api = Build.VERSION_CODES.N)
public class FaceGraphic extends GraphicOverlay.Graphic {
    private static final OkHttpClient client = new OkHttpClient.Builder()
            .authenticator(new RefreshTokenAuthenticator())
            .connectTimeout(25, TimeUnit.SECONDS)
            .writeTimeout(25, TimeUnit.SECONDS)
            .readTimeout(25, TimeUnit.SECONDS)
            .build();
    private static final MediaType JSON_UTF8 = MediaType.parse("application/json; charset=utf-8");
    @SuppressLint("StaticFieldLeak")
    private final static HomeFragment homeFragment = new HomeFragment();
    private static final float FACE_POSITION_RADIUS = 10.0f;
    private static Map<PointF, PointF> faceHashH;
    private final Paint circlePaint;
    private final Paint linePaint;
    private final Paint scannersPaint;
    private static Map<PointF, PointF> faceHashV;
    private static boolean oneCycle = false;
    private static PointF leftEyeTopPoint, leftEyeEnd, leftCheekPoint, rightEyeTopPoint, faceP2, faceP1, faceP3, faceP4, faceP5, faceP6, faceP7, faceP8, faceP9, faceP10, faceP11, faceP12, faceDotsV, faceDotsH, rightEyeEnd, rightCheekPoint, noseBridgeStart,
            noseBottomStart, noseBottomEnd, noseBottomMiddle, upperLipTopStart, upperLipTopEnd, upperLipTopMiddle, lowerLipBottomMiddle;
    private static int indexH = 9;
    private static int indexV = 0;
    private static boolean isApiCalled = false;
    private static boolean boolValue = true;
    private final Face face;
    private static OkHttpResponseFuture result = new OkHttpResponseFuture();
    private static MediaPlayer mp = MediaPlayer.create(HomeFragment.context, R.raw.start);
    private final MediaPlayer endMp = MediaPlayer.create(HomeFragment.context, R.raw.end);
    private final MediaPlayer failMp = MediaPlayer.create(HomeFragment.context, R.raw.sad);

    public FaceGraphic(GraphicOverlay overlay, Face face) {

        super(overlay);
        this.face = face;

        // circles Paint ..
        circlePaint = new Paint();
        circlePaint.setStrokeWidth(15.0f);
        circlePaint.setColor(Color.WHITE);
        circlePaint.setShadowLayer(40, 0, 1, Color.WHITE);

        // Lines Paint...
        linePaint = new Paint();
        linePaint.setStrokeWidth(2);
        linePaint.setColor(Color.WHITE);
        linePaint.setShadowLayer(40, 0, 1, Color.WHITE);

        // scanners paint...
        scannersPaint = new Paint();
        scannersPaint.setStrokeWidth(15.0f);
        scannersPaint.setColor(Color.WHITE);
        scannersPaint.setShadowLayer(40, 0, 1, Color.WHITE);
        scannersPaint.setStrokeCap(Paint.Cap.ROUND);

        faceHashV = new HashMap<>();
        faceHashH = new HashMap<>();


    }

    private static boolean soundStarted = true;

    private void resetClient() {
        mp = MediaPlayer.create(HomeFragment.context, R.raw.start);
        homeFragment.showProgress(binding, View.GONE);
        isApiCalled = false;
        oneCycle = false;
        boolValue = true;
        soundStarted = true;
        imageDataList = new ArrayList<>();
        imagesList = new ArrayList<>();
        result = new OkHttpResponseFuture();

    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void draw(Canvas canvas) {
        Face face = this.face;
        if (face == null) {
            return;
        }
        float y = translateY(face.getBoundingBox().centerY());

        for (String img : imagesList) {
            if (imageDataList.size() >= 3)
                break;
            imageDataList.add(new DetectFacesObject.ImageData(
                    userId,
                    UUID.randomUUID().toString() + ".jpg",
                    img));

        }

        if (oneCycle) {
            if (!isApiCalled) {
                if (!imageDataList.isEmpty()) {
                    homeFragment.showProgress(binding, View.VISIBLE);

                    RequestBody requestBody = RequestBody.create(JSON_UTF8, new Gson().toJson(new DetectFacesObject(userId, imageDataList)));
                    // redirect to login in case we Don't have a valid session...
                    if (token.isEmpty()) {
                        homeFragment.redirectToLogin();
                    }

                    Request request = new Request.Builder()
                            .header("Accept", "application/json")
                            .header("Content-Type", "application/json")
                            .header("Authorization", "Bearer " + token)
                            .url(ApiUrls.API_URL + "/face-recognition/check")
                            .post(requestBody)
                            .build();
                    client.newCall(request).enqueue(result);

                }
            }

            // start a listener on the CompletableFuture Task, Once We Got a Response From Future then we will process it.
            if (result.future.isDone()) {
                mp.stop();
                try {
                    Response response = result.future.get();
                    if (response.isSuccessful()) {

                        ApiResponse<List<RecognizedPeopleResponse>> res = new Gson().fromJson(Objects.requireNonNull(response.body()).charStream(),
                                new TypeToken<ApiResponse<List<RecognizedPeopleResponse>>>() {
                                }.getType()
                        );
                        if (res != null && res.isStatus()) {

                            endMp.start();
                            Vibrator vib = (Vibrator) homeView.getContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vib.vibrate(500);
                            homeFragment.showSuccessResponse(1, res.getResults(), binding);
                            resetClient();

                        } else if (res != null && !res.isStatus()) {
                            failMp.setVolume(0.25f, 0.25f);
                            failMp.start();
                            homeFragment.showSweetDialog(binding);
                            resetClient();
                        }
                    } else if (response.code() == 500) {
                        homeFragment.showSweetDialog(binding);
                        resetClient();
                    }

                } catch (ExecutionException | InterruptedException e) {
                    // means the connection is timeout
                    homeFragment.showInternetConnectionSlow(binding);
                    resetClient();
                }
            } else {
                isApiCalled = true;
            }
        }
        for (FaceContour contour : face.getAllContours()) {
            if (contour.getFaceContourType() == FaceContour.LEFT_EYEBROW_TOP) {
                leftEyeTopPoint = contour.getPoints().get(2);
                canvas.drawCircle(translateX(leftEyeTopPoint.x), translateY(leftEyeTopPoint.y), FACE_POSITION_RADIUS, circlePaint);
            }
            if (contour.getFaceContourType() == FaceContour.LEFT_EYE) {
                leftEyeEnd = contour.getPoints().get(contour.getPoints().size() - 1);
                canvas.drawCircle(translateX(leftEyeEnd.x), translateY(leftEyeEnd.y), FACE_POSITION_RADIUS, circlePaint);
            }
            if (contour.getFaceContourType() == FaceContour.LEFT_CHEEK) {
                leftCheekPoint = contour.getPoints().get(0);
                canvas.drawCircle(translateX(leftCheekPoint.x), translateY(leftCheekPoint.y), FACE_POSITION_RADIUS, circlePaint);
            }
            if (contour.getFaceContourType() == FaceContour.RIGHT_EYEBROW_TOP) {
                rightEyeTopPoint = contour.getPoints().get(2);
                canvas.drawCircle(translateX(rightEyeTopPoint.x), translateY(rightEyeTopPoint.y), FACE_POSITION_RADIUS, circlePaint);
            }
            if (contour.getFaceContourType() == FaceContour.RIGHT_EYE) {
                rightEyeEnd = contour.getPoints().get(contour.getPoints().size() / 2);
                canvas.drawCircle(translateX(rightEyeEnd.x), translateY(rightEyeEnd.y), FACE_POSITION_RADIUS, circlePaint);
            }
            if (contour.getFaceContourType() == FaceContour.RIGHT_CHEEK) {
                rightCheekPoint = contour.getPoints().get(0);
                canvas.drawCircle(translateX(rightCheekPoint.x), translateY(rightCheekPoint.y), FACE_POSITION_RADIUS, circlePaint);
            }
            if (contour.getFaceContourType() == FaceContour.NOSE_BRIDGE) {
                noseBridgeStart = contour.getPoints().get(0);
                canvas.drawCircle(translateX(noseBridgeStart.x), translateY(noseBridgeStart.y), FACE_POSITION_RADIUS, circlePaint);
            }
            if (contour.getFaceContourType() == FaceContour.NOSE_BOTTOM) {
                noseBottomStart = contour.getPoints().get(0);
                noseBottomEnd = contour.getPoints().get(contour.getPoints().size() - 1);
                noseBottomMiddle = contour.getPoints().get(contour.getPoints().size() / 2);
                canvas.drawCircle(translateX(noseBottomStart.x), translateY(noseBottomStart.y), FACE_POSITION_RADIUS, circlePaint);
                canvas.drawCircle(translateX(noseBottomEnd.x), translateY(noseBottomEnd.y), FACE_POSITION_RADIUS, circlePaint);
                canvas.drawCircle(translateX(noseBottomMiddle.x), translateY(noseBottomMiddle.y), FACE_POSITION_RADIUS, circlePaint);
            }
            if (contour.getFaceContourType() == FaceContour.UPPER_LIP_TOP) {
                upperLipTopStart = contour.getPoints().get(0);
                upperLipTopEnd = contour.getPoints().get(contour.getPoints().size() - 1);
                upperLipTopMiddle = contour.getPoints().get(contour.getPoints().size() / 2);
                canvas.drawCircle(translateX(upperLipTopStart.x), translateY(upperLipTopStart.y), FACE_POSITION_RADIUS, circlePaint);
                canvas.drawCircle(translateX(upperLipTopEnd.x), translateY(upperLipTopEnd.y), FACE_POSITION_RADIUS, circlePaint);
                canvas.drawCircle(translateX(upperLipTopMiddle.x), translateY(upperLipTopMiddle.y), FACE_POSITION_RADIUS, circlePaint);

            }
            if (contour.getFaceContourType() == FaceContour.LOWER_LIP_BOTTOM) {
                lowerLipBottomMiddle = contour.getPoints().get(contour.getPoints().size() / 2);
                canvas.drawCircle(translateX(lowerLipBottomMiddle.x), translateY(lowerLipBottomMiddle.y), FACE_POSITION_RADIUS, circlePaint);
            }
            if (contour.getFaceContourType() == FaceContour.FACE) {
                faceDotsV = contour.getPoints().get(indexV);
                faceDotsH = contour.getPoints().get(indexH);
                faceP1 = contour.getPoints().get(0);
                faceP2 = contour.getPoints().get(3);
                faceP3 = contour.getPoints().get(6);
                faceP4 = contour.getPoints().get(9);
                faceP5 = contour.getPoints().get(12);
                faceP6 = contour.getPoints().get(15);
                faceP7 = contour.getPoints().get(18);
                faceP8 = contour.getPoints().get(21);
                faceP9 = contour.getPoints().get(24);
                faceP10 = contour.getPoints().get(27);
                faceP11 = contour.getPoints().get(30);
                faceP12 = contour.getPoints().get(33);

                faceHashH.put(contour.getPoints().get(0), contour.getPoints().get(18));
                faceHashH.put(contour.getPoints().get(1), contour.getPoints().get(17));
                faceHashH.put(contour.getPoints().get(2), contour.getPoints().get(16));
                faceHashH.put(contour.getPoints().get(3), contour.getPoints().get(15));
                faceHashH.put(contour.getPoints().get(4), contour.getPoints().get(14));
                faceHashH.put(contour.getPoints().get(5), contour.getPoints().get(13));
                faceHashH.put(contour.getPoints().get(6), contour.getPoints().get(12));
                faceHashH.put(contour.getPoints().get(7), contour.getPoints().get(11));
                faceHashH.put(contour.getPoints().get(8), contour.getPoints().get(10));
                faceHashH.put(contour.getPoints().get(9), contour.getPoints().get(9));
                faceHashH.put(contour.getPoints().get(18), contour.getPoints().get(0));
                faceHashH.put(contour.getPoints().get(17), contour.getPoints().get(1));
                faceHashH.put(contour.getPoints().get(16), contour.getPoints().get(2));
                faceHashH.put(contour.getPoints().get(15), contour.getPoints().get(3));
                faceHashH.put(contour.getPoints().get(14), contour.getPoints().get(4));
                faceHashH.put(contour.getPoints().get(13), contour.getPoints().get(5));
                faceHashH.put(contour.getPoints().get(12), contour.getPoints().get(6));
                faceHashH.put(contour.getPoints().get(11), contour.getPoints().get(7));
                faceHashH.put(contour.getPoints().get(10), contour.getPoints().get(8));
                faceHashH.put(contour.getPoints().get(19), contour.getPoints().get(35));
                faceHashH.put(contour.getPoints().get(20), contour.getPoints().get(34));
                faceHashH.put(contour.getPoints().get(21), contour.getPoints().get(33));
                faceHashH.put(contour.getPoints().get(22), contour.getPoints().get(32));
                faceHashH.put(contour.getPoints().get(23), contour.getPoints().get(31));
                faceHashH.put(contour.getPoints().get(24), contour.getPoints().get(30));
                faceHashH.put(contour.getPoints().get(25), contour.getPoints().get(29));
                faceHashH.put(contour.getPoints().get(26), contour.getPoints().get(28));
                faceHashH.put(contour.getPoints().get(27), contour.getPoints().get(27));

                faceHashH.put(contour.getPoints().get(28), contour.getPoints().get(26));
                faceHashH.put(contour.getPoints().get(29), contour.getPoints().get(25));
                faceHashH.put(contour.getPoints().get(30), contour.getPoints().get(24));
                faceHashH.put(contour.getPoints().get(31), contour.getPoints().get(23));
                faceHashH.put(contour.getPoints().get(32), contour.getPoints().get(22));
                faceHashH.put(contour.getPoints().get(33), contour.getPoints().get(21));
                faceHashH.put(contour.getPoints().get(34), contour.getPoints().get(20));
                faceHashH.put(contour.getPoints().get(35), contour.getPoints().get(19));


                faceHashV.put(contour.getPoints().get(0), contour.getPoints().get(0));
                faceHashV.put(contour.getPoints().get(1), contour.getPoints().get(35));
                faceHashV.put(contour.getPoints().get(2), contour.getPoints().get(34));
                faceHashV.put(contour.getPoints().get(3), contour.getPoints().get(33));
                faceHashV.put(contour.getPoints().get(4), contour.getPoints().get(32));
                faceHashV.put(contour.getPoints().get(5), contour.getPoints().get(31));
                faceHashV.put(contour.getPoints().get(6), contour.getPoints().get(30));
                faceHashV.put(contour.getPoints().get(7), contour.getPoints().get(29));
                faceHashV.put(contour.getPoints().get(8), contour.getPoints().get(28));
                faceHashV.put(contour.getPoints().get(9), contour.getPoints().get(27));
                faceHashV.put(contour.getPoints().get(10), contour.getPoints().get(26));
                faceHashV.put(contour.getPoints().get(11), contour.getPoints().get(25));
                faceHashV.put(contour.getPoints().get(12), contour.getPoints().get(24));
                faceHashV.put(contour.getPoints().get(13), contour.getPoints().get(23));
                faceHashV.put(contour.getPoints().get(14), contour.getPoints().get(22));
                faceHashV.put(contour.getPoints().get(15), contour.getPoints().get(21));
                faceHashV.put(contour.getPoints().get(16), contour.getPoints().get(20));
                faceHashV.put(contour.getPoints().get(17), contour.getPoints().get(19));
                faceHashV.put(contour.getPoints().get(18), contour.getPoints().get(18));
                faceHashV.put(contour.getPoints().get(19), contour.getPoints().get(17));
                faceHashV.put(contour.getPoints().get(20), contour.getPoints().get(16));
                faceHashV.put(contour.getPoints().get(21), contour.getPoints().get(15));
                faceHashV.put(contour.getPoints().get(22), contour.getPoints().get(14));
                faceHashV.put(contour.getPoints().get(23), contour.getPoints().get(13));
                faceHashV.put(contour.getPoints().get(24), contour.getPoints().get(12));
                faceHashV.put(contour.getPoints().get(25), contour.getPoints().get(11));
                faceHashV.put(contour.getPoints().get(26), contour.getPoints().get(10));
                faceHashV.put(contour.getPoints().get(27), contour.getPoints().get(9));
                faceHashV.put(contour.getPoints().get(28), contour.getPoints().get(8));
                faceHashV.put(contour.getPoints().get(29), contour.getPoints().get(7));
                faceHashV.put(contour.getPoints().get(30), contour.getPoints().get(6));
                faceHashV.put(contour.getPoints().get(31), contour.getPoints().get(5));
                faceHashV.put(contour.getPoints().get(32), contour.getPoints().get(4));
                faceHashV.put(contour.getPoints().get(33), contour.getPoints().get(3));
                faceHashV.put(contour.getPoints().get(34), contour.getPoints().get(2));
                faceHashV.put(contour.getPoints().get(35), contour.getPoints().get(1));

            }
        }

        canvas.drawCircle(translateX(faceP1.x), translateY(faceP1.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP2.x), translateY(faceP2.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP3.x), translateY(faceP3.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP4.x), translateY(faceP4.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP5.x), translateY(faceP5.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP6.x), translateY(faceP6.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP7.x), translateY(faceP7.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP8.x), translateY(faceP8.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP9.x), translateY(faceP9.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP10.x), translateY(faceP10.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP11.x), translateY(faceP11.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP12.x), translateY(faceP12.y), FACE_POSITION_RADIUS, circlePaint);
        canvas.drawCircle(translateX(faceP10.x), translateY(y - translateY(faceP10.y)), FACE_POSITION_RADIUS, circlePaint);

        canvas.drawLine(translateX(faceP1.x), translateY(faceP1.y), translateX(faceP2.x), translateY(faceP2.y), linePaint);
        canvas.drawLine(translateX(faceP2.x), translateY(faceP2.y), translateX(faceP3.x), translateY(faceP3.y), linePaint);
        canvas.drawLine(translateX(faceP3.x), translateY(faceP3.y), translateX(faceP4.x), translateY(faceP4.y), linePaint);
        canvas.drawLine(translateX(faceP4.x), translateY(faceP4.y), translateX(faceP5.x), translateY(faceP5.y), linePaint);
        canvas.drawLine(translateX(faceP5.x), translateY(faceP5.y), translateX(faceP6.x), translateY(faceP6.y), linePaint);
        canvas.drawLine(translateX(faceP6.x), translateY(faceP6.y), translateX(faceP7.x), translateY(faceP7.y), linePaint);
        canvas.drawLine(translateX(faceP7.x), translateY(faceP7.y), translateX(faceP8.x), translateY(faceP8.y), linePaint);
        canvas.drawLine(translateX(faceP8.x), translateY(faceP8.y), translateX(faceP9.x), translateY(faceP9.y), linePaint);
        canvas.drawLine(translateX(faceP9.x), translateY(faceP9.y), translateX(faceP10.x), translateY(faceP10.y), linePaint);
        canvas.drawLine(translateX(faceP10.x), translateY(faceP10.y), translateX(faceP11.x), translateY(faceP11.y), linePaint);
        canvas.drawLine(translateX(faceP11.x), translateY(faceP11.y), translateX(faceP12.x), translateY(faceP12.y), linePaint);
        canvas.drawLine(translateX(faceP12.x), translateY(faceP12.y), translateX(faceP1.x), translateY(faceP1.y), linePaint);
        canvas.drawLine(translateX(faceP2.x), translateY(faceP2.y), translateX(rightEyeTopPoint.x), translateY(rightEyeTopPoint.y), linePaint);
        canvas.drawLine(translateX(faceP3.x), translateY(faceP3.y), translateX(rightEyeTopPoint.x), translateY(rightEyeTopPoint.y), linePaint);
        canvas.drawLine(translateX(faceP4.x), translateY(faceP4.y), translateX(rightEyeEnd.x), translateY(rightEyeEnd.y), linePaint);
        canvas.drawLine(translateX(faceP4.x), translateY(faceP4.y), translateX(rightCheekPoint.x), translateY(rightCheekPoint.y), linePaint);
        canvas.drawLine(translateX(faceP4.x), translateY(faceP4.y), translateX(upperLipTopEnd.x), translateY(upperLipTopEnd.y), linePaint);
        canvas.drawLine(translateX(faceP5.x), translateY(faceP5.y), translateX(upperLipTopEnd.x), translateY(upperLipTopEnd.y), linePaint);
        canvas.drawLine(translateX(faceP6.x), translateY(faceP6.y), translateX(upperLipTopEnd.x), translateY(upperLipTopEnd.y), linePaint);
        canvas.drawLine(translateX(faceP7.x), translateY(faceP7.y), translateX(upperLipTopEnd.x), translateY(upperLipTopEnd.y), linePaint);
        canvas.drawLine(translateX(faceP7.x), translateY(faceP7.y), translateX(upperLipTopStart.x), translateY(upperLipTopStart.y), linePaint);
        canvas.drawLine(translateX(faceP7.x), translateY(faceP7.y), translateX(upperLipTopMiddle.x), translateY(upperLipTopMiddle.y), linePaint);
        canvas.drawLine(translateX(faceP8.x), translateY(faceP8.y), translateX(upperLipTopStart.x), translateY(upperLipTopStart.y), linePaint);
        canvas.drawLine(translateX(faceP9.x), translateY(faceP9.y), translateX(upperLipTopStart.x), translateY(upperLipTopStart.y), linePaint);
        canvas.drawLine(translateX(faceP10.x), translateY(faceP10.y), translateX(upperLipTopStart.x), translateY(upperLipTopStart.y), linePaint);
        canvas.drawLine(translateX(faceP10.x), translateY(faceP10.y), translateX(leftCheekPoint.x), translateY(leftCheekPoint.y), linePaint);
        canvas.drawLine(translateX(faceP10.x), translateY(faceP10.y), translateX(leftEyeEnd.x), translateY(leftEyeEnd.y), linePaint);
        canvas.drawLine(translateX(faceP11.x), translateY(faceP11.y), translateX(leftEyeEnd.x), translateY(leftEyeEnd.y), linePaint);
        canvas.drawLine(translateX(faceP11.x), translateY(faceP11.y), translateX(leftEyeTopPoint.x), translateY(leftEyeTopPoint.y), linePaint);
        canvas.drawLine(translateX(faceP12.x), translateY(faceP12.y), translateX(leftEyeTopPoint.x), translateY(leftEyeTopPoint.y), linePaint);
        canvas.drawLine(translateX(rightEyeTopPoint.x), translateY(rightEyeTopPoint.y), translateX(rightEyeEnd.x), translateY(rightEyeEnd.y), linePaint);
        canvas.drawLine(translateX(rightEyeTopPoint.x), translateY(rightEyeTopPoint.y), translateX(noseBridgeStart.x), translateY(noseBridgeStart.y), linePaint);
        canvas.drawLine(translateX(rightEyeEnd.x), translateY(rightEyeEnd.y), translateX(noseBridgeStart.x), translateY(noseBridgeStart.y), linePaint);
        canvas.drawLine(translateX(rightCheekPoint.x), translateY(rightCheekPoint.y), translateX(noseBottomEnd.x), translateY(noseBottomEnd.y), linePaint);
        canvas.drawLine(translateX(rightCheekPoint.x), translateY(rightCheekPoint.y), translateX(rightEyeEnd.x), translateY(rightEyeEnd.y), linePaint);
        canvas.drawLine(translateX(leftCheekPoint.x), translateY(leftCheekPoint.y), translateX(noseBottomStart.x), translateY(noseBottomStart.y), linePaint);
        canvas.drawLine(translateX(leftCheekPoint.x), translateY(leftCheekPoint.y), translateX(leftEyeEnd.x), translateY(leftEyeEnd.y), linePaint);
        canvas.drawLine(translateX(noseBottomEnd.x), translateY(noseBottomEnd.y), translateX(noseBridgeStart.x), translateY(noseBridgeStart.y), linePaint);
        canvas.drawLine(translateX(noseBottomStart.x), translateY(noseBottomStart.y), translateX(noseBridgeStart.x), translateY(noseBridgeStart.y), linePaint);
        canvas.drawLine(translateX(upperLipTopStart.x), translateY(upperLipTopStart.y), translateX(lowerLipBottomMiddle.x), translateY(lowerLipBottomMiddle.y), linePaint);
        canvas.drawLine(translateX(lowerLipBottomMiddle.x), translateY(lowerLipBottomMiddle.y), translateX(upperLipTopEnd.x), translateY(upperLipTopEnd.y), linePaint);
        canvas.drawLine(translateX(upperLipTopMiddle.x), translateY(upperLipTopMiddle.y), translateX(upperLipTopStart.x), translateY(upperLipTopStart.y), linePaint);
        canvas.drawLine(translateX(upperLipTopMiddle.x), translateY(upperLipTopMiddle.y), translateX(upperLipTopEnd.x), translateY(upperLipTopEnd.y), linePaint);
        canvas.drawLine(translateX(leftEyeEnd.x), translateY(leftEyeEnd.y), translateX(noseBridgeStart.x), translateY(noseBridgeStart.y), linePaint);
        canvas.drawLine(translateX(leftEyeTopPoint.x), translateY(leftEyeTopPoint.y), translateX(noseBridgeStart.x), translateY(noseBridgeStart.y), linePaint);
        canvas.drawLine(translateX(upperLipTopStart.x), translateY(upperLipTopStart.y), translateX(noseBottomStart.x), translateY(noseBottomStart.y), linePaint);
        canvas.drawLine(translateX(upperLipTopEnd.x), translateY(upperLipTopEnd.y), translateX(noseBottomEnd.x), translateY(noseBottomEnd.y), linePaint);
        canvas.drawLine(translateX(noseBottomMiddle.x), translateY(noseBottomMiddle.y), translateX(noseBottomStart.x), translateY(noseBottomStart.y), linePaint);
        canvas.drawLine(translateX(noseBottomMiddle.x), translateY(noseBottomMiddle.y), translateX(noseBottomEnd.x), translateY(noseBottomEnd.y), linePaint);
        canvas.drawLine(translateX(noseBottomMiddle.x), translateY(noseBottomMiddle.y), translateX(upperLipTopMiddle.x), translateY(upperLipTopMiddle.y), linePaint);
        canvas.drawLine(translateX(faceP1.x), translateY(faceP1.y), translateX(noseBridgeStart.x), translateY(noseBridgeStart.y), linePaint);
        canvas.drawLine(translateX(leftEyeTopPoint.x), translateY(leftEyeTopPoint.y), translateX(leftEyeEnd.x), translateY(leftEyeEnd.y), linePaint);

        // draw scanners.
        drawScanners(canvas, scannersPaint);
    }

    private void drawScanners(Canvas canvas, Paint paint1) {
        if (boolValue) {
            if (soundStarted) {
                mp.start();
//                soundStarted = false;
            }

            PointF pointF = faceHashV.get(faceDotsV);
            assert pointF != null;
            canvas.drawLine(translateX(faceDotsV.x), translateY(faceDotsV.y), translateX(pointF.x), translateY(pointF.y), paint1);
            indexV += 1;
            if (indexV == 35) {
                indexV = 0;
                boolValue = false;

            }
            oneCycle = false;

        }
        if (!boolValue) {
            PointF pointF = faceHashH.get(faceDotsH);
            canvas.drawLine(translateX(faceDotsH.x), translateY(faceDotsH.y), translateX(pointF.x), translateY(pointF.y), paint1);
            indexH += 1;
            if (indexH == 35) {
                indexH = 0;
                indexV = 0;
                indexH = 9;
                boolValue = true;
            }
            oneCycle = true;
        }

    }


}
